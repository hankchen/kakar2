// vue basic lib
import Vue from 'vue'
import Vuex from 'vuex'
import VueCookies from 'vue-cookies'
import Qs from 'qs';
Vue.use(Vuex)
Vue.use(VueCookies)
Vue.use(Qs)
// 客製/第三方 lib
import md5 from 'md5'
import axios from 'axios'
import fetchData from '../libs/fetchData.js'

// import { getDistance } from 'geolib'
// import routerKakar from 'src/appKakar/router'
import routerKakar2 from 'src/appKakar2/router'
// import routerWuhulife from 'src/appWuhulife/router'
//import {GetJsonTest,GetJsonData1} from 'src/json1.js'	// 取得json數據
//import {GetJsonData1} from 'src/json1.js'	// 取得json數據
import { deviseFunction, ensureDeviseSynced, deviseSetLoginInfo, setRedirectPage, showMOrder } from 'src/libs/deviseHelper.js'
import { showAlert, setLoading, makeid_num, removeEmojis,isNotLinePara, showConfirm} from 'src/libs/appHelper.js'
//import { formatTime } from 'src/libs/timeHelper.js'
// import { formatTime, currentTimeLong } from 'src/libs/timeHelper.js'
import { S_Obj, L_Obj, IsFn, IsMobile, GroupBy } from 'src/fun1.js'	// 共用fun在此!

const IsDev = S_Obj.isDebug; 	// 開發debug
// const IsDev = false; 	// 開發debug @@

// 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
function rawRemberData() {
  return {
    isLoaded: false,
    // 會員卡
    card_info: {},
    // 會員資料
    vip_info: {},
    // (可使用的) 優惠卷清單
    ticketList: [],
    // (尚未能使用的) 優惠卷清單
    notAvailableTickets: [],
    // (已轉贈的) 優惠卷清單
    gaveTicketList: [],
    // 會員購買交易資料(有購買品項與總金額)
    purchaseRecord: [],
    // 會員積點交易資料(使用紀錄)
    expiredPoints: [],
    // (單一)優惠卷明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
    ticketInfo: {
      // 規則代號
      currentTypeCode: '',
      currentRuleCode: '',
      // 使用 QRcode
      currentTicketGID: '',
      currentTicketQRcode: '',
      // 規則條款
      ruleData: {},
      // 適用商品
      usageFoods: [],
      // 適用門店
      usageShops: [],
    }
  }
}

// 企業公開資料相關
function rawPublicData() {
  return {
    // LOGO 圖檔
    logo: '',
    logoLoaded: false,
    // DB connect 位置
    connect: '9001',
    // 首頁
    index: {
      // 主畫面可滑動的 Banners
      slideBanners: [],
      // 固定的副 Banner
      subBanner: [],
    },
    // 品牌資訊(關於我們, 使用者條款, 常見問題 ... etc)
    brandInfo: [],
    // 最新消息
    news: { data: {}, types: {}, banners: {}, },
    // 菜單頁
    menu: {
      // 大類 (代號, 名稱, 清單)
      mainType: [], // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
      // 小類 (代號, 名稱, 清單)
      subType: [],  // [{ code: '', name: '',  list: {} }, { code: '', name: '',  list: {} }, ...]
    },
    // 門店資訊頁
    storeList: { store: [] },
    // 主企業號 ID & connect (用於線上點餐)
    MainEnterPriseID: '',
    MainConnect: '',
    myBookList: [], // 我的預約
    myBookStatusList: [], // 我的預約狀態 - S:已送出, P:處理中, Y:己確認, N:己取消
    cleanItemList: [], // 清潔項目清單
    serviceItemList: [], // 芳療師服務列表
    modeType:[],//門店有設功能(是否有外帶,外送,商城等..)
    currentProduct: {} // 目前商品
  }
}
// 商城共用資料相關
function rawMallData() {
  return {
    foodkind2:[], //大類
    foodkind:[],//中類全部,未分
    kind:[],  //次類,內有foods(次類中的品項)
    foodTaste:{}, //口味加料
    iv_foods:{},//小類下的所有品項
    iv_styles:{},//品項下的所有規格
    foodSpec:[],//規格
    foodSpecKind:[],//規格類別
    shipData:[],//收件人清單
    iv_spec:{},//品項規格
    orderstatus:{},//訂單狀態
    shopOrderTmp:{Total:0},//訂單主表
    orders:[], //訂單清單
    imgUrl: 'https://9001.jinher.com.tw/WebPos/images/Food/',
    nowItem:{},//當下所選品項
    spyIndex: 0,//規格index
    shopping_cart:[],//購物車
    foodQty:{},//品項計數
    kindQty:{},//品項類別計數
    isCarUI:false,
    Banner:[], //商城首頁橫幅
    Header:[], //推薦商品群組
    iv_currentItem:[], //所有商品
    foodInfo:{},//商品資料object
    foodFreight:{},//運費相關
    foodExpresst:{},//物流運費相關
    foodMeasurement:{},//材積相關
    reloadShopFn:null,
    kind2Index:0, //記下目前進入的大類
    mallScrollTop:0,
    sortPriceType:'',
    barcodeItem:[{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}],//由掃瞄生成的品項
    thirdPartyPay:{},
    checkKind_list:[],
    singleUnFinish:[],
    singleFinish:[],
    becomeApp:false,
    payStatus:{},//付款狀態
    ordersPage:{}, //訂單清單,分頁
    unOrdersPage:{}, //未付款訂單清單,分頁
    cvsShopInfo:{},//選好的超商info
  }
}

/* 取卡包企業資訊 */ /** @Fix: Line掃碼回來-疑失token問題 */
function GetCurAppUser() {
	const obj1 	= VueCookies.get('kkAppUser') || {}
		,obj2 		= L_Obj.read('currentAppUser')
	return obj2 ? obj2 : obj1;
}

// 依據環境參數，來決定 API baseUrl 的值
var baseUrl = 'kakar2.jh8.tw'
	// ,Test8012 	= '8012test.jh8.tw'

let ArrGetATicketed = []	// 自動領券觸發,同一卡包只要一次

// === 動態產生baseUrl === start
// 在殼裡面
// if (typeof(JSInterface) !== 'undefined') {
//   console.log('baseUrl ===> 在殼裡面')
// // 在網頁
// } else {
//   console.log('baseUrl ===> 在網頁')
//   let cookieApi = VueCookies.get('kkRtAPIUrl')
//   if (cookieApi && cookieApi.trim()!='') {
//     baseUrl = cookieApi
//   }
// }
// === 動態產生baseUrl === end

const state = {
  appSite: 'kakar', // 系統參數: 站台
  promise: null, // 保存promise
  // 是否在殼裡面(判斷能否使用殼相關接口的依據)
  isDeviseApp: false,
  // 啟動/關閉 『讀取中』的動畫效果
  isLoading: false,
  marketBanner:{},//暫存卡包企業首頁進商城主橫幅
  //是否撈完卡包
  isLoadCard: false,
  // 顯示手動輸入 QRcode 表格
  showInputQRCode: false,
  // 顯示手動輸入其他卡
  showInputOtherCard: false,
  // 客製的彈窗 (用於 alert / confirm)
  customModal: {
    type: '', text: '', confirmText: '', cancelText: '',
    // confirm note
    confirmNoteTitle: '', // 附註標題
    confirmNoteChoice: [], // 附註選項
    confirmNoteChoosed: '', // 附註選項已選
    confirmNoteRemark: '', // 附註選了其他要手key的說明
    confirmNoteRemarkPlaceHolder: '', // 其他原因的place holder
    confirmNoteWarnMsg: '', // 警告訊息
    confirmNoteText: '', // 附註標題(style2)
  },
  //語系
  language:'tw',
  // CSS 全域參數(用於判斷 CSS 是否設定 -webkit-overflow-scrolling: touch)
  // true => touch,
  // false => auto (幾乎只用於首頁)
  cssTouch: true,
  lineLogin:{
    id:'1655314765',
    sc:'ba7c459ee0cb7522e690bf262c2b82b9',
    token:'',
    verify:'',
    appQrcode:'',
    EnterpriseID:'',
  },
  lineNotifyInfo:{},
  thirdKey:{
    line:'LINEloginID',
    fb:'FBloginID',
  },
  supportPay:["Scan2Pay","Newebpay","LINEpay_web","JkoPay_web","ecPay"],
  searchURL:{},
  // API url
  api: {
    // 卡+ 主服務
    kakarUrl: `https://${baseUrl}/Public/AppNCWOneEntry.ashx`,
    // 企業的公開資料
    publicUrl: `https://${baseUrl}/public/newsocket.ashx`,
    //會員本地kakar
    memberbaseUrl: `https://${baseUrl}/Public/AppVip.ashx`,
    // 企業的會員資料
    memberUrl: `https://${baseUrl}/Public/AppVipOneEntry.ashx`,

    // 企業的會員消費點數資料
    consumerUrl: `https://${baseUrl}/public/AppDataVIP.ashx`,
    // 企業的會員消費點數資料,通用,但較慢
    consumerPubUrl: `https://${baseUrl}/public/AppDataVIPOneEntry.ashx`,
    // 預約資料
    bookUrl: `https://${baseUrl}/public/AppBooking.ashx`,
    // 預約的token資料
    tokenUrl: `https://${baseUrl}/public/AppBookingOneEntry.ashx`,
    // 圖片的路徑
    picUrl: `https://${baseUrl}`,
    // 會員資料存取,例:收件人存取
    vipUrl: `https://${baseUrl}/public/APPMemberOneEntry.ashx`,
  },
  // 基本資訊
  baseInfo: {
    // 英文常稱
    isFrom: 'kakar',
    // 企業號
    EnterpriseID: 'kakar',
    // FunctionID
    FunctionID: '450101',
    // 是否接收推播訊息 (跟殼要)
    pushNotify: '0', // 1:開啟推播, 0:關閉推播
    // GPS 資訊 (跟殼要)
    gps: {},
    // 螢幕亮度值 (跟殼要, range 0 ~ 255 預設為中間值)
    brightness: '120',
    // 前端打包檔版本號 (跟殼要)
    WebVer: '',
    // 殼的裝置 (iOS / Android)
    AppOS: '',
    // 殼是否有新版本
    DeviseVersionIsDiff: false,
    // (給線上點餐用) 回到 卡+ 時導向到指定企業 app
    currentEnterPriseID: (VueCookies.get('currentEnterPriseID') || ''),
    // 企業公開資料相關 (五互只需要rawPublicData()的這些欄位)
    publicData: {
      // LOGO 圖檔
      logo: '',
      logoLoaded: false,
      // 首頁
      index: {
        // 主畫面可滑動的 Banners
        slideBanners: [],
        // 固定的副 Banner
        subBanner: [],
      },
      AppName: '',
      // 最新消息
      news: { data: {}, types: {}, banners: {}, },
      // 門店資訊頁
      storeIsLoaded: false,
      storeList: { store: [] },
      // === 門店搜尋條件 === start
      storeSearch: {
        searchBrand: '全部品牌',
        searchZoneDisplay: '全部地區',
        searchZoneType: '',
        searchCity: '',
        searchArea: '',
        searchKeyWord: '',
        store2Store: false // 在store頁,透過footer再度到store頁
      },
      // === 門店搜尋條件 === end
      myBookList: [], // 我的預約

    },
    // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
    memberData: {
      // 會員資料
      vip_info: {},
      // 會員卡
      card_info: {},
      // 積分換卷的資料
      pointToTickets: [],
      // 年度總積點資料
      expiredPoints_section: [],
      // 會員積點交易資料(使用紀錄)
      expiredPointsIsLoaded: false,
      expiredPoints: [],
      // (可使用的) 優惠卷清單
      ticketList: [],
      // (尚未能使用的) 優惠卷清單
      notAvailableTickets: [],
      // (已轉贈的) 優惠卷清單
      gaveTicketList: [],
      // (單一)優惠卷明細資訊 e.g. 使用規則、條款、期限、適用範圍 ... etc
      ticketInfo: {
        // 規則代號
        currentTypeCode: '',
        currentRuleCode: '',
        // 使用 QRcode
        currentTicketGID: '',
        currentTicketQRcode: '',
        // 規則條款
        ruleData: {},
        // 適用商品
        usageFoods: [],
        // 適用門店
        usageShops: [],
      },
      consumerPoints: [], 			// 全部消費點數契約清單
      consumerPointDetail: {}, 	// 單一筆契約消費點數紀錄
      oneOrderDetail: [], 			// 單一筆訂單購物清單
    }
  },
  // 簡訊認證設定 (需要從殼取)
  SMS_Config: {
    // 簡訊回傳的驗證碼
    reCode: "",
    // 每天最多簡訊次數 (預設為 5 次)
    APPSMSDayTimes: '5',
    // 每封簡訊發送間隔 (預設為 50 秒)
    AppSMSIntervalMin: '50',
    // 每日簡訊發送扣打 (用來紀錄已寄出幾封, 不得超過 APPSMSDayTimes 的值), 格式： yyyymmdd-times => e.g. 20190513-4 (05/13 已用四次)
    SMSSendQuota: '',
  },
  // 會員接口相關資訊
  member: {
    mac: IsMobile()?'':(VueCookies.get('kkMAC') || ''),
    Tokenkey: IsMobile()?'':(VueCookies.get('kkTokenkey') || ''),
    code: IsMobile()?'':(VueCookies.get('kkUserCode') || ''),
    pwd: IsMobile()?'':(VueCookies.get('kkUserPwd')||''),
  },
  // 首頁上面的卡面搜尋 bar
  appFilter: '',
  // 企業 app 的點擊次數統計
  appClicks: VueCookies.get('kkAppClicks') || {},
  // 卡包頁所需每張卡的圖片路徑
  cardPhoto: (typeof(JSInterface) !== 'undefined')?{}:(VueCookies.get('kkCardPhoto') || {}),
  vipAppsLoaded: false,
  // 回來的api數量
  vipAppsLoadedCount: 0,
  // 已綁定的企業 app 列表
  vipApps: [],
  vip2KApps: [],
  // 已綁定的企業 user 資料
  vipUserData: [],
  // 目前的進入/操作的 app
  currentAppUser: GetCurAppUser(),
  // (卡卡)會員帳號資訊
  userData: VueCookies.get('kkUserData') || {},
  // 企業會員卡相關資訊(所屬門店、卡號、儲值金額、會員等級、優惠卷 ... etc)
  memberData: rawRemberData(),
  // 企業公開資料相關
  publicData: rawPublicData(),
   // 商城公開資料相關
  mallData: rawMallData(),

  // 卡片條碼 QRcode
  cardQRCode: { image: '', value: '', loaded: false },
  cardListScroll2: 0,
  fontSizeBase: 1,
  cardListScrollMap: 0, //最後一次scroll位置記錄
  //分享卡包QRcode
  cardShareQRCode: { image: '', value: '', loaded: false },

  //單行兩卡檢視
  gridView: false
}

const mutations = {
  /** 暫存字體大小 */
  setFontSizeTemp(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
  },
  /** 永久保存字體大小 */
  setFontSizeForever(state, fontSize) {
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    let floatFontSize = parseFloat(fontSize)
    state.fontSizeBase = floatFontSize
    // 存入cookie
    VueCookies.set('fontSize', floatFontSize)
    // 若在殼中 => 存入殼裡
    if (IsMobile()) {
      let setThing = `{"spName":"fontSize", "spValue": ${ floatFontSize }}`
      deviseFunction('SetSP', setThing, '')
      // console.log('===> 已存入殼裡 =>' + setThing)
    }
  },
  // 存入系統參數：站台 & 基礎參數
  setAppSite(state, site) {
    state.appSite = site
    // 如果 site === 'wuhulife' , 要修改對應的 baseInfo.isFrom, baseInfo.EnterpriseID
	if (site == 'wuhulife') {
		state.baseInfo.isFrom = 'wuhulifeapp'
		state.baseInfo.FunctionID = '450301'
		state.baseInfo.EnterpriseID = '53239538'
		state.baseInfo.AppName = '龍海就是消費'
	}

	if (site == 'kakar2') {
		state.baseInfo.isFrom = 'kakar2'
		state.baseInfo.FunctionID = '450401'
		state.baseInfo.FunctionID_login = '450402'
		state.baseInfo.EnterpriseID = 'kakar2'
		state.baseInfo.AppName = '卡+2.0'
	}
  },
  // 存入當下進入的企業 app 資料之token
  setCurrentAppUserToken(state, value) {
    VueCookies.set('kkTokenkey_'+state.currentAppUser.EnterPriseID, value);
    state.currentAppUser.token = value
		S_Obj.save('ApiToken', value);	// 只用在傳輸api Token
    //S_Obj.save('Tokenkey', value);
    if (typeof(JSInterface) != 'undefined') {
      // 更新token到殼
      let setThing = `{"spName":"Tokenkey_${state.currentAppUser.EnterPriseID}", "spValue": "${value}"}`
      deviseFunction('SetSP', setThing, '')
    }
  },
  // 存入當下進入的企業 app 資料
  setCurrentAppUser(state, app) {
    if (!app) return
    if (!app.Account) app.Account = state.member.code
    if (!app.token) app.token = VueCookies.get('kkTokenkey_'+app.EnterpriseID)
    const shrt_app = {
      Account:app.Account,
      mac:app.mac,
      isFrom:app.isFrom,
      EnterpriseID:app.EnterpriseID,
      EnterPriseID:app.EnterpriseID,
      apiip:app.apiip,
    }//cookie塞不下,短一點

    if (app.apiip) {
      const url1 	= app.apiip.trim()			// " https://8001.jh8.tw/public/AppVIP.ashx"
			,npos  		= url1.indexOf("//")+2
			,str1			= (npos != -1) ? url1.substring(npos) : ''
			,host1 		= str1.substring(0, str1.indexOf("/"))		// "8001.jh8.tw"
      S_Obj.save('CardHost', host1);	// 記錄卡包host
    }
    VueCookies.set('kkAppUser', shrt_app);
    state.currentAppUser = app
		L_Obj.save('currentAppUser', app);
  },
  // 清空當下進入的企業 app 資料
  clearCurrentAppUser(state) { VueCookies.remove('kkAppUser'); state.currentAppUser = {} },
  // 存入企業 app 列表的點擊紀錄 (整個 object)
  saveAppClicks(state, clicks) { state.appClicks = clicks },
  // 存入企業 app 列表的卡片圖片 (整個 object)
  saveCardPhoto(state, cardPhotos) { state.cardPhoto = cardPhotos },
  // 存入企業 app 列表的點擊紀錄 ( EnterpriseID += 1 )
  setAppClicks(state, eID) {
    state.appClicks[eID] = state.appClicks[eID] || 0
    state.appClicks[eID] += 1
    // vue 的雷： object & array 的變化，需要用下面方式才會觸發 computed / getters
    // ref: https://github.com/vuejs/vuex/issues/1311
    Vue.set(state.appClicks, eID, state.appClicks[eID])
    // 存入 cookies
    VueCookies.set('kkAppClicks', state.appClicks)
    // 存入殼裡
    deviseFunction('SetSP', `{"spName":"appClicks", "spValue": ${ JSON.stringify(state.appClicks) }}`, '')
  },
  setCardPhoto1(state, {eID, filePath}) {
    state.cardPhoto[eID] = filePath
  },
  setCardPhotoAll(state) {
    // 存入 cookies
    VueCookies.set('kkCardPhoto', state.cardPhoto)
    // 存入殼裡
    deviseFunction('SetSP', `{"spName":"cardPhoto", "spValue": ${ JSON.stringify(state.cardPhoto) }}`, '')
  },
  // 把store.state.vipUserData清掉重來
  clearVipUserData(state) {
    state.vipUserData = []
  },
  // 存入會員擁有的企業 app 使用者資料 (重複的就刷新點數資訊, 卡片資訊)
  setVipUserData(state, app) {
    const userData = state.vipUserData.find(item => item.EnterPriseID === app.EnterPriseID)
    const notFound = userData === undefined

    if (notFound) {
      state.vipUserData.push(app)
    }
    if (!notFound) {
      Object.keys(app).map(p_key=>{
        userData[p_key] = app[p_key];
      });

      userData.cardPoint = app.cardPoint
      userData.CardTypeCode = app.CardTypeCode
      userData.CardTypeName = app.CardTypeName
      userData.CardFacePhoto = app.CardFacePhoto
    }
  },
  setVipUserLoaded() { state.vipAppsLoaded = true },
  // 設定 cssTouch
  setCssTouch(state, status){ state.cssTouch = status },
  // 設定 商城目前scrollTop
  setMallScrollTop(state, value){ state.mallData.mallScrollTop = value },
  // 設定是否在殼裡面
  setIsDeviseApp(state, status) { state.isDeviseApp = status },
  // 『讀取中...』 切換
  setLoading(state, status) { state.isLoading = status },
  //暫存卡包企業首頁進商城主橫幅
  setMarketBanner(state, data) { state.marketBanner = data },
  setLoadCard(state, status) { state.isLoadCard = status },
  // 『顯示手動輸入 QRcode 表格』切換
  setInputQRCode(state, status) { state.showInputQRCode = status },
  // 『顯示手動輸入其他卡號』切換
  showInputOtherCard(state, status) { state.showInputOtherCard = status },
  // 設定彈跳視窗的資料 (顯示 / 隱藏)
  setCustomModal(state, { type, text, cancelText, confirmText,
    confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
    confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText,
    resolve, reject }) {
    cancelText = cancelText || '取消'
    confirmText = confirmText || '確認'

    state.customModal = { type, text, cancelText, confirmText,
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText}

    if (resolve && reject) state.promise = { resolve, reject }
  },
  // 存入已綁定的企業 app 列表
  setVipApps(state, payload) { state.vipApps = payload },
  // 存入手機簡訊認證的設定
  setSMS_Config(state, data) { state.SMS_Config = Object.assign({}, state.SMS_Config, data) },
  // 存入基本資訊
  setBaseInfo(state, data) { state.baseInfo = Object.assign({}, state.baseInfo, data) },
  // 語系
  setlang(state, data) { state.language = data;if (data == 'cn') baseUrl = 'kakar2.jinher-cn.com'},
  // 更新會員資訊
  setUserData(state, data) { state.userData = data },
  // line login 會員資訊
  setLineLoginInfo(state, data) {
    state.lineLogin.token = data.token;
    state.lineLogin.verify = data.verify;

    state.member.isFromTD = data.verify.tdType;
    state.member.FromTDID = data.verify.sub;
    state.member.MT_NickName = removeEmojis(data.verify.name || '');
    state.member.FromTDEmail = data.verify.email;

  },
  setLineNotifyInfo(state, data){
    state.lineNotifyInfo = data;
  },
  setThirdID(state){
    if (!IsMobile()){
      const t_line = VueCookies.get(state.thirdKey.line) || ""
      const t_fb   = VueCookies.get(state.thirdKey.fb) || ""
      state.member.FromTDID = (t_line.length ? t_line : t_fb)
    }

  },
  setLineLoginKey(state, data) {
    state.lineLogin[data.key] = data.value;
    //state.lineLogin.appQrcode = data.appQrcode;
  },
  setSearchUrl(state, data) {
    state.searchURL[data.key] = data.value;
  },
  //kakar主企業token
  setMainToken(state, token) {
    state.member.Tokenkey = token
    VueCookies.set('kkTokenkey', token)
    if (typeof(JSInterface) != 'undefined') {
      // 更新token到殼
      let setThing = `{"spName":"Tokenkey", "spValue": "${token}"}`
      deviseFunction('SetSP', setThing, '')
    }
  },
  setUpdateInfo(state, data){
    //kakar主企業
    VueCookies.set('kkTokenkey', data.Tokenkey);
    state.member.Tokenkey = data.Tokenkey;
    S_Obj.save('Tokenkey', data.Tokenkey);
  },
  // 存入會員登入資訊
  setLoginInfo(state, payload) {
    // payload sample: {data: data, rememberMe: true, pwd: 'xxx'}
    const data = payload.data
    // console.log('存入會員登入資訊: ', data)	// @@
    // console.log('rememberMe, pwd => '+payload.rememberMe, payload.pwd)	// @@

		/** @Fix:針對手機掃碼(APP一開啟無做登入狀態,故強制都記錄 */
    // 存入 cookies
    // if (payload.rememberMe) {
      // console.log('===> is payload.rememberMe')
      // // 在網頁
      // if (typeof(JSInterface) == 'undefined') {
        // console.log('setLoginInfo ===> in browser => save account to cookies')
        VueCookies.set('kkMAC', data.mac)
        VueCookies.set('kkTokenkey', data.Tokenkey)
        VueCookies.set('kkUserData', data)
        VueCookies.set('kkUserCode', data.Account)
        VueCookies.set('kkUserPwd', payload.pwd)
        VueCookies.set('kkRtAPIUrl', data.RtAPIUrl)

        const p_key = Object.keys(data);//第三方有分fb,line Login...

        if (p_key.indexOf(state.thirdKey.line) > -1) VueCookies.set(state.thirdKey.line, data[state.thirdKey.line])
        if (p_key.indexOf(state.thirdKey.fb) > -1) VueCookies.set(state.thirdKey.fb, data[state.thirdKey.fb])

      // // 在殼裡面
      // } else {
        // console.log('setLoginInfo ===> 在殼裡面')
        // // baseUrl = data.RtAPIUrl
      // }
    // }

    //console.log('===> baseUrl =>' + baseUrl)

    // 存入 store
    state.userData = data
    state.member.mac = data.mac
    state.member.Tokenkey = data.Tokenkey
    state.member.code = data.Account
    state.member.pwd = payload.pwd
    if (data.FromTDID) state.member.FromTDID = data.FromTDID;

    // 如果是龍海 app, 依據登入參數導向指定頁面
    if (data.EnterPriseID === "kakar2") setRedirectPage()
  },
  // 清除企業 app 的會員資料
  clearAppMemberData(state) { state.memberData = rawRemberData() },
  // 清除企業 app 的公開資料
  clearAppPublicData(state) { state.publicData = rawPublicData() },
  // 會員登出
  setLogout(state) {
    VueCookies.remove('kkJWT')
    VueCookies.remove('kkMAC')
    VueCookies.remove('kkTokenkey')
    VueCookies.remove('kkUserData')
    VueCookies.remove('kkUserCode')
    VueCookies.remove('kkUserPwd')
    VueCookies.remove('kkUserJWT')
    VueCookies.remove(state.thirdKey.line)
    VueCookies.remove(state.thirdKey.fb)

	// L_Obj.clear();	// nono:有存loginForm
    state.vipApps = []
    state.userData = {}
    state.vipUserData = []
    // 清除會員基本資訊
    state.member.mac = ''
    state.member.Tokenkey = ''
    state.member.code = ''
    state.member.pwd = ''
    state.member.FromTDID = '' //第三方UID
    // === 此console不可以移除 === start
    // 為了安全先拿掉console
    // let msg123 = '===> state.member =>' + JSON.stringify(state.member)
    //   + '<=== cookie => '
    //   + 'tokenkey=>' + VueCookies.get('kkTokenkey')
    //   + '<= kkJWT =>' + VueCookies.get('kkJWT')
    //   + '<= kkMAC =>' + VueCookies.get('kkMAC')
    //   + '<= kkUserData =>' + VueCookies.get('kkUserData')
    //   + '<= kkUserCode =>' + VueCookies.get('kkUserCode')
    //   + '<= kkUserPwd =>' + VueCookies.get('kkUserPwd')
    //   + '<= kkUserJWT =>' + VueCookies.get('kkUserJWT')
    //console.log(msg123)
    // alert(msg123)
    // === 此console不可以移除 === end
  },
  // 存入企業 app 的 LOGO 資訊
  setAppLogo(state, data) { state.publicData.logoLoaded = true ;state.publicData.logo = data },
  // 存入企業 app 的 LOGO 資訊 - 五互
  setAppLogoWuhu(state, data) { state.baseInfo.publicData.logoLoaded = true ;state.baseInfo.publicData.logo = data },
  // 存入我的預約
  setMyBookList(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
      })
    }
    state.baseInfo.publicData.myBookList = data || [];
  },
  // 存入我的預約
  setMyBookStatusList(state, data) {
    state.baseInfo.publicData.myBookStatusList = data
  },
  // 存入清潔項目資料
  setCleanItemList(state, data) {
    // TODO 這裡把卡包的服務預約,設為主企業public(kakar)???, 不適合.....OMG
    state.baseInfo.publicData.cleanItemList = data || [];
  },
  // 存入芳療師服務列表
  setServiceItemList(state, data) {
		data.forEach((o)=>{
			if (o.OrderNo == "undefined") {
				o.OrderNo =  '';
			}
			o.CreateTime = o.CreateTime.replace(/T/, ' ')
		})
		/** @Debug: 待查用 */
		if (IsDev && data.length) {
			let tmparr2 = []	// @@
			data.forEach((op1)=>{
				let {Order_ID,OrderNo,FoodName,CreateTime,LastModify} = op1
				tmparr2.push({Order_ID,OrderNo,FoodName,CreateTime,LastModify});
			})
			console.table(tmparr2)	// @@
		}
		let arrObj1 = []
		data = GroupBy(data, "Order_ID").map(arr1 => {
			if (arr1.length) {
				const o1 	= arr1[0]
				let obj1 = {
					Order_ID: o1.Order_ID,
					OrderNo: o1.OrderNo,
					ShopID: o1.ShopID,
					ShopName: o1.ShopName,
					VIPNo: o1.VIPNo,
					VIPName: o1.VIPName,
					CreateOP: o1.CreateOP,
					CreateTime: o1.CreateTime,
					LastModify: o1.LastModify,
				}
				obj1.arrService = arr1;

				let arr2 = []
				arr1.forEach((op1)=>{
// console.log('芳療師服務_op1: ', op1);	// @@
					let {FoodID,FoodName,GID,Item_ID,SerOP_Nickname,SerOP_name,SerOP_Nickname2,SerOP_name2,SerOP_Nickname3,SerOP_name3,ServiceOP,ServiceOP2,ServiceOP3} = op1;
					FoodID && arr2.push({FoodID,FoodName,GID,Item_ID,SerOP_Nickname,SerOP_name,SerOP_Nickname2,SerOP_name2,SerOP_Nickname3,SerOP_name3,ServiceOP,ServiceOP2,ServiceOP3});
					obj1.arrService = arr2;
				})
				arrObj1.push(obj1);
			}
		});
		// console.warn('setServiceItemList: ', arrObj1);	// @@
    state.baseInfo.publicData.serviceItemList = arrObj1 || [];
  },
  // 存入企業 app 的 db connect 資訊
  setAppConnect(state, data) { const connect = data || '9001' ; state.publicData.connect = connect },
  // 存入首頁主畫面可滑動的 Banners
  setIndexSlideBanners(state, data) { state.publicData.index.slideBanners = data },
  // 存入首頁主畫面可滑動的 Banners - 五互
  setIndexSlideBannersWuhu(state, data) { state.baseInfo.publicData.index.slideBanners = data },
  // 存入首頁固定的副 Banner
  setIndexSubBanner(state, data) { state.publicData.index.subBanner = data },
  // 五互 - 存入首頁固定的副 Banner
  setIndexSubBannerWuhu(state, data) { state.baseInfo.publicData.index.subBanner = data },
  // 存入企業門店清單
  setStoreListData(state, payload) {
    const {stores, MainEnterPriseID, MainConnect} = payload
    if (MainEnterPriseID) state.publicData.MainEnterPriseID = MainEnterPriseID
    if (MainConnect) state.publicData.MainConnect = MainConnect
    if (stores) state.publicData.storeList = stores
  },
  setStoreModeType(state, payload) {
    const {p_modeType} = payload
    state.publicData.modeType = p_modeType
  },
  //存入英特拉金流回傳值
  setAppThirdPartyPay(state, data) { state.mallData.thirdPartyPay = data },
  // 存入企業門店清單
  setStoreListDataWuhu(state, payload) {
    let {stores/*, MainEnterPriseID, MainConnect*/} = payload
    // state.publicData.MainEnterPriseID = MainEnterPriseID
    // state.publicData.MainConnect = MainConnect

    state.baseInfo.publicData.storeList = stores
  },
  // 存入品牌資訊
  setBrandInfo(state, data) { if (data) state.publicData.brandInfo = data },
  // 存入最新消息的資料
  setNewsData(state, data) { if (!data.ErrorCode) state.publicData.news.data = data },
  // 存入最新消息的資料
  setNewsDataWuhu(state, data) { if (!data.ErrorCode) state.baseInfo.publicData.news.data = data },
  // 存入最新消息的分類
  setNewsTypes(state, data) { if (!data.ErrorCode) state.publicData.news.types = data },
  // 存入菜單的大類資料
  setMenuMainTypeListData(state, payload) { state.publicData.menu.mainType.find(item => {return item.code === payload.code}).list = payload.data},
  //存入全部小類下商品
  setFoodmarketData(state, data) {
    const foodCheck=(foods,isSetFood)=>{return foods.map(p_food=>{
      p_food.ID = p_food.ID || p_food.MainID;  //品項ID
      p_food.Price = p_food.CurPrice;    //原價
      if (isSetFood && !state.mallData.foodInfo[p_food.ID]) state.mallData.foodInfo[p_food.ID] = p_food;
      // p_food.Price1 = (p_food.Price1 != undefined?p_food.Price1:9999);  //售價

      return p_food;
    });}

    if (Array.isArray(data.foods)) {
      state.mallData.iv_foods[data.kindID] = foodCheck(data.foods,true);

      const findKind = (et) => et.ID == data.kindID;
      var kind_index = state.mallData.foodkind.findIndex(findKind);
      state.mallData.foodkind[kind_index]["foods"] = foodCheck(data.foods);
      // console.log("setFoodmarketData",data.kindID,state.mallData);
    }

  },
  setMenuMainTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.mainType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.mainType = state.publicData.menu.mainType.concat( { code: data.code, name: data.name, list: [] } )
  },
  // 存入菜單的小類資料
  setMenuSubTypeListData(state, payload)  {state.publicData.menu.subType.find(item => {return item.code === payload.code}).list = payload.data},
  setMenuSubTypeCodeName(state, data) {
    const noHaveData = (state.publicData.menu.subType.find(item => {return item.code === data.code}) === undefined)
    if (noHaveData) state.publicData.menu.subType = state.publicData.menu.subType.concat( { code: data.code, name: data.name, list: [] } )
  },
  setCurrentProduct(state, data) {
    if (data && data.length>0) {
      state.publicData.currentProduct = data[0]
    }
  },
  // 存入會員卡資訊
  setMemberVipData(state, data) {
    state.memberData.vip_info = data.querydata_return_info.vip_info
    state.memberData.card_info = data.querydata_return_info.card_info
    state.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    state.memberData.expiredPoints_section = data.ExpiredPoints_section // 年度總積點資料

    //PointMoneyAmount體驗點數,Balance消費點數,在此沒有
  },
  // 存入會員卡資訊 - 五互
  setMemberVipDataWuhu(state, data) {

		/** @@Test 積分換券 */
		// let json1 = await GetJsonTest('PointToTickets')	// @@
		// data.PointToTickets = json1

    state.baseInfo.memberData.vip_info = data.querydata_return_info.vip_info
    state.baseInfo.memberData.card_info = data.querydata_return_info.card_info
    state.baseInfo.memberData.pointToTickets = data.PointToTickets // 積分換卷的資料
    //console.log("優惠券總數data>>",state.baseInfo.memberData.vip_info.TicketCount);
    // === only for test start ===
    // data.ExpiredPoints_section = [{
				// ShopID: "TX07",
				// ShopName: "香繼光測試機B",
				// Points: "68.00",
				// ExpiredDate: "2020-12-31"
			// }, {
				// ShopID: "TX07",
				// ShopName: "香繼光測試機B",
				// Points: "6.00",
				// ExpiredDate: "2021-12-31"
			// }
    // ]
    // === only for test end ===

    // === 排序 start ===
    let expiredPoints_section = data.ExpiredPoints_section
    if (expiredPoints_section && expiredPoints_section.length>0) {
      if (expiredPoints_section.length>1) {
        // 排序: 由小到大
        expiredPoints_section.sort(function (a, b) {
          if (a.ExpiredDate > b.ExpiredDate) {
            return 1
          } else {
            return -1
          }
        })
      }
    }
    // === 排序 end ===

    state.baseInfo.memberData.expiredPoints_section = expiredPoints_section // 年度總積點資料
  },
  // 設定 卡包,目前scrollTop
  setCardListScrollMap(state, value){ state.cardListScrollMap = value },
  // 存入卡片條碼 QRcode
  setCardQRCodeData(state, data) { state.cardQRCode = data },
  //
  setShowQrcode(state, data) { state.ShowQrcodeFn = data },
  //卡包分享QRcode
  setCardShareQRCodeData(state, data) { state.cardShareQRCode = data },
  // 設定簡訊驗證碼
  setSMSreCode(state, data) { state.SMS_Config.reCode = data },
  // 存入會員的購買交易紀錄
  setMemberPurchaseRecordData(state, data) { state.memberData.purchaseRecord = data },
  // 存入優惠卷列表資訊
  setMemberTicketListData(state, data) {state.memberData.ticketList = data},
  // 存入優惠卷列表資訊 - 五互
  setMemberTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.ticketList = data
  },
  // 存入已贈送的優惠卷列表資訊
  setMemberGaveTicketListData(state, data) {state.memberData.gaveTicketList = data},
  // 存入已贈送的優惠卷列表資訊 - 五互
  setMemberGaveTicketListDataWuhu(state, data) {
    state.baseInfo.memberData.gaveTicketList = data
  },
  // 存入尚未能使用的優惠卷資料
  setMemberNotAvailableTickets(state, data) {state.memberData.notAvailableTickets = data},
  // 存入尚未能使用的優惠卷資料 - 五互
  setMemberNotAvailableTicketsWuhu(state, data) {
    state.baseInfo.memberData.notAvailableTickets = data
  },
  // 存入會員的積點紀錄
  setMemberExpiredPointsData(state, data) { state.memberData.expiredPoints = data },
  // 存入會員的積點紀錄
  setMemberExpiredPointsDataWuhu(state, data) { state.baseInfo.memberData.expiredPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointsDataWuhu(state, data) { state.baseInfo.memberData.consumerPoints = data },
  // 存入會員的消費點數紀錄
  setMemberConsumerPointDetailWuhu(state, data) {
    if (data) {
      // 手動加入readMore開闔欄位
      data.forEach((item)=>{
        item.readMore = false // 預設是闔
        item.orderDetail = null // 該筆訂單的購物清單
      })
    }

    state.baseInfo.memberData.consumerPointDetail = data
  },
  // 存入會員的單筆訂單紀錄
  setOneOrderDetail(state, data) {
    if (data) {
      let currentItem = state.baseInfo.memberData.consumerPointDetail[data.arrayIndex]
      if (currentItem) {
        currentItem.orderDetail = data.orderDetail
      }
    }
  },
  // 存入優惠卷明細資訊 e.g. 規則條款、期限、適用範圍 ... etc
  setMemberTicketInfoData(state, data) {
    const ticket = state.memberData.ticketInfo
    // 規則代號
    if (data.currentTypeCode) ticket.currentTypeCode = data.currentTypeCode
    if (data.currentRuleCode) ticket.currentRuleCode = data.currentRuleCode
    // 使用規則、條款
    if (data.ruleData)   ticket.ruleData = data.ruleData
    // 適用商品
    if (data.usageFoods) ticket.usageFoods = data.usageFoods
    // 適用門店
    if (data.usageShops) ticket.usageShops = data.usageShops
  },
  // 存入優惠卷條碼 QRcode
  setTicketQRCodeData(state, data) {
    state.memberData.ticketInfo.currentTicketGID    = data.gid
    state.memberData.ticketInfo.currentTicketQRcode = data.image
  },
  // 存入殼的版本是否需要更新
  SetDeviseVersionIsDiff(state, status) {
    const value = (status === '1') ? true : false
    state.baseInfo.DeviseVersionIsDiff = value
  },
   // 存入購物車
  setShopCart(state, data) { state.mallData.shopping_cart = data },
   // 新增購物車
  addShopCart(state, data) { state.mallData.shopping_cart.push(data) },
   // 付款方式清單
  setCheckKind(state, data) { state.mallData.checkKind_list = data },
  // 商城觸發UI更新用
  turnShopCart(state) {state.mallData.isCarUI = !state.mallData.isCarUI},
  // 商城分組商品更新
  groupFood(state, data) {state.mallData.home[data.fIndex]["type"] = data.GroupItems;},
   // 存入商品詳情頁
  setShopNowItem(state, data) {state.mallData.nowItem = data},
  // 商城存入小類商品
  setShopKindFoods(state, data) {state.mallData.kind[data.index]["foods"] = data.foods},
  // 商城存入訂單記錄
  setShopRecord(state, data) { state.mallData.orders = data },
  // 商城存入訂單記錄,索引特定index
  setShopRecordIndex(state, data) { state.mallData.orders[data.index] = data.Obj },
  // 商城存入訂單記錄,分頁
  setShopRecordPage(state, data) { state.mallData.ordersPage = data },
  // 商城存入訂單記錄,分頁
  setShopUndRecordPage(state, data) { state.mallData.unOrdersPage = data },
  //付款狀態
  setPayStatus(state, data) {
    if(Array.isArray(data)){
      data.map((p_status)=>{
        const p_key =p_status.id.toString();
        state.mallData.payStatus[p_key] = p_status.text;
      });

    }
  },
  // 商城小類頁品項排序
  setSortPriceType(state, data) {state.mallData.sortPriceType = data},
  // 商城小類頁scrollListener
  setShoplistenELe(state, data) {state.mallData.listenEL = data},
  // 存入單筆未完成訂單記錄
  setSingleUnFinish(state, data) {
    state.mallData.singleUnFinish = data
  },
  // 存入單筆已經完成訂單記錄
  setSingleFinish(state, data) {
    state.mallData.singleFinish = data
  },
  // 取回單筆已經選好的超商info
  setCvsShopInfo(state, data) {
    state.mallData.cvsShopInfo = data;
  },
  setBecomeApp(state) {
    state.mallData.becomeApp = !state.mallData.becomeApp
  },
  // 商城清除暫存
  clsShopMall(state) {
    state.mallData.foodkind2 = [];  //大類
    state.mallData.foodkind = [];   //小類
    state.mallData.iv_foods = {}; //小類下的所有品項
    state.mallData.iv_styles = {}; //品項下的所有規格
    state.mallData.foodInfo = {}; //by foodID 品項基本資料
  },
  initQrCodeItem(state){
    //由掃瞄生成的品項
    state.mallData.barcodeItem = [{ID:"T001",Name:"消費體驗高雄館-餐點消費",Price1:''},{ID:"T002",Name:"消費體驗高雄館-商品消費",Price1:''}];
  },
  setGridView() { state.gridView = !state.gridView },
}

const getters = {
  getBaseUrl : () => {
    //console.log('getBaseUrl ======> state.language =>' + state.language)
    if (state.language == 'cn') baseUrl = 'kakar2.jinher-cn.com'
    return baseUrl
  },
	requestHead: () => {
		return { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
	},
  fontSizeBase: state => {
    let fontSize = state.fontSizeBase
    if (!fontSize || isNaN(fontSize) || parseFloat(fontSize)<=0) {
      fontSize = 1 // 預設1
    }
    return parseFloat(fontSize)
  },
  // 站台 router
  router: state => {
    // if (state.appSite === 'wuhulife') return routerWuhulife
    if (state.appSite === 'kakar2') return routerKakar2
    // return routerKakar
  },
  isNotLinePara:() => {return isNotLinePara()},
  // 是否登入
  isLogin: state => {
    return !!(state.member.code && state.member.mac && state.member.Tokenkey);
  },
  isLoginChild: state => {
    return !!(state.member.code && state.currentAppUser.token);
  },
  // 卡卡的 requestBody
  kakarBody: state => (body) =>{
    const inputBody = ( body || {} )
    const {mac, code} = state.member
    const {isFrom, EnterpriseID, FunctionID} = state.baseInfo
    const basicBody = { FunctionID, EnterpriseID, isFrom, mac, Account: code,Tokenkey: state.member.Tokenkey}
    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBody: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, isFrom, EnterPriseID, token} = state.currentAppUser
    const p_token = (isFrom === state.baseInfo.isFrom ? state.member.Tokenkey : token);
    const basicBody = {
      mac, isFrom,
      EnterpriseID: EnterPriseID,
      Account: state.member.code,
      Tokenkey:p_token,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody
  appBodyWuhu: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }
    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      Account: state.member.code,
    }
    return Object.assign({}, basicBody, inputBody)
  },
  // 企業 app 的 requestBody-卡加專用
  appBodykaka: state => (body) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
			mac: state.currentAppUser.mac,
			Tokenkey: state.currentAppUser.token,
			isFrom: state.currentAppUser.isFrom
    }
		const basicBody = {
			mac, isFrom, Tokenkey,
			EnterPriseID: state.currentAppUser.EnterPriseID,
			Account: state.member.code,
		}
    return Object.assign({}, basicBody, inputBody)
  },
	// 卡包企業 app 的 requestBody
	appBodyEncode_sub: state => (body, paramBody) => {
		const inputBody = ( body || {} )
		const {mac, Tokenkey, isFrom} = {
			mac: state.currentAppUser.mac,
			Tokenkey: state.currentAppUser.token,
			isFrom: state.currentAppUser.isFrom
		}
		// === param start ===
		const paramInputBody = (paramBody || {})
		let paramData = {
			Account: btoa(state.member.code.toString())
		}
		let paramEndData = Object.assign({}, paramData, paramInputBody)
		// === param end ===
		const basicBody = {
			mac, isFrom, Tokenkey,
			EnterPriseID: state.currentAppUser.EnterPriseID, // 注意2個P大小寫不同
			param: paramEndData,
			Account: state.member.code,
		}
		return Object.assign({}, basicBody, inputBody)
	},
  // 企業 app 的 requestBody
  appBodyWuhuEncode: state => (body, paramBody) => {
    const inputBody = ( body || {} )
    const {mac, Tokenkey, isFrom} = {
      mac: state.member.mac,
      Tokenkey: state.member.Tokenkey,
      isFrom: state.baseInfo.isFrom
    }

    // === param start ===
    const paramInputBody = (paramBody || {})
    let paramData = {
      Account: btoa(state.member.code)
    }
    let paramEndData = Object.assign({}, paramData, paramInputBody)
    // === param end ===

    const basicBody = {
      mac, isFrom, Tokenkey,
      EnterPriseID: state.baseInfo.EnterpriseID, // 注意2個P大小寫不同
      param: paramEndData,
    }

    return Object.assign({}, basicBody, inputBody)
  },
  // 目前的企業列表 (使用 state.appFilter 篩選 + 依據點擊次數排序)
  currentAppUsers: state => {
    let appUsers = state.vipUserData
    // 依據點擊次數排序
    appUsers = appUsers.sort((a, b) => {
      const aClicks = state.appClicks[a.EnterPriseID] || 0
      const bClicks = state.appClicks[b.EnterPriseID] || 0
      if (aClicks > bClicks) return -1
      if (aClicks < bClicks) return 1
      if (aClicks === bClicks) return 0
    })
    // 使用 state.appFilter 篩選
    if (state.appFilter === '') return appUsers
    const eIDs = state.vipApps.filter(item => {
      const filter  = state.appFilter.trim().toLowerCase()
      const eID     = item.EnterpriseID.trim().toLowerCase()
      const isFrom  = item.isFrom.trim().toLowerCase()
      const AppName = item.AppName.trim().toLowerCase()
      return RegExp(filter).test(eID) || RegExp(filter).test(isFrom) || RegExp(filter).test(AppName)
    }).map(item => item.EnterpriseID)
    return state.vipUserData.filter(item => eIDs.find(eID => (eID === item.EnterPriseID)) )
  },
  currentAppUsersWuhu: state => {
    let appUsers = state.vipUserData.sort((a, b) => {
      if (a.EnterPriseID < b.EnterPriseID) {
        return 1;
      }
      if (a.EnterPriseID > b.EnterPriseID) {
        return -1;
      }
    })
    // 依據點擊次數排序
    // appUsers = appUsers.sort((a, b) => {
    //   const aClicks = state.appClicks[a.EnterPriseID] || 0
    //   const bClicks = state.appClicks[b.EnterPriseID] || 0
    //   if (aClicks > bClicks) return -1
    //   if (aClicks < bClicks) return 1
    //   if (aClicks === bClicks) return 0
    // })

    // 使用 state.appFilter 篩選
    if (state.appFilter === '') return appUsers
    const eIDs = state.vipApps.filter(item => {
      const filter  = state.appFilter.trim().toLowerCase()
      const eID     = item.EnterpriseID.trim().toLowerCase()
      const isFrom  = item.isFrom.trim().toLowerCase()
      const AppName = item.AppName.trim().toLowerCase()
      return RegExp(filter).test(eID) || RegExp(filter).test(isFrom) || RegExp(filter).test(AppName)
    }).map(item => item.EnterpriseID)
    return appUsers.filter(item => eIDs.find(eID => (eID === item.EnterPriseID)) )
  },
  // 目前的企業 App
  currentApp: state => {
    return state.vipApps.find(item => item.EnterpriseID === state.currentAppUser.EnterPriseID)
  },
  connect: (state) => {
    return state.publicData.connect
  },
  // 企業 app 的 memberApiUrl
  appMemberApiUrl: (state, getters) => {
    return getters.currentApp ? getters.currentApp.apiip : ""
  },
  // 企業 app 的 memberApiUrl - 五互
  appMemberUrlWuhu: (state) => {
    return state.api.memberUrl
  },
  // 企業 app 的 預約資料 - 五互
  appBookUrlWuhu: (state) => {
    return state.api.bookUrl
  },
  // 企業 app 的 預約token資料 - 五互
  appTokenUrlWuhu: (state, getters) => {
   //console.log("**>>appTokenUrlWuhu",getters.getBaseUrl,state.currentAppUser.apiip);
    if (getters.subSrcUrl == '') return `https://${getters.getBaseUrl}/public/AppBookingOneEntry.ashx`
    return `${getters.subSrcUrl}/public/AppBooking.ashx`

    //return state.api.tokenUrl
  },
  // 企業 app 的 publicApiUrl
  appPublicApiUrl: (state, getters) => {
    return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '/public/AppNCW.ashx')
  },
  // 靜態檔的 url
  srcUrl: (state, getters) => {
    if (getters.currentApp && getters.currentApp.apiip)
    return getters.currentApp.apiip.replace('/public/AppVIP.ashx', '')

    return '';
  },
  // 靜態檔的 url
  subSrcUrl: (state,) => {
    return (state.currentAppUser.apiip?state.currentAppUser.apiip.replace('/public/AppVIP.ashx', ''):'')
  },
  // 卡包商城url
  // subBookingUrl: () => {
  subBookingUrl: (state) => {
		const apiip	= state.currentAppUser.apiip || ''
			,_file 		= 'AppVIP.ashx'
			,hasFind 	= apiip.indexOf(_file) > -1

		return hasFind ? apiip.replace(_file,'AppBooking.ashx') : state.api.tokenUrl
/*
 */
		// return 'https://8012test.jh8.tw/Public/AppBooking.ashx'	// @@
  },
  // 卡包url,[開發接口API(季河)->龍海APP->龍海APP接口(950201)]
  subConsumerUrl: (state) => {
		const apiip	= state.currentAppUser.apiip || ''
			,_file 		= 'AppVIP.ashx'
			,hasFind 	= apiip.indexOf(_file) > -1
		return hasFind ? apiip.replace(_file,'AppDataVIP.ashx') : state.api.consumerPubUrl
  },
  subTokenApiUrl: (state) => {
		const apiip	= state.currentAppUser.apiip || ''
			,_file 		= 'AppVIP.ashx'
			,hasFind 	= apiip.indexOf(_file) > -1
		// console.log('subTokenApiUrl-apiip: ', apiip);	// @@
    if (apiip) {
			return hasFind ? apiip.replace(_file,'AppDataVIP.ashx') : apiip
    } else {
      return 'https://8012.jh8.tw/Public/AppVipOneEntry.ashx'
    }
  },
  appVipUrl: (state) => {
    //會員資料存取,例:收件人存取 by 20200225
    return state.api.vipUrl
  },
  // 取手機的螢幕亮度值
  deviseBrightness: state => {
    if (state.baseInfo.brightness <= 70) return 120
    return state.baseInfo.brightness
  },
  //  五互[我的預約]歷史紀錄
  getMyBookList: (state) => {
    return state.baseInfo.publicData.myBookList
  },
  //  五互[我的預約]狀態清單
  getMyBookStatusList: (state) => {
    return state.baseInfo.publicData.myBookStatusList
  },
  getLineTk: (state) => {
    const p_head = { "headers": { "Content-Type": "application/x-www-form-urlencoded;" } }
		const hostN = window.location.hostname == 'localhost'?'localhost:8080':window.location.hostname;
		const redirect_name = window.location.protocol + "//" + hostN + window.location.pathname;
		const p_token = {
				grant_type: "authorization_code",
				code: state.code,
				redirect_uri: redirect_name,
				client_id: state.lineLogin.id,
				client_secret: state.lineLogin.sc }
    return Object.assign({}, {head:p_head,tokenObj:p_token})

  } //,
  // getCustomModal: (state) => {
  //   return state.customModal
  // }
}

const actions = {
  /* 卡卡資料相關 */
  // init 時執行所需的 action
  async fetchInitData() {
    // 1. 跟殼取資料
    await ensureDeviseSynced()
  },
  // (卡卡) 確認會員帳號是否未註冊
  async ensureMemberCodeNotRegistered({state,getters}, account) {

    // const url = 'https://kakar.jinher.com.tw/public/AppVIP.ashx'
    // const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const url = `https://${baseUrl}/public/AppVIP.ashx`
    const body = { act: "memberchk", "isFrom":state.baseInfo.isFrom, "EnterpriseID":state.baseInfo.EnterpriseID, Account: account }
    const head = getters.requestHead
    const data = await fetchData({url, body, head})

    return new Promise(resolve => resolve(data))
  },
  // (卡卡) 會員註冊
  // async ensureMemberRegister({state,getters}, payload){
  async ensureMemberRegister({state}, payload){
    // const url = state.api.memberUrl
    const url = state.api.consumerPubUrl	// 2022.01.14 秋哥說應該打這支,針對"性別"問題
    // const url = `https://${getters.getBaseUrl}/Public/AppVipOneEntry.ashx`;
    const body = Object.assign({}, { act: "memberreg", "isFrom": state.baseInfo.isFrom, "EnterpriseID": state.baseInfo.EnterpriseID }, payload)
    const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
    const {data} = await axios.post( url, body, head )
    return new Promise(resolve => resolve(data))
  },
	// 取預設卡包門店
	async cardSySetting({state}){
		if (state.currentAppUser.apiip.indexOf('/public/AppVIP.ashx') > -1) {
			const hotp = state.currentAppUser.apiip.replace('/public/AppVIP.ashx','')
			//const url = `https://${hotp}/Public/GetAppSysConfig.ashx?EnterpriseID=${state.currentAppUser.EnterPriseID}&AppSysName=APPVipSaleShopID`
			// const {data} = await axios.post( url, body, head )
			const {data} = axios.get(`${hotp}/Public/GetAppSysConfig.ashx`, { params: { EnterpriseID: state.currentAppUser.EnterPriseID, AppSysName: 'APPVipSaleShopID'} })

			return data
		}else{
			return {};
		}
	},
  // (卡卡) LineLogin v2.1,主企業kakar
	async memberLineLogin_v2({state,commit},formData) {
    if (!formData) return;
    const {EnterpriseID, path, appQrcode} = formData;
    if (!EnterpriseID || path === undefined || appQrcode === undefined) return;
    setLoading(true)
    const hostN = window.location.hostname == 'localhost'?'localhost:8080':window.location.hostname;
		const redirect_name = window.location.protocol + "//" + hostN + window.location.pathname;
    const redirect_uri = encodeURIComponent(redirect_name);
    const url = "https://surl.jh8.tw/AppShortURL.ashx"
    const surlID = makeid_num(6);
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
		const body = { act: "SetUrl",
									EnterPriseID: EnterpriseID,
									BackData: JSON.stringify({
										nonce:surlID,
                    EnterpriseID:EnterpriseID,
                    path:path,
                    appQrcode:appQrcode,
                  }),
									QrCodeType: "2",//1:url直接轉址,2:只運行 BackData
									ExpiredType: "1" //1:限1次,0:無限,2:限3天
								};
    const {data} = await axios.post( url, body, head )
    commit('setLoading', false)
    if (data.ErrorCode != '0') return showAlert(data.ErrorMsg || "")
		var p_state = (data.Remark || '');
		var lineUrl = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${state.lineLogin.id}&redirect_uri=${redirect_uri}&state=${p_state}&scope=openid%20profile%20email&nonce=${surlID}`;

		window.open(lineUrl, "_self");
	},
	// (卡卡) LineLogin,主企業kakar
	async memberLineLogin({state,commit}) {
		setLoading(true)
		const hostN = window.location.hostname == 'localhost'?'localhost:8080':window.location.hostname;
		const redirect_name = window.location.protocol + "//" + hostN + window.location.pathname;
		const surlID = makeid_num(6);
		const redirect_uri = encodeURIComponent(redirect_name);
		const url = "https://surl.jh8.tw/AppShortURL.ashx"
		const {EnterpriseID} = state.baseInfo
		const body = { act: "SetUrl",
									EnterPriseID: EnterpriseID,
									BackData: JSON.stringify({
										nonce:surlID,}),
									QrCodeType: "2",//1:url直接轉址,2:只運行 BackData
									ExpiredType: "1" //1:限1次,0:無限,2:限3天
								};

		//const data 	= await fetchData({url, body})
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
		const {data} = await axios.post( url, body, head )
		commit('setLoading', false)
		if (data.ErrorCode != '0') return showAlert(data.ErrorMsg || "")
		var p_state = (data.Remark || '');

		if (p_state == "") return;
		//var lineUrl = `https://access.line.me/oauth2/v2.1/authorize?response_type=code&client_id=${state.lineLogin.id}&redirect_uri=${redirect_uri}&state=${p_state}&scope=openid%20profile%20email&nonce=${surlID}`;
    var lineUrl = `https://access-auto.line.me/dialog/oauth/weblogin?response_type=code&client_id=${state.lineLogin.id}&redirect_uri=${redirect_uri}&state=${p_state}`;
		window.open(lineUrl, "_self");
	},
  async calcuSurl({state,commit}, formData) {
    formData.qrType = formData.qrType || "2";
    formData.exType = formData.exType || "1";
    formData.data = formData.data || "{}";
    setLoading(true)
    const url = "https://surl.jh8.tw/AppShortURL.ashx"
		const {EnterpriseID} = state.baseInfo
		var body = { act: "SetUrl",
									EnterPriseID: EnterpriseID,
									BackData: formData.data,
									QrCodeType: formData.qrType,//1:url直接轉址,2:只運行 BackData
									ExpiredType: formData.exType //1:限1次,0:無限,2:限3天
								};
    if (formData.url) body.BackUrl = formData.url;
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
		const {data} = await axios.post( url, body, head )
		commit('setLoading', false)
		if (data.ErrorCode != '0') showAlert(data.ErrorMsg || "")
    if (IsFn(formData.fn)) formData.fn((data.Remark || ''));
		return (data.Remark || '');
  },
  async goSurl({commit}, formData) {
    //限只會back data的jinher surl
		setLoading(true)
		await axios.post( `https://surl.jh8.tw/${formData.sUrl}`)
		.then((res) => {
			commit('setLoading', false);
      IsFn(formData.fn) && formData.fn(res.data);

		}).catch( () => {
      IsFn(formData.fn) && formData.fn({});
			commit('setLoading', false)
		})

	},
	// (卡卡) 第三方登入,以LINE log in為主
	async memberThirdCheck({dispatch,commit}, formData) {
		setLoading(true)
		//短網址取得nonce(linelogin by jinher check)
		await axios.post( `https://surl.jh8.tw/${formData.state}`)
		.then((res) => {
			commit('setLoading', false);
			const dataSurl = res.data;
			if (!dataSurl.nonce) {
				return //showAlert(dataSurl.ErrorMsg || "error")
			}

      if (dataSurl.appQrcode != undefined) {
        commit('setLineLoginKey', {key:"appQrcode",value:dataSurl.appQrcode}); //外部呼叫開卡包代碼
        if (dataSurl.EnterpriseID) commit('setLineLoginKey', {key:"EnterpriseID",value:dataSurl.EnterpriseID});
        if (dataSurl.path) commit('setLineLoginKey', {key:"path",value:dataSurl.path});
        dispatch('LineThirdToken_v2', Object.assign({}, formData, dataSurl ))
			}else{
        dispatch('LineThirdToken', Object.assign({}, formData, dataSurl ))
      }

		}).catch( () => {
			commit('setLoading', false)
		})

	},
  // (卡卡) 第三方登入,取line token v2.1
	async LineThirdToken_v2({dispatch,state}, formData) {
		setLoading(true)

    const {head,tokenObj} = getters.getLineTk(Object.assign({}, formData, state));

		//取linelogin token
    //accessToken
		await axios.post( `https://api.line.me/oauth2/v2.1/token`, Qs.stringify(tokenObj),head )
		.then((res) => {
			setLoading(false);
			const p_data = res.data;
			if (!p_data.id_token) {return} //v2.1
			dispatch('LineThirdVerify', Object.assign({}, formData, { token: p_data.id_token } )) //v2.1

		}).catch( () => {
			setLoading(false);
		});

	},

	// (卡卡) 第三方登入,取line token
	async LineThirdToken({dispatch,state, commit}, formData) {
		setLoading(true)
		const {head,tokenObj} = getters.getLineTk(Object.assign({}, formData, state))

		//取linelogin token
    //accessToken
    await axios.post( `https://api.line.me/v2/oauth/accessToken`, Qs.stringify(tokenObj),head )
		//await axios.post( `https://api.line.me/oauth2/v2.1/token`, Qs.stringify(p_token),head )
		.then((res) => {
			commit('setLoading', false);
			const p_data = res.data;
      if (!p_data.access_token) {return}
			dispatch('LineThirdProfile', Object.assign({}, formData, { token: p_data.access_token } ))
			//if (!p_data.id_token) {return} //v2.1
			//dispatch('LineThirdVerify', Object.assign({}, formData, { token: p_data.id_token } )) //v2.1

		}).catch( () => {
			commit('setLoading', false)
		});

	},
  // (卡卡) 第三方登入,LINE web profile
	async LineThirdProfile({dispatch, commit}, formData) {
		setLoading(true)
    var head = { "headers": { "Content-Type": "application/x-www-form-urlencoded;" } }
		//const head = { "headers": { "Authorization": `Bearer ${formData.token}` } }
    if (formData.token && formData.token!=""){
      head.headers["Authorization"] = `Bearer ${formData.token}`;

    }

		//取linelogin info
		await axios.post( `https://api.line.me/v2/profile`, {},head )
		.then((res) => {
			commit('setLoading', false);
			var p_data = res.data;
			//if (!p_data.sub) {return}
      if (!p_data.userId) {return}
      p_data.sub = p_data.userId;
			p_data.tdType = "LINElogin";
      p_data.name = p_data.displayName;
      p_data.email = p_data.email || "";

			commit('setLineLoginInfo', {token:formData.token, verify:p_data});
			dispatch('memberThirdLogin', { sub: p_data.sub ,isFromTD: p_data.tdType})

		}).catch( () => {
			commit('setLoading', false)
		});
	},
    // (卡卡) 第三方登入,LINE ,app殼登入
	async LineThirdMobileApp({dispatch, commit}, formData) {
    var p_data = formData;
    //if (!p_data.sub) {return}
    if (!p_data.userid || !p_data.accesstoken) {return}
    p_data.sub = p_data.userid;
    p_data.tdType = "LINElogin";
    p_data.name = p_data.displayname;
    p_data.email = p_data.email || "";

    commit('setLineLoginInfo', {token:p_data.accesstoken, verify:p_data});
    dispatch('memberThirdLogin', { sub: p_data.sub,isFromTD: p_data.tdType})
	},
	// (卡卡) 第三方登入,LINE verify
	async LineThirdVerify({dispatch,state, commit,getters}, formData) {
		setLoading(true)
		const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded;" } }
		const p_token = { id_token: formData.token,
					nonce: formData.nonce,
					client_id: state.lineLogin.id }

		//取linelogin verify
		await axios.post( `https://api.line.me/oauth2/v2.1/verify`, Qs.stringify(p_token),head )
		.then(async (res) => {
			commit('setLoading', false);
			var p_data = res.data;
			if (!p_data.sub) {return}
			p_data.tdType = "LINElogin";
			commit('setLineLoginInfo', {token:formData.token, verify:p_data});
      const isNotify = !state.lineLogin.isLoadNotify && state.lineLogin.path === "cardShop" && !state.searchURL.lineNofInfo;

			await dispatch('memberThirdLogin', { sub: p_data.sub ,isFromTD: p_data.tdType, isOnlyLoad:isNotify})
      if (!getters.isLogin) return;
      if (isNotify && !state.userData.RegId) await dispatch('memberLineNotifySys', { EnterpriseID: state.lineLogin.EnterpriseID,"isGoHome":true})//取參數
      else getters.router.push('/')
		}).catch( () => {
			commit('setLoading', false)
		});
	},
  async memberThirdChildRegister({state, commit}, formData) {
		//第三方註冊(卡包子企業)
		setLoading(true)
		const {isFrom, EnterpriseID, FromTDID} = formData
    const {MT_NickName, FromTDEmail} = state.member
		const url = state.api.consumerPubUrl//memberbaseUrl
		const body = {
			isFrom, EnterpriseID,FromTDID,FromTDEmail,
			"act": "membersthirdreg",
			"isFromTD": "LINElogin",
      Account:state.member.code,
      FromTDNickName:MT_NickName,
		}
		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		return new Promise(resolve => resolve(data))
	},
  async pathCheck({dispatch,state, commit, getters}, one){
		const linePath 	= state.lineLogin.path
			,ePid 				= one.EnterPriseID
		// IsDev && console.warn('pathCheck-isFromTest, linePath: '+S_Obj.isFromTest(), linePath);	// @@
    //前往何處路由(短網址解析有path的.)

    //if (!state.lineLogin.isScan && linePath != "memberQrCode"){ //&& linePath != "orders"
    if (!state.lineLogin.isScan){
      setTimeout(function() {
        commit('setLineLoginKey', {key:"appQrcode",value:""});
        commit('setLineLoginKey', {key:"isScan",value:true});
      }, 3000);
     //限定member為第三方登入才有效
      // if (linePath == "member") 			getters.router.push('/cardMember/'+ePid);

      if (linePath == "member") 			location.href = '#/cardMember/'+ePid;
      else if (one && linePath == "memberQrCode") 		{await dispatch('fetchVipAppData', {app: one});  getters.router.push('/card/'+ePid);}
      else if (linePath == "news") 		getters.router.push('/cardNews/'+ePid);
      else if (linePath == "shops")		getters.router.push('/cardStore/'+ePid);
      else if (linePath == "bookingClean") 	getters.router.push('/bookingClean/'+ePid);
      else if (linePath == "bookingMine") 	getters.router.push('/bookingMine/'+ePid);
      else if (linePath == "cardCoupon") 		getters.router.push('/cardCoupon/'+ePid);
      else if (linePath == "faq") 		getters.router.push('/cardFaq/'+ePid+'?name=常見問題');
			else if (linePath == "serviceRec") 		getters.router.push('/serviceRec/'+ePid);
      else if (linePath == "cardShop"){
        //商城
        //"cardShop"
        getters.router.push('/cardShop/'+ePid);
        const p_regid = state.lineLogin.RegId;
        if (state.searchURL.lineNofInfo && p_regid) await dispatch('memberThirdRegisterID',p_regid)

      }
      else if (linePath == "orders"){
        if (state.currentAppUser && !state.publicData.MainConnect) await dispatch('fetchStoreMainInfo')
        // 導向我的訂餐
        const p_data =  {
          EnterpriseID: ePid,
          Mobile:state.userData.Account,
          Username:state.userData.Name,
          mac:state.currentAppUser.mac,
          connect:state.publicData.MainConnect,
          lang:state.language,
          Address:state.userData.Address,
        }
        showMOrder('', '', 'card', p_data);
      }

    }else{
      getters.router.push({path: `/card/${ePid}`})
    }
  },
  async memberLineNotifySys({state, dispatch, commit}, formData){
    //取得line notify 參數 ,用卡包企業的設定,但存入主kakar的memberthird.regid?,待商確(線上點餐是用此方式)
    if (isNotLinePara() || state.userData.RegId) return;  //一般使用(非LINE應用),不用取參數
    const {EnterpriseID,isGoHome} = formData
    if(EnterpriseID){
      commit('setLineLoginKey', {key:"isLoadNotify",value:true});
      const b_url = `https://8012.jh8.tw/public/GetAppSysConfigOneEntry.ashx?EnterpriseID=${EnterpriseID}&AppSysName=AppLineNofCt_id,AppLineNofCt_sc,AppLineNofCt_IsPush`
      await dispatch('calcuSurl',{qrType:"0",exType:"0",url:b_url ,fn:(p_code)=>{
        dispatch('getMemberLineNotifySys', { "code": p_code,"isGoHome":isGoHome})//取參數
      }});
    }

  },
  async getMemberLineNotifySys({state, dispatch, commit, getters}, formData){
    //取得line notify 參數 ,用卡包企業的設定,但存入主kakar的memberthird.regid?,待商確(線上點餐是用此方式)
    if (isNotLinePara()) return;  //一般使用(非LINE應用),不用取參數
    const {code,isGoHome} = formData
    if(code){
      await dispatch('goSurl',{sUrl:code,fn:(p_data)=>{
        if (p_data.indexOf("OK,") < 0 || p_data.indexOf("ErrorCode") > -1) return;
        p_data = JSON.parse("{"+(p_data || "").replace("OK,","").replace(/'/g, '"')+"}");
        let isNotify = false;
        if (p_data.AppLineNofCt_id && p_data.AppLineNofCt_IsPush != undefined){
          commit('setLineNotifyInfo', {
            id:p_data.AppLineNofCt_id,
            secret:p_data.AppLineNofCt_sc,
            isPush:(p_data.AppLineNofCt_IsPush.toLowerCase() == "true"?true:false)});

            //前往line notify page
            if (state.lineNotifyInfo.isPush && !state.userData.RegId){
              isNotify = true;
              dispatch('getLineNotify')
              //已取到notify info,以LINE應用首次進入的企業
            }
        }
        if (!isNotify && isGoHome) getters.router.push('/')
      }});
    }

  },
  async getLineNotify({state,dispatch}){
    if (!state.lineNotifyInfo.id || !state.lineNotifyInfo.secret || isNotLinePara()) return;
    const hostN = window.location.hostname == 'localhost'?'localhost:8080':window.location.hostname;
    var morder_surl = window.location.protocol + "//" + hostN + window.location.pathname;
    const {path, EnterpriseID, appQrcode} = state.lineLogin;
    if (!path || !EnterpriseID || !appQrcode) return;
    var p_send = {
      path, EnterpriseID, appQrcode,
      client_id: state.lineNotifyInfo.id,
      client_secret: state.lineNotifyInfo.secret
    };
    dispatch('calcuSurl',{data:JSON.stringify(p_send),qrType:"2",exType:"1",url:morder_surl ,fn:(p_code)=>{
      if (p_code == "") return;
      var lineUrl = 'https://notify-bot.line.me/oauth/authorize?response_type=code&scope=notify&response_mode=form_post&client_id=' + state.lineNotifyInfo.id + '&redirect_uri=' + encodeURIComponent("https://surl.jh8.tw/ThirdPartyNotify/lineNotify") + '&state=' + p_code;
      //console.log("lineUrl>",lineUrl,morder_surl,JSON.stringify(p_send));

      const p_body = "下一步將進入Line Notify即時通知設定<br><span style='color:red;font-size:16px;'><b>請選擇透過一對一聊天接收後，<br>畫面下拉點擊「同意並連動」!</b></span><br>獲得最新訂單資訊，<br>未來還有機會收到獨家活動折扣碼喔!";
      showConfirm(p_body,'下一步', 'noCancel')
        // 點擊確認執行贈卷
        .then(() =>  window.open(lineUrl, "_self"))

    }});
  },
  async memberThirdChildLogin({dispatch,state, commit}, formData) {
		//第三方登入(卡包子企業)
		const {isFrom, EnterpriseID, FromTDID} = formData
    if (state.lineLogin.path === "cardShop" && state.member.FromTDID && !state.searchURL.lineNofInfo){
      //指向商城,且,karkay主企業沒有linenotify的RegId,且,非linenotify callback回來的
      if (!state.lineLogin.isLoadReg) await dispatch('memberThirdLogin', { sub: state.member.FromTDID,isFromTD: "LINElogin", isOnlyLoad:true})//重取linenotify的RegID,需要重撈
      if (!state.lineLogin.isLoadNotify) await dispatch('memberLineNotifySys', { EnterpriseID: state.lineLogin.EnterpriseID})//取參數

    }
		const url = state.api.consumerPubUrl//memberbaseUrl
		const body = {
			isFrom, EnterpriseID,FromTDID,
			"act": "membersthirdlogin",
			"isFromTD": "LINElogin",
		}
		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		if (!data.error && Array.isArray(data) && data.length){
			const one	= data[0]
      commit('setCurrentAppUserToken', one.Tokenkey)	// 存入 Tokenkey
      //註冊推播
			deviseFunction('Do_Register_Other', `{"MembersName":"${one.Name}", "Account":"${one.Account}", "EnterpriseID":"${one.EnterPriseID}"}`, '');
// IsHank && alert('前往何處路由: \n'+JSON.stringify(one));	// @@

      dispatch('pathCheck',  one) //前往何處路由
      const fn1 = formData.fn;
			IsFn(fn1) && fn1(data);

      return new Promise(resolve => {resolve(true)})
		}else if(data.error && data.detail.ErrorCode === "5" && data.detail.Remark === ""){
			//未綁第三方(line login),或會員時,回應ErrorCode=5
      //showAlert("請註冊會員")
      await dispatch('memberThirdChildRegister', formData).then((res) => {
        if (!res.error && res.ErrorCode == '0'){
          dispatch('memberThirdChildLogin',  formData)
        }
      })

      return new Promise(resolve => {resolve(false)})
		}
		//綁第三方,未綁會員時,應該不會發生
		//else if(data.error && data.detail.ErrorCode === "6" && data.detail.Remark === ""){}

	},
	async memberThirdLogin({state, commit, getters}, formData) {
		//第三方登入
    formData.isFromTD = formData.isFromTD || 'LINElogin'
		const {isFrom, EnterpriseID} = state.baseInfo
		const url = state.api.consumerUrl//memberbaseUrl, consumerPubUrl
		const body = {
			isFrom, EnterpriseID,
			"act": "membersthirdlogin",
			"isFromTD": formData.isFromTD,//"LINElogin",
			"FromTDID": (formData.sub || formData.FromTDID)
		}

		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		if (!data.error && Array.isArray(data) && data.length){
			let one	= data[0]
			S_Obj.save('Tokenkey', one.Tokenkey);	// 登入後主Token
			const pwd='';
		// => 存入會員資訊 (cookie, store)
      if (formData.isFromTD == 'LINElogin') one[state.thirdKey.line] = formData.sub
      if (formData.isFromTD == 'FBlogin') one[state.thirdKey.fb] = formData.sub
      one.FromTDID = formData.sub;
      commit('setLineLoginKey', {key:"isLoadReg",value:true});
			commit('setLoginInfo', {data: one, rememberMe: true, pwd})
			// => 存入會員資訊 (殼)
			deviseSetLoginInfo({type: 'set', data: one, rememberMe: true, pwd})
			// 取完後再導向首頁
			if (!formData.isOnlyLoad) getters.router.push('/')
			// getters.router.push('/list')
		}else if(data.error && data.detail.ErrorCode === "5" && data.detail.Remark === ""){
			//未綁第三方(line login),或會員時,回應ErrorCode=5
      //showAlert("請註冊會員")
			getters.router.push({ path: '/register', query: { loginType: 'line' } })

		}
		//綁第三方,未綁會員時,應該不會發生
		//else if(data.error && data.detail.ErrorCode === "6" && data.detail.Remark === ""){}

	},
  async memberThirdRegisterID({state, commit},p_RegId) {
		//第三方註冊,LINE Notify
		setLoading(true)
		const {isFrom, EnterpriseID} = state.baseInfo
		const {FromTDID,code} = state.member
    if (!FromTDID) return;
		const url = state.api.memberbaseUrl; //consumerUrl
		const body = {
			isFrom, EnterpriseID,
			"act": "membersthirdregid",
			"isFromTD": "LINElogin",
			"FromTDID":FromTDID,
			Account:code,
			RegId: p_RegId, //state.lineLogin.RegId,
		}
		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		return new Promise(resolve => resolve(data))
	},
	async memberThirdRegister({state, commit}, account) {
		//第三方註冊
		setLoading(true)
		const {isFrom, EnterpriseID} = state.baseInfo
		const {isFromTD, FromTDID, MT_NickName, FromTDEmail} = state.member
		const url = state.api.consumerUrl//memberbaseUrl
		const body = {
			isFrom, EnterpriseID,
			"act": "membersthirdreg",
			"isFromTD": isFromTD,
			"FromTDID":FromTDID,
			Account:account,
			FromTDNickName:MT_NickName,
			FromTDEmail:FromTDEmail,
		}
		const data 	= await fetchData({url, body})
		commit('setLoading', false)
		return new Promise(resolve => resolve(data))
	},
  // (卡卡) 會員登入,主企業kakar登入
  async memberLogin({state, commit, getters}, formData) {
    setLoading(true)
    const {isFrom, EnterpriseID} = state.baseInfo
		const url = state.api.consumerPubUrl//consumerUrl  (xxxxxxOneEntry才能撈RtUrl)
    const pwd = formData.noNeedMd5 ? formData.password : md5(formData.password)
    const body = {
      isFrom, EnterpriseID,
      "act": "login",
      "memberphone": formData.account,
      "memberPwd": pwd,
      "FunctionID":state.baseInfo.FunctionID_login
    }
    const data 	= await fetchData({url, body})
			,one			= data[0]

    commit('setLoading', false)
    if (data.error) return showAlert(data.msg)

		IsDev && console.warn('主企業kakar登入-data: ', one);	// @@

		S_Obj.save('Tokenkey', one.Tokenkey);	// 登入後主Token
		L_Obj.save('userData', one);
		IsMobile() && L_Obj.save('loginForm', formData);

	// => 存入會員資訊 (cookie, store)
    commit('setLoginInfo', {data: one, rememberMe: formData.rememberMe, pwd})
    // => 存入會員資訊 (殼)
    deviseSetLoginInfo({type: 'set', data: one, rememberMe: true, pwd})
    // 取完後再導向首頁
    getters.router.push('/')
    // getters.router.push('/list')
  },
  // (卡卡) 會員登入卡包企業
  async memberLogin_sub({dispatch, state, commit},payload) {
    if ((state.member.code || '') === '') {showAlert('會員編號遺失!'); return dispatch('memberLogout');}
    commit('setLoading', true)
    let url = `https://${baseUrl}/Public/AppDataVipOneEntry.ashx`
    // let url = `https://8012.jinher.com.tw/Public/AppDataVipOneEntry.ashx`
    let body = {
      "act":"login",
      "memberphone":state.member.code,
      "memberPwd":state.member.pwd,
      "isFrom": state.currentAppUser.isFrom,
      "EnterpriseID": state.currentAppUser.EnterPriseID,
      "FunctionID":state.baseInfo.FunctionID_login
    }

    const data	= await fetchData({url, body});
		const one	 	= (!data.error?data[0]:{})
			,_token 	= one.Tokenkey
		console.warn('會員登入卡包企業-one: ', one);	// @@

    commit('setLoading', false)

		if (!data.error) {
			if (data.length) {
				commit('setCurrentAppUserToken', _token)	// 存入 Tokenkey
				deviseFunction('Do_Register_Other', `{"MembersName":"${one.Name}", "Account":"${one.Account}", "EnterpriseID":"${one.EnterPriseID}"}`, '');
			}
			const fn1 = payload.fn;
			IsFn(fn1) && fn1(data);

		} else {
			showAlert(data.msg)
    }
  },
  // (卡卡) 會員登出
  memberLogout({commit, getters}, bManual) {
    // 清除 殼 裡的會員資料
    deviseSetLoginInfo({type: 'reset'})
    // 清除 state 裡的會員資料
    commit('setLogout')
    commit('setSearchUrl', {key:"code",value:""});
    commit('setSearchUrl', {key:"state",value:""});
    commit('setSearchUrl', {key:"linePara",value:""});
    commit('setSearchUrl', {key:"lineNofInfo",value:""});

    commit('setLineLoginKey', {key:"appQrcode",value:""});
    commit('setLineLoginKey', {key:"isScan",value:true});
    commit('setLineLoginKey', {key:"RegId",value:""});
    window.history.replaceState(null, null, window.location.pathname);//有帶參數會拿掉

		/** @PS: 殼會固定先清空,改手按才調用 */
		if (bManual) {
			L_Obj.del('userData');
			L_Obj.del('currentAppUser');
		}

    // 導向登入頁
    getters.router.push('/login')
  },
  // 會員修改個人資訊
  async ensureMemberUpdateProfile({state, getters, commit}, data){
    commit('setLoading', true)
    //const url = state.api.memberUrl
    let url = `https://${baseUrl}/Public/AppDataVipOneEntry.ashx`
    const body = Object.assign({}, data, { act: "memberupdate_wuhu", memberphone: state.member.code,"FunctionID":state.baseInfo.FunctionID }, getters.kakarBody() )
    const update = await fetchData({url, body})
    commit('setLoading', false)
    if (update.error) {
      showAlert((update.msg || "修改密碼失敗"))
    }
    return new Promise(resolve => resolve(update) )
  },
  // 使用積分換卷(後續邏輯由 call 此 action 的人處理)
  async ensurePointExchangeTicket({state, getters}, ticket) {
    // const url = state.api.memberUrl
		const host1	= S_Obj.read('CardHost')
      ,url 			= `https://${host1}/public/AppVip.ashx`
			//,url 			= `https://${host1}/public/AppVipOneEntry.ashx`
			// ,body = getters.appBodyWuhu({
			,body = getters.appBody({
      act: "PointExchangeTicket",
      OpenID: state.member.code,
      TicketCount: "1",
      TicketTypeCode: ticket.TicketTypeCode,
      Points: ticket.Points
    })
    const data = await fetchData({url, body})
		return !data.error ? data : data.detail;
  },
  // 優惠卷領用
/*
  async ensureFetchDrawTicket({state, getters}, DrawGID) {
    setLoading(true, 1);

    const url = state.api.memberUrl
			,now1 	= formatTime(new Date(), 'YYYYMMDDHHmm')
		// console.log('優惠卷領用-now1: ', now1);	// @@
    const body = getters.appBodyWuhu({
      act: "GetDrawTicket",
      DrawGID: DrawGID,
      actTime: now1
    })
    const data = await fetchData({url, body})
    setLoading(false);
		return !data.error ? data : data.detail;
  },
 */
  /**
    統一用這個先將品牌資料撈好
  */
  async fetchVipAppsByCheck({state, dispatch, commit}, enterpriseID) {
    // await dispatch('fetchVipAppsIfNoData', enterpriseID)
    const brandList = state.vipUserData
		// 針對沒值而重撈
		if (!brandList.length) {
			await dispatch('fetchVipAppsOnlyList', {"enterpriseID":enterpriseID})
		}

    const findApp = brandList.find(app => app.EnterPriseID === enterpriseID)
    const findApp2 = state.vipApps.find(app => app.EnterpriseID === enterpriseID) //test temp
    commit('setCurrentAppUser', findApp || findApp2)
  },
  /**
    refresh時只呼叫卡包清單和單一品牌
  */
  // async fetchVipAppsIfNoData({dispatch}, enterpriseID) {
    // let brandList = state.vipUserData
    // // 有資料表示為正常點按,不用重撈
    // if (brandList && brandList.length>0) {
      // // console.log('===> brandList > 0')
      // new Promise(resolve => {resolve(true)})
    // // refresh
    // } else {
      // // console.log('===> brandList < 0')
      // await dispatch('fetchVipAppsOnlyList', {"enterpriseID":enterpriseID})
    // }
  // },
  /**
    解綁一張卡片
  */
  async fetchReleaseOneCard({getters, state, commit}) {
    if (!getters.isLogin) return

    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    let body = getters.appBody({
      FunctionID: state.baseInfo.FunctionID,
      act: 'releaseKKVipAPP'
    })

    const data = await fetchData({url, body})
    //if (data.error) return

    // 把store.state.vipUserData清掉重來
    commit('clearVipUserData')

    return new Promise(resolve => resolve(data))
  },
  /**
    只呼叫卡包清單和單一品牌
  */
  async fetchVipAppsOnlyList({dispatch, getters, commit}, P_data) {
    if (!getters.isLogin)		return
    commit('setThirdID') //存回thrid UID
    const {isOnly,enterpriseID} = P_data;
    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody(), { act: 'GetKKVipAPP' } )
    const apps = await fetchData({url, body})
    if (apps.error) return
    await commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    await commit('setVipUserLoaded')
    if (!isNotLinePara()) return; //line應用不用做
    // 將"單一" app 的會員資料存入
    let app = await apps.find(curApp => {
      return curApp.EnterpriseID === enterpriseID
    })
    if (app) await dispatch('fetchVipAppData', {app: app})
    if (!isOnly) dispatch('fetchVipApps') //順便全取卡包 => 需要有這行,不然卡包會被清空剩一個
  },
  fetchVipSetData({state,commit},app){
    const user_url = app.apiip || `https://${baseUrl}/Public/AppVipOneEntry.ashx`;

      // 預設先用存在殼或cookie的圖片路徑
      let filePath = state.cardPhoto[app.EnterpriseID]
      if (!filePath || (typeof filePath==='undefined')
        || filePath.trim()=='') {
        filePath = ''
      }

      const userData = {
        Sex:           '',  //男/女
        mac:           app.mac,
        Name:          '',
        Email:         '',
        isFrom:        app.isFrom,
        Address:       '',
        Mobile:        '',
        Account:       '',
        Birthday:      '',
        ExpiredDate:   '',
        EnterPriseID:  (app.EnterPriseID || app.EnterpriseID),
        CardID:        '',
        CardNO:        '',
        cardGID:       '',
        cardPoint:     0,
        cardTicket:    '',
        CardTypeCode:  '',
        CardTypeName:  '',
        // CardFacePhoto: '',
        CardFacePhoto: filePath,
        apiip:         user_url,
        SaleShopID:    '',
        EnterpriseID:  (app.EnterpriseID || app.EnterPriseID),
        // token:         p_token,
      }
      //console.log("not cardgid",JSON.stringify(userData));
      commit('setVipUserData', userData)
  },
  // (卡卡) 取已綁定的企業 app 列表
  async fetchVipApps({dispatch, getters, commit}) {
    if (!getters.isLogin)		return
    commit('setThirdID') //存回thrid UID
    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = Object.assign({}, getters.kakarBody(), { act: 'GetKKVipAPP' } )
    const apps = await fetchData({url, body})
    commit('setLoading', true)
    commit('setLoadCard', true)
    if (apps.error) {commit('setLoading', false);commit('setLoadCard', false);return}

    // 存入已綁定的企業 app
    commit('setVipApps', apps)
    // 完成使用者的基本資料載入
    commit('setVipUserLoaded')
    // 將各 app 的會員資料存入
    //apps.forEach(app => dispatch('fetchVipAppData', {app: app}) )
    //var promise_arr = [];

    commit('setLoading', false);commit('setLoadCard', false)
    //if (Array.isArray(apps) && apps.length == 0) {commit('setLoading', false);commit('setLoadCard', false)}
    //透過快速連結進入的(帶有appQrcode),不撈全卡包各別資訊,減少Loading
    if (state.lineLogin.appQrcode === "") apps.forEach(app => {
      // console.log('取已綁定的企業apps-AppName,EnterpriseID: '+app.AppName, app.EnterpriseID);	// @@
      dispatch('fetchVipSetData', app)
    })
    getters.currentAppUsers.forEach(app => {
      dispatch('fetchVipAppData', {app: app})
    })

    // getters.currentAppUsers.forEach(app => promise_arr.push(new Promise((resolve)=>{

    //   dispatch('fetchVipAppData', {app: app, fn:p_data=>{
    //     resolve(p_data);
    // }})})) )

    // Promise.all(promise_arr).then(()=> {
    //   setTimeout(() => {
    //     //if (document.querySelector(".pull-down-container")) document.querySelector(".pull-down-container").scrollTop = state.cardListScrollMap;
    //     commit('setLoading', false)
    //     commit('setLoadCard', false)
    //   }, 1)


    // }).catch( () => {
    //   // 失敗訊息 (立即)
    //   commit('setLoading', false)
    //   commit('setLoadCard', false)
    // });
  },
  // (卡卡) 取綁定企業的基本資料
  async fetchVipAppData({state, getters, commit}, payload) {
		const fn1 = payload.fn;
    if (!getters.isLogin) {
			IsFn(fn1) && fn1();
			return;
    }

    const { app } = payload
    const { EnterpriseID, isFrom, mac,EnterPriseID } = app
    // 取會員的點數與卡資料
    // const url = state.api.memberUrl
    // const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const url = app.apiip || `https://${baseUrl}/Public/AppVipOneEntry.ashx`;
    const body = {
      act: "GetVipAndCardInfo",
      Account: state.member.code,
      mac, isFrom, EnterpriseID,EnterPriseID
    }

    const vipCardInfo = await fetchData({url, body})
    if (vipCardInfo.error) {
			// commit('setLoading', false);	// NG!
			showAlert(`卡包: ${isFrom}<br>`+vipCardInfo.msg);
      IsFn(fn1) && fn1(vipCardInfo.error);
      return;
    }

    // 將取得的資料存入入 userData 裡面
    const ticketData = vipCardInfo.querydata_return_info.ticket_info
    const cardData   = vipCardInfo.querydata_return_info.card_info
    const vipData    = vipCardInfo.querydata_return_info.vip_info
    // if (state.member.pwd === "") {
    //   //第三方登入,Line login會沒有pwd
    //   const pwd = md5(vipData.PassWord);
    //   commit('setLoginInfo', {data: state.userData, rememberMe: true, pwd})
    //   // => 存入會員資訊 (殼)
    //   deviseSetLoginInfo({type: 'set', data: state.userData, rememberMe: true, pwd})

    // }

    //if (state.lineLogin.appQrcode !== "") cbFnScanQrCode({QrCode:state.lineLogin.appQrcode}) //暫時不放這
		IsFn(fn1) && fn1(vipCardInfo);

    // 取企業會員的卡片圖檔
    let apiip = app.apiip || app.APPUrl;
    if (typeof apiip === "string") apiip = apiip.trim().toLowerCase();
    let cardImg
    let isS3Image = /staging/.test(cardData.CardFacePhoto)
    if (app.Dir && app.FileName) cardImg = 'https://' + baseUrl + app.Dir + app.FileName
    if (cardData.CardFacePhoto !== '' && apiip) cardImg = apiip.replace('/public/appvip.ashx', '') + cardData.CardFacePhoto
    if (isS3Image) cardImg = cardData.CardFacePhoto
    // const p_token = VueCookies.get('kkTokenkey_'+app.EnterpriseID);
    // {url, body}
    // const reqHead = head || baseHead
    // if (!p_token) getSubsidiaryToken
    // 設定要存入的會員資料
    // console.log('===> cardImg =>' + cardImg)

    // 企業 app 卡包圖片
    commit('setCardPhoto1', {
      eID: EnterpriseID,
      filePath: cardImg
    })
    state.vipAppsLoadedCount++
    if (state.vipAppsLoadedCount == state.vipApps.length) {
      //console.log('===> all is back')
      state.vipAppsLoadedCount = 0 // 清空
      commit('setCardPhotoAll')
    } else {
      //console.log('===> not all is back')
    }

    const userData = {
      Sex:           vipData.sex,
      mac:           app.mac,
      Name:          vipData.CnName,
      Email:         vipData.Email,
      isFrom:        app.isFrom,
      Address:       vipData.Addr,
      Mobile:        vipData.mobile,
      Account:       vipData.CardNO,
      Birthday:      vipData.BirthDay,
      ExpiredDate:   cardData.ExpiredDate,
      EnterPriseID:  (app.EnterPriseID || app.EnterpriseID),
      EnterpriseID:  (app.EnterpriseID || app.EnterPriseID),
      CardID:        vipData.CardId,
      CardNO:        vipData.CardNO,
      cardGID:       cardData.GID,
      cardPoint:     parseInt(cardData.Points),
      cardTicket:    ticketData,
      CardTypeCode:  cardData.CardTypeCode,
      CardTypeName:  cardData.CardTypeName,
      CardFacePhoto: cardImg,
      apiip:         url,
      // SaleShopID:    cardData.SaleShopID,
      SaleShopID:    (vipCardInfo.APPVipSaleShopID || cardData.SaleShopID)
      // token:         p_token,
    }
    //console.log((app.EnterpriseID || app.EnterPriseID),"vipCardInfo.APPVipSaleShopID",JSON.stringify(userData));
    commit('setVipUserData', userData)
  },
  // 取會員資料 => 主要是點數給card/Member.vue和card/Point.vue
  async fetchVipAndCardInfo({getters, commit}) {
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.currentAppUser.EnterPriseID === undefined) return

    // const url = getters.appMemberApiUrl
    const url = getters.subTokenApiUrl
    let body = getters.appBody({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberVipData', data)
  },
  // 取付款狀態
  async fetchPayStatus({getters, commit, state}) {
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin) return
    //const url = state.currentAppUser.apiip || state.api.memberUrl;
    //let body = getters.appBodyWuhu({act: 'GetData',"DataName":"P_PayStatus"})
    const chkFetchData = (p_body)=>{
      const p_isSubsidiary = (state.currentAppUser && state.currentAppUser.isFrom !== state.baseInfo.isFrom);
      const url =  (!p_isSubsidiary?state.api.consumerUrl:getters.subConsumerUrl);
      const body = (!p_isSubsidiary?getters.appBodyWuhu(p_body):getters.appBody(p_body));

      return {url,body,isSubsidiary:p_isSubsidiary}
    }

    const data = await fetchData(chkFetchData({act: 'GetData',"DataName":"P_PayStatus"}));
    //const data = await fetchData({url, body})
    if (data.error) return
    commit('setPayStatus', data)
  },
  // 取會員資料 => 主要是點數給HomeMember.vue和HomePoint.vue
  async fetchVipAndCardInfoWuhu({getters, commit}) {
    // console.log('fetchVipAndCardInfoWuhu ===> start')
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || !state.baseInfo.EnterpriseID) return

    // const url = state.api.picUrl + '/public/AppVIP.ashx' //getters.appMemberApiUrl
		const host1	= S_Obj.read('CardHost');
    if (!host1) return;   //have undefined ,not do it.
		const url 			= `https://${host1}/public/AppVIP.ashx`  //被改為host1,主企業不適用
			// ,body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
			,body = getters.appBody({act: 'GetVipAndCardInfo'})
			,data = await fetchData({url, body})
		// console.log('取會員資料-GetVipAndCardInfo: ', data);	// @@

    if (data.error) return

    commit('setMemberVipDataWuhu', data)
  },
  // (卡卡) 取 currentAppUser 的相關資料
  async fetchAppUserData({dispatch, getters, commit, state}){
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.currentAppUser.EnterPriseID === undefined) return

    const url = getters.appMemberApiUrl
    const body = getters.appBody({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      commit('setMemberVipData', data)
      // 取會員的消費紀錄
      dispatch('fetchMemberPurchaseRecordData')
      // 取會員的積點交易紀錄
      dispatch('fetchMemberExpiredPointsData')
      // 取優惠卷列表資訊
      dispatch('fetchMemberTicketListData')
      // 取會員已轉贈的優惠卷資訊
      dispatch('fetchMemberGaveTicketListData')
      // 取尚未能使用的優惠卷資料
      dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  // (卡卡) 取 currentAppUser 的相關資料 - 五戶
  async fetchAppUserDataWuhu({getters, commit, state},p_info){
    // 必須為登入狀態 && 有 currentAppUser
    if (!getters.isLogin || state.baseInfo.EnterpriseID === undefined) return

    const url = state.api.picUrl + '/public/AppVIP.ashx' //getters.appMemberApiUrl
    const body = getters.appBodyWuhu({act: 'GetVipAndCardInfo'})
    const data = await fetchData({url, body})
    if (data.error) return

    return new Promise(resolve => {
      const isKakar = p_info && p_info.isKakar;
      if (!isKakar) commit('setMemberVipData', data)
      if (isKakar) commit('setMemberVipDataWuhu', data)


      // // 取會員的消費紀錄
      // dispatch('fetchMemberPurchaseRecordData')
      // // 取會員的積點交易紀錄
      // dispatch('fetchMemberExpiredPointsData')
      // // 取優惠卷列表資訊
      // dispatch('fetchMemberTicketListData')
      // // 取會員已轉贈的優惠卷資訊
      // dispatch('fetchMemberGaveTicketListData')
      // // 取尚未能使用的優惠卷資料
      // dispatch('fetchMemberNotAvailableTickets')

      resolve(true)
    })
  },
  //
  async fetchCardQRCodeData_kakar2({commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )
    const url = state.api.memberUrl
    const body = getters.kakarBody({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})

    if (data.error) return
    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )
  },
  // (卡卡) 取 cardQRcode
  async fetchCardQRCodeData({commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )
    const url = getters.appMemberApiUrl
    const body = getters.appBody({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})

    if (data.error) return
    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )
  },
  // (龍海) 取 cardQRcode
  async fetchWuhuCardQRCodeData({commit, getters}, GID) {
    // 先清空舊的 QRcode
    commit('setCardQRCodeData', { value: '', image: '', loaded: false } )

    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({act: 'GetVipCardQrCodeStr', GID})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setCardQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true } )
  },
  // (卡卡) 進入企業 app(點選卡包
  async AccessInApp({dispatch,commit}, userData) {
    // 存入企業號 (用於線上點餐，回到 卡+ 時正確導向到指定頁面)
    deviseFunction('SetSP', `{"spName":"currentEnterPriseID", "spValue":"${userData.EnterPriseID}"}`, '')
    VueCookies.set('currentEnterPriseID', userData.EnterPriseID)
    // 企業 app 點擊紀錄 +1
    commit('setAppClicks', userData.EnterPriseID)


    // 先存入 currentAppUser
    await commit('setCurrentAppUser', userData)
    //await dispatch('cardSySetting') //取參數

		// console.warn('AccessInApp進入企業-userData: ', userData);	// @@
		const url1 	= userData.apiip.trim()			// " https://8001.jh8.tw/public/AppVIP.ashx"
			,npos  		= url1.indexOf("//")+2
			,str1			= (npos != -1) ? url1.substring(npos) : ''
			,host1 		= str1.substring(0, str1.indexOf("/"))		// "8001.jh8.tw"
		S_Obj.save('CardHost', host1);
    const {FromTDID} = state.member;
    const {EnterpriseID, isFrom, fn} = userData

    if (FromTDID && FromTDID != undefined) await dispatch('memberThirdChildLogin',  {EnterpriseID, isFrom, FromTDID, fn})
    else await dispatch('memberLogin_sub',userData)
  },
  /* 企業公開資料相關 */

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataWuhu({dispatch}){
    // console.log('fetchAppPublicDataWuhu ===> start')

    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')

    // 取首頁主 banners
    dispatch('fetchIndexSlideBannersWuhu')

    // 取首頁副 banners
    dispatch('fetchIndexSubBannerWuhu')

    // // 取最新消息的資料
    // dispatch('fetchNewsDataWuhu')

    // 取最新消息的分類
    // dispatch('fetchNewsTypes')
    // 取品牌資訊的資料
    // dispatch('fetchBrandInfo')
    // 取門店資訊的資料
    // dispatch('fetchStoreListDataWuhu')
    // 取 menu 資料
    // dispatch('fetchMenuData')

  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataStoreWuhu({dispatch}){
    // console.log('fetchAppPublicDataStoreWuhu ===> start')

    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')

    // 取門店資訊的資料
    dispatch('fetchStoreListDataWuhu')

  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataNewsWuhu({dispatch}){
    // console.log('fetchAppPublicDataNewsWuhu ===> start')

    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')

    // 取最新消息的資料
    dispatch('fetchNewsDataWuhu')

  },

  // (公開) 取 currentApp 的公開資料 - HomeMember頁
  async fetchAppPublicDataMemberWuhu({dispatch}){
    // console.log('fetchAppPublicDataMemberWuhu ===> start')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')
    // 取點數等資料
    dispatch('fetchVipAndCardInfoWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - HomePoint頁
  async fetchAppPublicDataPointWuhu({dispatch}){
    // console.log('fetchAppPublicDataPointWuhu ===> start')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')
    // 取點數等資料
    dispatch('fetchVipAndCardInfoWuhu')
    // 取會員的積點交易紀錄
    dispatch('fetchMemberExpiredPointsDataWuhu')
  },

  // (公開) 取 currentApp 的公開資料 - HomeCoupon頁
  async fetchAppPublicDataCouponWuhu({dispatch}){
    // console.log('fetchAppPublicDataCouponWuhu ===> start')
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoWuhu')
    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListDataWuhu')
    // 取會員已轉贈的優惠卷資訊
    dispatch('fetchMemberGaveTicketListDataWuhu')
    // 取尚未能使用的優惠卷資料
    dispatch('fetchMemberNotAvailableTicketsWuhu')
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataDetailWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
    // 取首頁主 banners
    dispatch('fetchIndexSlideBanners')
    // 取首頁副 banners
    // dispatch('fetchIndexSubBannerWuhu')
    dispatch('fetchIndexSubBanner')			// - 卡加use!
    // 取線上點餐的參數(.Mode)存入到門店資料裡面
    dispatch('fetchAppMode')
    // 取最新消息的資料
    //  dispatch('fetchNewsData')
    // 取最新消息的分類
    //  dispatch('fetchNewsTypes')
    // 取品牌資訊的資料
    // dispatch('fetchBrandInfo')
    // 取門店資訊的資料
    // dispatch('fetchStoreListData')
    // 取 menu 資料
    dispatch('fetchMenuData_kakar2')

    // 自動領券(要在fetchMemberTicketListDataWuhu之前且要await)
    await dispatch('fetchGetDrawAutoTicketWuhu')

    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListData')
  },

  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataDetailStoreKakar2({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
    // 取門店資訊的資料
    dispatch('fetchStoreListDataDetail_kakar2')
  },
  // (公開) 取 currentApp 的公開資料
  async fetchAppPublicDataDetailNewsWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
	// 取最新消息的資料
    // dispatch('fetchNewsData')
    dispatch('fetchNewsData_kakar2')
  },

  // (公開) 取 currentApp 的公開資料 - card/Member頁
  async fetchAppPublicDataDetailMemberWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')

    // 取點數等資料
    dispatch('fetchVipAndCardInfo')

    // 自動領券(要在fetchMemberTicketListDataWuhu之前且要await)
    await dispatch('fetchGetDrawAutoTicketWuhu')

    // 取優惠卷列表資訊
    dispatch('fetchMemberTicketListData')
  },

  // (公開) 取 currentApp 的公開資料 - card/Point
  async fetchAppPublicDataDetailPointWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')
    // 取點數等資料
    dispatch('fetchVipAndCardInfo')
    // 取會員的積點交易紀錄
    dispatch('fetchMemberExpiredPointsData')
  },

  // (公開) 取 currentApp 的公開資料 - card/Coupon
  async fetchAppPublicDataDetailCouponWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')

    // 自動領券(要在fetchMemberTicketListDataWuhu之前且要await)
    await dispatch('fetchGetDrawAutoTicketWuhu')

		/** @Fix: 優化-按了分頁後才更新對應之列表(goActTab */
    // 取優惠卷列表資訊
    // dispatch('fetchMemberTicketListData')
    // // 取會員已轉贈的優惠卷資訊
    // dispatch('fetchMemberGaveTicketListData')
    // // 取尚未能使用的優惠卷資料
    // dispatch('fetchMemberNotAvailableTickets')
  },

  // (公開) 取 currentApp 的公開資料
  // async fetchAppPublicData({dispatch}){
    // // 取企業 app 的 connect
    // await dispatch('fetchAppConnect')
    // // 取企業 app 的 LOGO
    // dispatch('fetchAppLogo')
    // // 取首頁主 banners
    // dispatch('fetchIndexSlideBanners')
    // // 取首頁副 banners
    // dispatch('fetchIndexSubBanner')
    // // 取最新消息的資料
    // dispatch('fetchNewsData')
    // // 取最新消息的分類
    // dispatch('fetchNewsTypes')
    // // 取品牌資訊的資料
    // dispatch('fetchBrandInfo')
    // // 取門店資訊的資料
    // dispatch('fetchStoreListData')
    // // 取 menu 資料
    // dispatch('fetchMenuData')
  // },
  // (公開) 取企業 App 的 db connect 資料
  async fetchAppConnect({state, commit}){
    const url = state.api.memberUrl
    const body = {"act":"db_name","EnterPriseID": state.currentAppUser.EnterPriseID}
    const connect = await fetchData({url, body})
    if (connect.error) return

    commit('setAppConnect', connect)
  },
  // (公開) 取企業 App 的 LOGO 資料
  async fetchAppLogo({state, getters, commit}){
    const url = state.api.memberUrl
    const body = {act: "getapplogo", EnterPriseID: getters.currentApp.EnterpriseID}
    const data = await fetchData({url, body})
    if (data.error) return
    if (data.length == 0) {return commit('setAppLogo', '') }
    const isFullUrl = /http:|https:/.test(data[0].Dir); //是否有在s3設http直連
    if (isFullUrl) commit('setAppLogo', data[0].Dir);
    if (!isFullUrl) commit('setAppLogo', getters.srcUrl + data[0].Dir + data[0].FileName);
    // const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName
    // commit('setAppLogo', logoUrl)
  },

  // 卡包企業,(公開) 取企業 App 的 LOGO 資料
  async fetchAppLogoDetail({state, getters, commit}){
    // const url = state.api.publicUrl
    const url = state.currentAppUser.apiip || "https://8012.jh8.tw/Public/AppVipOneEntry.ashx"
    const body = {act: "getapplogo", EnterPriseID: state.currentAppUser.EnterPriseID}
    const data = await fetchData({url, body})
    if (data.error) return
    // if (data.length == 0) {data.push({Dir:'',FileName:''})}
    if (data.length == 0) {return commit('setAppLogo', '') }
    const isFullUrl = /http:|https:/.test(data[0].Dir); //是否有在s3設http直連
    if (isFullUrl) commit('setAppLogo', data[0].Dir);
    if (!isFullUrl) commit('setAppLogo', getters.srcUrl + data[0].Dir + data[0].FileName);
    // const logoUrl = getters.srcUrl + data[0].Dir + data[0].FileName
    // commit('setAppLogo', logoUrl)
  },

  // [五戶](公開) 取企業 App 的 LOGO 資料
  // async fetchAppLogoWuhu({state, getters, commit}){
  async fetchAppLogoWuhu({state, commit}){
    const url = state.api.memberbaseUrl;//memberUrl
    // const url = getters.subBookingUrl
    const body = {act: "getapplogo", EnterPriseID: state.baseInfo.EnterpriseID}
    const data = await fetchData({url, body})

    if (data.error) return
    // if (data.length == 0) {data.push({Dir:'',FileName:''}) }
    if (data.length == 0) {return commit('setAppLogoWuhu', '') }
    const isFullUrl = /http:|https:/.test(data[0].Dir); //是否有在s3設http直連
    if (isFullUrl) commit('setAppLogoWuhu', data[0].Dir);
    if (!isFullUrl) commit('setAppLogoWuhu', state.api.picUrl + data[0].Dir + data[0].FileName);
    // const logoUrl = state.api.picUrl + data[0].Dir + data[0].FileName
    // commit('setAppLogoWuhu', logoUrl)
  },

  // [五戶]撈[我的預約]清單
  async fetchMyBookList({getters, commit}){
    const url = getters.subBookingUrl
    // const body = getters.appBodykaka({act: 'get_mybooking'})
			,body 	= getters.appBodykaka({act: 'get_myservicebooking'})
			,data 	= await fetchData({url, body})
		// IsDev && console.warn('我的預約_data: ', data);	// @@

    if (data.error) return

    commit('setMyBookList', data.mybooking)
    commit('setMyBookStatusList', data.status)
  },
  /* 取得清潔項目資料 */
  async fetchCleanItemList({getters, commit}){
    const url = getters.subBookingUrl
			,body 	= getters.appBodykaka({act: 'get_serviceitem'})
			,data 	= await fetchData({url, body})
		// IsDev && console.warn('取得清潔項目資料: ', data);	// @@
    commit('setCleanItemList', !data.error ? data.cleanItem : [])
  },
  /* 取得 芳療師服務列表 */
  async fetchVIPNo_Services({getters, commit}){
    const url = getters.subTokenApiUrl
			,body 	= getters.appBodykaka({act: 'getVIPNo_Services', srow: 0, erow: 20})
			,data 	= await fetchData({url, body})
		// IsDev && console.warn('芳療師服務列表: ', data);	// @@
    !data.error && commit('setServiceItemList', data)
  },
	/* 取得 芳療師姓名匿稱 */
  async getVIPServiceOP({getters}, payload){
    const url = getters.subTokenApiUrl
			,body 	= getters.appBodykaka({act: 'getvip_serviceop', ServiceOP: payload})
			,data 	= await fetchData({url, body})
		body.Tokenkey && S_Obj.save('ApiToken', body.Tokenkey);
    return data;
  },
	/* 綁定-芳療師編號 */
  async setVIPServiceOP({getters}, payload){
		const param1 = {
			act: 'setVIP_ServicesOP',
			GID: payload.GID,
			ServiceOP: payload.ServiceOP,		// 'null' 表清除
			ServiceOP2: payload.ServiceOP2,	// 'null' 表清除
			ServiceOP3: payload.ServiceOP3,	// 'null' 表清除
		}
		/** @Fix: 修正Line登入疑失token問題 */
		let body 	= getters.appBodykaka(param1)
		if (!body.Tokenkey) {
			const _token = S_Obj.read('ApiToken')
			body.Tokenkey = _token
		}

		const url = getters.subTokenApiUrl
			,data 	= await fetchData({url, body})
		// const res = await axios.post(url, body, getters.requestHead)
			// ,data		= res.data
    // const data = await fetchData2(getters, param1)

    if (!data.error) {
			showAlert(data.ErrorMsg);			// 成功 訊息
			IsFn(payload.fn) && payload.fn();

		} else {
			showAlert(data.msg);		// 失敗 訊息
			return data.detail;
		}

    return data;
  },

  // [五戶]撈[商城]大小類等
  async fetchMallData({state}){
    const reloadShopFn = state.mallData.reloadShopFn;
    IsFn(reloadShopFn) && reloadShopFn();
  },
  // [五戶]撈[商城]記錄
  async fetchShopMine({state}){
    const reloadShopMineFn = state.mallData.reloadShopMineFn;
    IsFn(reloadShopMineFn) && reloadShopMineFn();
  },
  // [五戶]撈[商城]小類下品項,或品項的規格
  async fetchMallKindData({state},query){
    if (!query) return;
    let isMallKindLoad = (query.goto!=undefined)//商城只允許第一個小類可刷新
    let isMallProductLoad = (query.type!=undefined && query.type === 'product');

    if (isMallProductLoad && IsFn(state.mallData.reloadProductFn)){
      state.mallData.reloadProductFn();

    }else if (isMallKindLoad && IsFn(state.mallData.reloadGoKindFn)) {
      state.mallData.reloadGoKindFn();
    }
  },
  // (公開) 取主畫面可滑動的 Banners 資料	- 卡加use!
/*
  async fetchIndexSlideBanners({commit, getters}, payload){
    const url = `${getters.srcUrl}/public/newsocket.ashx`
		let data = await GetJSON_Banner(payload, 'M')
    if (data.error) return
    commit('setIndexSlideBanners', data)
  },
 */
  async fetchIndexSlideBanners({getters, commit}){
		const host1 = getters.srcUrl
    if (host1 == '') return

		const url = `${host1}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrandBanner",BannerType:"M"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setIndexSlideBanners', data)
  },
  // (公開) 取主畫面可滑動的 Banners 資料
  async fetchIndexSlideBannersWuhu({state, getters, commit}){
		if (!getters.isLogin) return

    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrandBanner",BannerType:"M"})
    let data = await fetchData({url, body})

    if (data.error) return

    commit('setIndexSlideBannersWuhu', data)
  },
  // (公開) 取固定的副 Banner 資料	- 卡加use!
/*
  async fetchIndexSubBanner({commit, getters}, payload){
    const url = `${getters.srcUrl}/public/newsocket.ashx`
		let data = await GetJSON_Banner(payload, 'D')
    if (data.error) return
    commit('setIndexSubBanner', data)
  },
 */
  async fetchIndexSubBanner({getters, commit}){
		const host1 = getters.srcUrl
    if (host1 == '') return

		const url = `${host1}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrandBanner",BannerType:"D"})
    const data = await fetchData({url, body})
// console.warn('取固定的副Banner-data: ', data); // @@
    if (data.error) return
    commit('setIndexSubBanner', data)
  },
  // // (公開) 取固定的副 Banner 資料
  async fetchIndexSubBannerWuhu({state, commit, getters}){
		if (!getters.isLogin) return

    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrandBanner",BannerType:"D"})

    const data = await fetchData({url, body})
    state.baseInfo.publicData.index.subBannerIsLoaded = true
    if (data.error) return

    commit('setIndexSubBannerWuhu', data)
  },
  // (公開) 取最新消息的資料
  async fetchNewsData({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsData', data)
  },
  // (公開) 取最新消息的資料
  async fetchNewsData_kakar2({commit, getters}){
    // const url = state.api.publicUrl
    if (getters.subSrcUrl == '') return;
    const url = `${getters.subSrcUrl}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsData', data)
  },

  // (公開) 取最新消息的資料 - 五互
  async fetchNewsDataWuhu({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBodyWuhu({act:"GetBrendNews"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsDataWuhu', data)
  },
  // (公開) 取最新消息的分類
  async fetchNewsTypes({state, commit, getters}){
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrendNewsType"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setNewsTypes', data)
  },
  // (公開) 取品牌資訊的資料	// hank:應該是舊資料,待拿掉...
  // async fetchBrandInfo({state, commit, getters}){
    // const url = state.api.kakarUrl
    // const body = { act: "getbrandmsg", EnterpriseID: getters.currentApp.EnterpriseID, brandid: "1,2,3,4,5,6,7,8,9" }
    // const data = await fetchData({url, body})
    // if (data.error) return

    // commit('setBrandInfo', data)
  // },
  // (公開) 取門店資訊的資料	// @hank: 只用-fetchStoreListDataWuhu
  // async fetchStoreListData({state, getters, commit}){
    // // 取得主企業號 ID & connect
    // const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    // const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    // const dataA = await fetchData({url: memberUrl, body: bodyA})
    // const MainEnterPriseID = dataA.EnterpriseID
    // const MainConnect = dataA.connect

    // // 取得門店資料
    // const urlB = state.api.publicUrl
    // const bodyB = getters.appBody({act:"GetStoreList"})
    // const stores = await fetchData({url: urlB, body: bodyB})

    // // 取得門店的線上點餐參數
    // const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode"}

    // // 將線上點餐的參數(.Mode)存入到門店資料裡面
    // const storeMode = await await fetchData({url: memberUrl, body: bodyC})
    // stores.forEach(store => {
      // const mode = storeMode.find(m => m.ShopID === store.OrgCode)
      // store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
      // store.ShopID = (mode) ? mode.ShopID : undefined
    // })

    // // 存入到 Vuex 裡面
    // commit('setStoreListData', {stores, MainEnterPriseID, MainConnect})
  // },

  /* (公開) 取門店資訊的資料 - 五互 */
  async fetchStoreListDataWuhu({state, getters, commit}){
		const EPID	= state.currentAppUser.EnterPriseID
		,_lastEID 	= 'LastEnterpriseID'
		,lstEID			= S_Obj.read(_lastEID)
		,isDiff			= EPID != lstEID
		,_act1 			= 'GetVipMainEnterpriseID'
		,_act2 			= 'GetStoreList2'
		,_act3 			= 'GetAishileAppMode'
		,memberUrl 	= `https://${baseUrl}/Public/AppVipOneEntry.ashx`
		// ,memberUrl	= 'https://8012.jh8.tw/Public/AppVipOneEntry.ashx'

		let dataA		= S_Obj.read(_act1)
			,stores		= S_Obj.read(_act2)
			,storeMode	= S_Obj.read(_act3)
			,MainEnterPriseID, MainConnect
		// console.log('S_Obj取得資料: ', dataA, stores, storeMode);	// @@

			// 取得主企業號 ID & connect
		if (isDiff || !dataA) {
			const bodyA = {act:_act1, EnterpriseID: EPID}
			dataA = await fetchData({url: memberUrl, body: bodyA})
			S_Obj.save(_act1, dataA);
		}
		// console.log('取得門店資料-GetVipMainEnterpriseID: ', dataA);	// @@

		MainEnterPriseID = dataA.EnterpriseID
		MainConnect = dataA.connect

		// 取得門店資料
		if (isDiff || !stores) {
			const host1 = getters.srcUrl
			if (host1 == '') return

			const urlB = `${host1}/public/newsocket.ashx`
				,bodyB	= getters.appBodykaka({act:_act2})
			stores = await fetchData({url: urlB, body: bodyB})
			//console.log('取得門店資料-GetStoreList2: ', stores);	// @@
			S_Obj.save(_act2, stores);
		}

			//取得門店的線上點餐參數
		if (isDiff || !storeMode) {
			const bodyC = {act:_act3, EnterpriseID: EPID, FunctionID: "310703"}
			// 將線上點餐的參數(.Mode)存入到門店資料裡面
			storeMode = await fetchData({url: memberUrl, body: bodyC})
			S_Obj.save(_act3, storeMode);
		}
		// console.log('線上點餐的參數-storeMode: ', storeMode);	// @@

    state.baseInfo.publicData.storeIsLoaded = true
    // 存入到 Vuex 裡面
    commit('setStoreListDataWuhu', {stores, MainEnterPriseID, MainConnect, storeMode})
		S_Obj.save(_lastEID, EPID);

  },
  // (公開) 取門店資訊的資料
  async fetchStoreMainInfo({state, commit}){
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 存入到 Vuex 裡面
    commit('setStoreListData', {MainEnterPriseID, MainConnect})
  },
  // (公開) 取門店資訊的資料
  async fetchStoreListDataDetail_kakar2({state, getters, commit}){
    // 取得主企業號 ID & connect
    const memberUrl = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const bodyA = { act:'GetVipMainEnterpriseID',EnterpriseID: state.currentAppUser.EnterPriseID }
    const dataA = await fetchData({url: memberUrl, body: bodyA})
    const MainEnterPriseID = dataA.EnterpriseID
    const MainConnect = dataA.connect

    // 取得門店資料
    const urlB = `${getters.srcUrl}/public/newsocket.ashx` || state.api.publicUrl
    const bodyB = getters.appBody({act:"GetStoreList"})
    const stores = await fetchData({url: urlB, body: bodyB}) || []
//console.log('取得門店資料-stores: ', stores);	// @@
    //if ((Object.keys(state.publicData.modeType)).length ==0) dispatch('fetchAppMode')// 取線上點餐的參數(.Mode)存入到門店資料裡面

    // 取得門店的線上點餐參數??
    const bodyC = {"EnterPriseID": MainEnterPriseID, "act": "GetAppMode","FunctionID":state.baseInfo.FunctionID}

    //const urlC = state.currentAppUser.apiip || memberUrl
    // 將線上點餐的參數(.Mode)存入到門店資料裡面
    const storeMode = await fetchData({url: memberUrl, body: bodyC})
    //const storeMode = state.publicData.modeType;
    if (Array.isArray(stores)) stores.forEach(store => {
      if (Array.isArray(storeMode)){
        const mode = storeMode.find(m => m.ShopID === store.OrgCode)
        store.Mode = (mode) ? JSON.parse(mode.Mode) : undefined
        store.ShopID = (mode) ? mode.ShopID : undefined
      }
    })
    // 存入到 Vuex 裡面
    commit('setStoreListData', {stores, MainEnterPriseID, MainConnect})
  },

  async fetchAppMode({state, commit}){
    // const url = state.api.publicUrl
    const url = `https://${baseUrl}/Public/AppVipOneEntry.ashx`
    const body = {act: "GetAppMode", EnterPriseID: state.currentAppUser.EnterPriseID,"FunctionID":state.baseInfo.FunctionID}
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setStoreModeType', {p_modeType:data})
  },
  //
  async fetchAppThirdPartyPay({state, commit}, payload){
    // const url = state.api.publicUrl
    const url = `https://${baseUrl}/Public/ThirdPartyPay.ashx`
    //不給ShopID時,API會去抓預設門店//payload.OrderID
    //currentTimeLong()
       //{"act":"Scan2Pay","CheckKind":"Scan2Pay","servicetype":"OLPay","amount":"1","orderid":"20190703A003","ShopID":"KH01","PosType":"APPPOS","EnterpriseID":"54827062"}
    // const body =   {"act":"Scan2Pay","CheckKind":"Scan2Pay","servicetype":"OLPay","amount":"1","orderid":"20190703A003","PosType":"APPPOS","EnterpriseID":"newpos001"}
    const body = {act: "Scan2Pay","CheckKind":"Scan2Pay","servicetype":"OLPay","amount":"1","orderid":payload.OrderID,"PosType":"APPPOS","ShopID":state.currentAppUser.SaleShopID,EnterpriseID: state.currentAppUser.EnterPriseID,"FunctionID":state.baseInfo.FunctionID}
    // console.log(body2)
    const data = await fetchData({url, body})
    //if (data.error) return
    if (data.error && (data.msg) ) return showAlert(data.msg);
    commit('setAppThirdPartyPay', data)
  },
  //分享卡包Q
  async fetchCardShareQRCodeData({state, commit}){
    commit('setCardShareQRCodeData', { value: '', image: '', loaded: false } )
    const url = `https://${baseUrl}/Public/AppDataVipOneEntry.ashx`
    const {isFrom, EnterPriseID} = state.currentAppUser;

    const body = {"act": "GetKKQrcode", "EnterpriseID":state.baseInfo.EnterpriseID, "toEnterPriseID":EnterPriseID,"FunctionID":state.baseInfo.FunctionID,"Account":state.member.code,"mac":state.member.mac,"isFrom":isFrom,}
    const data = await fetchData({url, body})
    if (data.error) return
    commit('setCardShareQRCodeData', { value: data.QrCode, image: data.QrCodeImg, loaded: true })
  },

  async fetchMenuData_kakar2({getters, dispatch}) {
    // const url = state.api.publicUrl
    if (getters.subSrcUrl == '') return;
    const url = `${getters.subSrcUrl}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrandProductType"})
    const menuKindIds = await fetchData({url, body})
    if (menuKindIds.error) return
    menuKindIds.forEach(item => dispatch('fetchMenuMainTypeData_kakar2', {code: item.KindID, name: ''}) )
  },
  // (公開) 取 Menu 大類的資料
  async fetchMenuMainTypeData_kakar2({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.mainType.find(item => { return item.code === payload.code }) !== undefined
    if (dataIsOwned) return
    // const url = state.api.publicUrl
    if (getters.subSrcUrl == '') return;
    const url = `${getters.subSrcUrl}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrandProductType", MKindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuMainTypeCodeName', payload)
    commit('setMenuMainTypeListData', { code: payload.code, data: data })
  },
  // (公開) 取 Menu 資料
  async fetchMenuData({state, getters, dispatch}) {
    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType"})
    const menuKindIds = await fetchData({url, body})
    if (menuKindIds.error) return
    menuKindIds.forEach(item => dispatch('fetchMenuMainTypeData', {code: item.KindID, name: ''}) )
  },
  // (公開) 取 Menu 大類的資料
  async fetchMenuMainTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.mainType.find(item => { return item.code === payload.code }) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProductType", MKindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuMainTypeCodeName', payload)
    commit('setMenuMainTypeListData', { code: payload.code, data: data })
  },

  // (公開) 取 某個商品的資料
  async fetchProductData({commit, getters}, payload) {
    let detailID = payload

    // 如果沒有傳payload,表示是從pull request來的,改成直接從url拿到detailID
    if (!detailID) {
      detailID = routerKakar2.currentRoute.params.detailID
    }
    if (getters.subSrcUrl == '') return;
    const url = `${getters.subSrcUrl}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrandProduct", DetailID: detailID})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setCurrentProduct', data)
  },

  // (公開) 取 Menu 小類的資料
  async fetchMenuSubTypeData({state, commit, getters}, payload) {
    // 如果已經有資料則離開
    const dataIsOwned = state.publicData.menu.subType.find(item => item.code === payload.code ) !== undefined
    if (dataIsOwned) return

    const url = state.api.publicUrl
    const body = getters.appBody({act:"GetBrandProduct", KindID: payload.code})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuSubTypeCodeName', payload)
    commit('setMenuSubTypeListData', { code: payload.code, data: data } )
  },
  // (公開) 取 Menu 小類的資料
  async fetchMenuSubTypeData_kakar2({state, commit, getters}, ifExistSkip=false) {

    let kindID = routerKakar2.currentRoute.params.id // 改成直接從url拿到kindID
    // 如果可以忽略最新資料
    if (ifExistSkip) {
      // 如果已經有資料則離開
      const dataIsOwned = state.publicData.menu.subType.find(item => item.code === kindID ) !== undefined
      if (dataIsOwned) return
    }

    // const url = state.api.publicUrl
    if (getters.subSrcUrl == '') return;
    const url = `${getters.subSrcUrl}/public/newsocket.ashx`
    const body = getters.appBody({act:"GetBrandProduct", KindID: kindID})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMenuSubTypeCodeName', {code: kindID})
    commit('setMenuSubTypeListData', { code: kindID, data: data } )
  },
  fetchFoodmarketMulti({state,dispatch}){
    // console.log("fetchFoodmarketMulti",state.mallData.foodkind);
    //迴圈一起跑會GG (API無法負荷??),已改為1筆1筆取,第一筆回應,再next
    if (Array.isArray(state.mallData.foodkind) && state.mallData.foodkind.length > 0) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[0]["ID"],kIndex:0})
  },
  // 取全部小類下商品
  async fetchFoodmarketData({commit,getters,dispatch},p_obj) {
    const url =  getters.appBookUrlWuhu;
    // console.log("fetchFoodmarketData",url,p_obj.kindID,p_obj.kIndex);
    const body = getters.appBodyWuhu({"act": 'get_foodmarket',"FoodKind": p_obj.kindID});
    const data = await fetchData({url, body});
    var isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != "0";

    if (isErr || data.error) return
    commit('setFoodmarketData', {foods: (data.FoodMarket || []), kindID: p_obj.kindID})
    const p_index = p_obj.kIndex+1;
    //已改為1筆1筆取,等待第一筆回應,再next
    if (state.mallData.foodkind.length > p_index) dispatch('fetchFoodmarketData',{kindID:state.mallData.foodkind[p_index]["ID"],kIndex:p_index})

  },
  /* 企業會員資料相關 */
  // 取會員的消費紀錄
  async fetchMemberPurchaseRecordData({commit, getters}) {
    const url = getters.subTokenApiUrl
			,body 	= getters.appBody({"act":"GetVipPosItems"})
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 購買交易紀錄 */
		// let data = await GetJsonTest('purchaseRecord')	// @@
    commit('setMemberPurchaseRecordData', data)
  },
  // (公開) 取 currentApp 的公開資料 - card/Pay
  async fetchAppPublicDataDetailPurchaseWuhu({dispatch}){
    // 取企業 app 的 LOGO
    dispatch('fetchAppLogoDetail')

	dispatch('fetchMemberPurchaseRecordData')
  },
  // 取會員的積點交易紀錄
  async fetchMemberExpiredPointsData({state, commit, getters}) {
    const url = state.api.memberUrl
    const body = getters.appBody({"act":"GetVipRecord", "TradeTypeCode":"4,-4,6,-6,7,-7,15,-15,16,-16,17,-17,71,-71,72,-72"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberExpiredPointsData', data)
  },
  // 取會員的積點交易紀錄 - 五互
  async fetchMemberExpiredPointsDataWuhu({state, commit, getters}) {
    const url = state.api.memberUrl
    // 4,   -4: 積分兌換, 積分兌換退回
    // 7,   -7: 積分換券, 積分換券退回
    // 15, -15: 公關贈點, 公關贈點退回
    // 16, -16: 活動贈點, 活動贈點退回
    // 17, -17: 商品贈點, 商品贈點退回 ==文字改為==> 滿額贈點、滿額贈點退回
    // 23, -23: 首購禮,   首購禮退回
    // 71, -71: 點數轉贈, 點數轉贈退回
    // 72, -72: 點數受贈, 點數受贈退回
    const body = getters.appBodyWuhu({"act":"GetVipRecord", "TradeTypeCode":"4,-4,7,-7,15,-15,16,-16,17,-17,23,-23,31,-31,71,-71,72,-72"})
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberExpiredPointsDataWuhu', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  // 秋林版 - 目前已改用黃大哥版(如下)
  async fetchMemberConsumerPointsDataWuhu_bak({state, commit, getters}) {
    const url = state.api.consumerUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_Points_Info_List_all",
      "FunctionID":"950201",
      "orderby":"end_day desc, deed_no desc",
      "srow": 0 // srow=0代表傳全部
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的[消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDataWuhu({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    const dynamicBody = (payload || {})
    let fixData = {
      "act":"get_deedpoints",
      "orderby":"end_day desc, deed_no desc"
    }
    let allData = Object.assign({}, fixData, dynamicBody)

    const body = getters.appBodyWuhu(allData)
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointsDataWuhu', data)
  },
  // 取會員的單一筆[契約][消費點數紀錄]=[契約] - 五互
  async fetchMemberConsumerPointsDetailWuhu({state, commit, getters}, payload) {
    const url = state.api.consumerUrl
    const body = getters.appBodyWuhuEncode({
      "act":"WUHU_Deed_PointsBalance_Trade",
      "FunctionID":"950201",
      "orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "deed_no": btoa(payload.deed_no)
    })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberConsumerPointDetailWuhu', data)
  },
  //取得單一筆未完成訂單
  async fetchUnfinishorder_single({commit, getters}, payload) {
    commit('setSingleUnFinish', [])
    const url = getters.subConsumerUrl
    const body = getters.appBodyEncode_sub({
      "act":"get_unfinishorder_qry",
      "FunctionID":"950201",
      //"orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": btoa(payload.ID)
    })
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setSingleUnFinish', data)
  },
   //取得已完成訂單
   async fetchOrder_single({commit, getters}, payload) {
    commit('setSingleFinish', [])
    const url = getters.subConsumerUrl
    const body = getters.appBodyEncode_sub({
      "act":"get_myorder_qry",
      "FunctionID":"950201",
      //"orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": btoa(payload.ID)
    })
    const data = await fetchData({url, body})

    if (data.error) return

    commit('setSingleFinish', data)
  },
  //取得使用者選的超商Info
  async fetchOrder_cvsShop({commit, getters}, payload) {
    //localhost以及staging要在test資料庫撈
    commit('setCvsShopInfo', [])
    let url = getters.subConsumerUrl;

    if (S_Obj.isDebug || S_Obj.isStaging) url = "https://8012test.jh8.tw/public/AppDataVIP.ashx"
    const body = getters.appBodyEncode_sub({
      "act":"myCvsShop_950201",
      "FunctionID":"950201",
      //"orderby":"LastModify",
      "srow": 0 // srow=0代表傳全部
    }, {
      "OrderID": btoa(payload.ID)
    })
    const data = await fetchData({url, body})

    if (data.error) return
    commit('setCvsShopInfo', data);
    IsFn(payload.fn) && payload.fn();
  },
  // 取會員的單一筆[訂單][消費點數紀錄] - 用Order No
  async fetchOneOrderByOrderNo({/*state,*/ commit, getters}, payload) {
    const url = getters.appBookUrlWuhu

    if (payload) {
      const dynamicBody = (payload.requestBody || {})
      let fixData = {
        "act":"get_orderitems"
      }
      let allData = Object.assign({}, fixData, dynamicBody)

      const body = getters.appBodyWuhu(allData)
      const data = await fetchData({url, body})
      if (data.error) return

      let orderDetail = data?data.OrderItem:null

      commit('setOneOrderDetail', {
        orderDetail: orderDetail,
        arrayIndex: payload.arrayIndex
      })
    }

  },
  async ensureGetDeviseGPS({/*state,*/ commit}) {
    //
    if (IsMobile()) {
      // (在殼裡面) call 跟殼取 GPS 資訊的 API 與 callback function
      deviseFunction('gps', '', '"cbFnSyncDeviseGPS"')
    } else {
      // 不在殼裡面
      if ("geolocation" in navigator) {
        /* geolocation is available */
        navigator.geolocation.getCurrentPosition((position)=>{
          //console.log('navigator ===> position =>', position)
          // 轉成 google maps api 需要的預設格式
          const gpsData = {gps: {
            lat: position.coords.latitude,
            lng: position.coords.longitude} }
          // 將 gps 存入 state.baseInfo
          commit('setBaseInfo', gpsData)
        }, ()=>{
          //console.log('navigator ===> error => no navigator')
        })
      } else {
        /* geolocation IS NOT available */
      }
    }
  },
  // 取可領用的優惠卷列表資訊 + 領取優惠券 - 五互
  // async fetchMemberDrawTicketListDataWuhu({state, commit, getters}){
    // const url = state.api.memberUrl
    // const body = getters.appBodyWuhu({ act: "GetDrawTicketList" })
    // let data = await fetchData({url, body})
    // if (data.error) return

		// /** @@Test 可領用的優惠卷列表 */
		// // data = await GetJsonTest('drawTicketList')	// @@
    // commit('setMemberDrawTicketListData', data)
  // },

  /* 自動領券(單純告訴後端,要開始領了的觸發act... */
  async fetchGetDrawAutoTicketWuhu({getters}){
		if (!getters.isLogin)		return

		const epid 	= state.currentAppUser.EnterPriseID
			,npos 		= ArrGetATicketed.indexOf(epid)
			,hasGeted	= npos > -1
		if (hasGeted) {
			return new Promise(resolve => resolve(true))
		}
    const url = getters.subTokenApiUrl
			,body 	=	getters.appBody({ act: "getdrawautoticket" })
			,data 	= await fetchData({url, body})
			,hasOK	= !data.error
		return new Promise((resolve) => {
			/** @Important: 秋哥說: 啟動後同一卡包,只發1次即可,不需再發 */
			if (hasOK && !hasGeted) {
				ArrGetATicketed.push(epid);
			}
			resolve(hasOK);
		})
  },
  /* 取優惠卷列表資訊 */
  async fetchMemberTicketListData({commit, getters}){
    const url = getters.subTokenApiUrl
			,body 	=	getters.appBody({ act: "GetVipTicket" })
			,data 	= await fetchData({url, body})
    if (data.error) return
    commit('setMemberTicketListData', data)
  },
  // 取優惠卷列表資訊 - 五互
  async fetchMemberTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
    const body = getters.appBodyWuhu({ act: "GetVipTicket" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberTicketListDataWuhu', data)
  },
  // 取已贈送的優惠卷列表資訊
  async fetchMemberGaveTicketListData({commit, getters}){
    const url = getters.subTokenApiUrl
    let body = getters.appBody({ act: "GetGaveTicketRecord" })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setMemberGaveTicketListData', data)
  },
  // 取已贈送的優惠卷列表資訊 - 五互
  async fetchMemberGaveTicketListDataWuhu({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	 getters.appBodyWuhu({ act: "GetGaveTicketRecord" })
			,data 	=  await fetchData({url, body})
    if (data.error) return

		/** @@Test 已贈送的優惠卷列表資訊 */
		// let data = await GetJsonTest('gaveTicketList')	// @@
    commit('setMemberGaveTicketListDataWuhu', data)
  },
  // 取尚未能使用的優惠卷資料
  async fetchMemberNotAvailableTickets({ commit, getters}){
    const url = getters.subTokenApiUrl
			,body 	=	getters.appBody({ act: "NotTodayTicketList" })
			,data 	= await fetchData({url, body})
    if (data.error) return

    commit('setMemberNotAvailableTickets', data)
  },
  // 取尚未能使用的優惠卷資料 - 五互
  async fetchMemberNotAvailableTicketsWuhu({state, commit, getters}){
    const url = state.api.memberUrl
			,body 	=	getters.appBodyWuhu({ act: "NotTodayTicketList" })
			,data 	= await fetchData({url, body})
    if (data.error) return

		/** @@Test 可領用的優惠卷列表 */
		// let data = await GetJsonTest('notAvailableTickets')	// @@
    commit('setMemberNotAvailableTicketsWuhu', data)
  },
  // 優惠卷贈送
  async ensureMemberTicketTrans({getters}, payload){
    const url = getters.subTokenApiUrl
			,body 	=	getters.appBody(Object.assign({}, { act: "SetTicketToOther" }, payload ))
			,data 	= await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },
  // 取優惠卷明細的使用條款與兌換條件
  async fetchMemberTicketInfoData({dispatch}, ticket){
    // 取該卷使用的 QRcode
    dispatch('fetchTicketQRCodeData', ticket.GID)
    // 取該卷的條款與規則
    dispatch('fetchTicketTradeRules', ticket.TicketTypeCode)
  },
  // 取優惠卷條碼 QRcode
  async fetchTicketQRCodeData({commit, getters}, ticketGID){
    // 先清空現在的 QRcode 資料
    commit('setTicketQRCodeData', {gid: '', image: ''} )
    const url = getters.subTokenApiUrl
    let body = getters.appBody({ act: "GetTicketQrCodeStr", GID: ticketGID, QRCodeScale: 4 })
    const data = await fetchData({url, body})
    if (data.error) return

    commit('setTicketQRCodeData', {gid: data.QrCode, image: data.QrCodeImg} )
  },
  // 取優惠卷明細資訊: 規則條款
  async fetchTicketTradeRules({commit, getters, dispatch}, TypeCode){
    const url = getters.subTokenApiUrl
    const body = getters.appBody({ act: "GenerateTicketTradeRules", TicketTypeCode: TypeCode })
    let data = await fetchData({url, body})
    if (data.error) return
    // console.log('取優惠卷明細資訊_data: ', data);	// @@

		/** @@Test 優惠卷明細資訊-規則條款 */
		// let data = await GetJsonTest('ruleData')	// @@

		const one = data.length ? data[0] : null;
		if (one) {
			// 取優惠卷的兌換條件: 商品
			dispatch('fetchTicketUsageRuleFoods', one.TradeRuleCode)
			// 取優惠卷的兌換條件: 門店
			dispatch('fetchTicketUsageRuleShops', one.TradeRuleCode)
			// 設定使用條款與適用日
			commit('setMemberTicketInfoData', { ruleData: one, currentTypeCode: TypeCode })
		}
  },
  // 取優惠卷明細資訊的兌換條件: 商品
  async fetchTicketUsageRuleFoods({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return

    const url = getters.subTokenApiUrl
    let body = getters.appBody({ act: "GenerateTicketTradeRuleFoods", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    if (data.error) return

		/** @@Test 優惠卷明細資訊的兌換-商品 */
		// let data = await GetJsonTest('usageFoods')	// @@
    commit('setMemberTicketInfoData', { usageFoods: data, currentRuleCode: RuleCode } )
  },

  // 取優惠卷明細資訊的兌換條件: 門店
  async fetchTicketUsageRuleShops({state, commit, getters}, RuleCode){
    // 已有相同資料就不再取
    if (state.memberData.ticketInfo.currentRuleCode === RuleCode) return

    const url = getters.subTokenApiUrl
    const body = getters.appBody({ act: "GenerateTicketTradeRuleShops", TradeRuleCode: RuleCode })
    let data = await fetchData({url, body})
    // console.log('取優惠卷兌換門店-data: ', data);	// @@

    if (data.error) return

		/** @@Test 優惠卷明細資訊的兌換-門店 */
		// let data = await GetJsonTest('usageShops')	// @@
    commit('setMemberTicketInfoData', { usageShops: data, currentRuleCode: RuleCode })
  },
  // 優惠卷兌換門市
  async ensureTicketExchange({getters}, payload){
	// 要9001...AppDataVIP.ashx 才對
	const url = getters.subConsumerUrl
    // const body = getters.appBodyWuhu(Object.assign({}, { act: "SetTicketExchange" }, payload ))
    const body = getters.appBodykaka(Object.assign({}, { act: "SetTicketExchange" }, payload ))
    const data = await fetchData({url, body})
    if (data.error) return data.detail

    return data
  },

	/* 針對綁定過了,做偷偷登入才能存取... */
	async getAPIToken({dispatch,state, commit, getters}, payload) {
    if ((state.member.code || '') === '') {showAlert('會員編號遺失!!'); return dispatch('memberLogout');}
		const head	= { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
			,url 			= `https://${getters.getBaseUrl}/Public/AppDataVipOneEntry.ashx`
			,body 		= {
				"act": 'login',
				"memberphone": state.member.code,
				"memberPwd": state.member.pwd,
				"isFrom": payload.isFrom,
				"EnterpriseID": payload.EnterpriseID
			}

		const {data} 	= await axios.post( url, body, head )
			,hasOK			= data.ErrorCode == 0
		if (hasOK) {
			const tokenDataObj = JSON.parse(data.Remark)
			// console.log('getAPIToken-tokenDataObj: ', tokenDataObj);	// @@
			// 把Tokenkey存入
			tokenDataObj.length && commit('setCurrentAppUserToken', tokenDataObj[0].Tokenkey)

		} else {
			return showAlert(data.ErrorMsg+"!");
		}

		return new Promise(resolve => {resolve(hasOK)})
	},
  async fetchKKGrouplist({dispatch,state,getters}, payload){
    const url = `https://${getters.getBaseUrl}/Public/AppVipOneEntry.ashx`;

    const body = {act:"GetKKGrouplist",
      EnterPriseID:state.baseInfo.EnterpriseID,
      MEnterpriseID:payload.EnterpriseID,
      Account:state.member.code,
      FunctionID:"450403",
      mac:state.member.mac};

    const head = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
    const {data} = await axios.post( url, body, head )
    if (data.ErrorCode == '0' && data.Remark) {
      const gpList = JSON.parse(data.Remark);
      gpList.forEach((tGp,tIndex)=>{
        setTimeout(function() {
          if (!tGp.iskakar) dispatch('ensureNewCardQRCode', {Qr:tGp.QrCode,isChkSub:false,isHideMsg:true})

        }, 1000 * tIndex);


      });

    }
    return new Promise(resolve => resolve(data))
  },
  /* 手機掃碼QrCode *///{Qr:QrCode,isChkSub:true}
  async ensureNewCardQRCode({dispatch, state, commit, getters}, p_Info){
		const head	= { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }
			,url 			= `https://${getters.getBaseUrl}/Public/AppVipOneEntry.ashx`
			,body 		= Object.assign({}, getters.kakarBody(), {act: "SetKKVipAPP",QrCode: p_Info.Qr,})
			,res			= await axios.post(url, body, head)
			// ,hasOK		= res.data.ErrorCode == 0
			,_msg 		= res.data.ErrorMsg
			,isMobile	= IsMobile()

		// return res

/** @QQ: Test臨時開關
const _txt = 'QrCode: '+QrCode+'\n'
					+ 'body: '+JSON.stringify(body)+'\n'
					+ '_msg: '+_msg
// alert(_txt);	// @@
// console.warn('手機掃碼QrCode: ', _txt);	// @@
 */

		//!isMobile && commit('setInputQRCode', false)
    commit('setInputQRCode', false)//isMobile也要自動關掉掃碼視窗.
    //const isAddedCard = (res.data.ErrorCode == 0 ? true : _msg == '綁定過了')
		if (res.data.ErrorCode == 0) {
      let p_response = null
        if (res.data.Remark) {
          p_response = JSON.parse(res.data.Remark)
          if (p_response.length) {
            //const one = p_response[0]

            const one = p_response.find(s_res => s_res.QrCode === p_Info.Qr)
            if (one && p_Info.Qr) commit('setThirdID')
            const {FromTDID} = state.member

            if (one && p_Info.Qr && FromTDID && FromTDID != undefined){
              //新綁卡,做第三方登入
              //await dispatch('fetchVipAppsOnlyList', {enterpriseID:one.EnterpriseID,isOnly:true})
              commit('setVipApps', p_response)
              const user = state.vipUserData.find(item => item.EnterPriseID === one.EnterpriseID)
              if (IsFn(p_Info.fn)) p_Info.fn();

              if (user){
                user.EnterpriseID = one.EnterpriseID;
                if (!p_Info.isHideMsg) await dispatch('AccessInApp', user)
              }
            }else{
              if (!p_Info.isHideMsg) dispatch('fetchVipApps')
            }
            if (p_Info.isChkSub) await dispatch('fetchKKGrouplist', one);//聯盟加卡
          }

        }

        if (isNotLinePara() && !p_Info.isHideMsg) showAlert(_msg)	// 只有綁定成功和非綁定過要秀訊息,LINE應用不show

			if (isMobile) {
				deviseFunction('setIO', '1', '')
				// if (getters.router.fullPath != '/list') getters.router.replace({path: '/list'})
				getters.router.push({path: '/list'})
			}

		} else {

			// 綁定過了 => 直接到該品牌首頁
			if (_msg == '綁定過了') {

        // 動態撈response裡的企業號
        let responseData = null
        if (res.data.Remark) {
          responseData = JSON.parse(res.data.Remark)
          if (responseData.length) {
           // const one = responseData[0]
           const one = responseData.find(s_res => s_res.QrCode === p_Info.Qr)
            let act_login;
            const {FromTDID} = state.member
            const {EnterpriseID, isFrom} = one//

            if (one && p_Info.Qr && FromTDID && FromTDID != undefined){
              //綁過卡,做第三方登入
              const user = state.vipUserData.find(item => item.EnterPriseID === EnterpriseID)
              if (user){
                user.EnterpriseID = EnterpriseID;
                if (!p_Info.isHideMsg) await dispatch('AccessInApp', user)
              }
              if (!user) act_login = await dispatch('memberThirdChildLogin',  {EnterpriseID, isFrom, FromTDID})

            }else if(one){
              if (!p_Info.isHideMsg) act_login = await dispatch('getAPIToken', one)

            }

            if (act_login && one) getters.router.push({path: `/card/${one.EnterpriseID}`})
            if (isMobile) {
              deviseFunction('setIO', '1', '')
            } else {
              commit('setCardListScrollMap', document.querySelector(".pull-down-container").scrollTop);
              state.cardListScroll2 = window.pageYOffset
            }
            if (p_Info.isChkSub) await dispatch('fetchKKGrouplist', one);//聯盟加卡
          }
        }

			} else {
				if (getters.isNotLinePara) showAlert(_msg)	// 只有綁定成功和非綁定過要秀訊息
			}
		}


  },

}


export default new Vuex.Store({ state, mutations, getters, actions })
