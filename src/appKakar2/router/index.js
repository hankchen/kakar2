// vue basic lib
import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// vue router 設定(桌面版)
const routes = [
  // === 主頁面 ===
  { path: '/',  meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeIndex.vue') },
  { path: '/list', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/Index.vue') },
  { path: '/member',  meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeMember.vue') },
  { path: '/news',  meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeNews.vue') },
  { path: '/newspage/:id',  meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeNewspage.vue') },
  { path: '/store', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeStore.vue') },
  { path: '/openUrl/:pageName', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/openUrl.vue') },
  { path: '/point', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomePoint.vue') },
  { path: '/consumePoint', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeConsumePoint.vue') },
  { path: '/coupon', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeCoupon.vue') },
  { path: '/booking', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeBooking.vue') },
  // { path: '/bookingClean', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeBookingClean.vue') },
  { path: '/bookingHotel', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeBookingHotel.vue') },
  // { path: '/bookingMine', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeBookingMine.vue') },
  { path: '/shop', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeShop.vue') },
  { path: '/shopType', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeShopType.vue') },
  { path: '/shopCart', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeShopCart.vue') },
  { path: '/shopMine', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeShopMine.vue') },
  { path: '/contactUs', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeContactUs.vue') },
  { path: '/faq', meta: { layout: 'Home' }, component: () => import('src/appKakar2/pages/HomeFaq.vue') },
  // === 卡包列表 ===
  { path: '/login', component: () => import('src/appKakar2/pages/Login.vue') },
  { path: '/setting', component: () => import('src/appKakar2/pages/Setting.vue') },
  { path: '/register', component: () => import('src/appKakar2/pages/Register.vue') },
  { path: '/qreader', component: () => import('src/appKakar2/pages/QReader.vue') },
  { path: '/echart1', component: () => import('src/appKakar2/pages/EChart1.vue') },
  { path: '/404' },
  // === 企業 APP 系列 ===
  // 企業首頁
  { path: '/card/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Index.vue') },
  // 會員頁
  { path: '/cardMember/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Member.vue') },
  // 分享Qrcode
  { path: '/cardQrcode/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Qrcode.vue') },  
  // 積點紀錄
  { path: '/cardPoint/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Point.vue') },
  // 積點換券
  { path: '/cardExchange/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Exchange.vue') },
  // 消費紀錄
  { path: '/cardPay/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Pay.vue') },
  // 優惠卷
  { path: '/cardCoupon/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Coupon.vue') },
  // 會員資訊
  { path: '/cardProfile', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Profile.vue') },
  // // 門市查詢
  { path: '/cardStore/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Store.vue') },
  // 產品列表頁
  { path: '/cardMenu/:id', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Menu.vue') },
  // // 產品介紹頁
  { path: '/cardProducts/:detailID', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Products.vue') },
  // 消息列表頁
  { path: '/cardNews/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/News.vue') },
  // 消息介紹頁
  { path: '/cardNewspage/:enterpriseId/:id', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Newspage.vue') },
  // // 常見問題頁
  { path: '/cardFaq/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Faq.vue') },
  // 商品禮卷(寄杯)頁
  { path: '/cardGiftVouchers', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/GiftVouchers.vue') },
  // 集點換券頁
  { path: '/pointExg', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages/cards/Exchange.vue') },
  // 我的訂單(商城 & 外帶外送)
  { path: '/cardMyOrders/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/MyOrders.vue') },
	// 服務紀錄
  { path: '/serviceRec/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages/cards/ServiceRec.vue') },

  // 商城首頁
  { path: '/cardShop/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/Shop.vue') },
  // 購物車
  { path: '/cardShopCart/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/ShopCart.vue') },
  // 訂單記錄
  { path: '/cardShopMine/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/ShopMine.vue') },
  // 商品頁
  { path: '/cardShopType/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages//cards/ShopType.vue') },  
	// 服務預約
  { path: '/bookingClean/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages/HomeBookingClean.vue') },
	// 我的預約
  { path: '/bookingMine/:enterpriseId', meta: { layout: 'Card' }, component: () => import('src/appKakar2/pages/HomeBookingMine.vue') },
  
]

export default new VueRouter({
  routes,
  scrollBehavior: function (to) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
  },
})
 
// fix相同的路由click出錯
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}