import Vue from 'vue'
import VueCookies from 'vue-cookies'

import axios from 'axios'
import store from '../store'
// import { deviseFunction } from 'src/libs/deviseHelper.js'
import { S_Obj } from 'src/s_obj.js'
//import { IsFn } from 'src/fun1.js'
Vue.use(VueCookies)

// const baseUrl = 'https://wuhuapp.jh8.tw/Public/AppCloudOneEntry.ashx'
const baseUrl = 'https://kakar2.jh8.tw/Public/AppCloudOneEntry.ashx'
const baseHead = { "headers": { "Content-Type": "application/x-www-form-urlencoded" } }

// 將取得的資料做 JSON.parse, 並檢驗資料格式是否正確
function parse(data) {
  let pData ;
  if (typeof data.Remark === "string") {
    data.Remark = data.Remark.replace(/\t/g, '');
    // eslint-disable-next-line no-control-regex
    data.Remark = data.Remark.replace(//g, '');
    data.Remark = data.Remark.replace(/ {2}/g, "");
    // eslint-disable-next-line no-control-regex
    data.Remark = data.Remark.replace(//g,"");
    data.Remark = data.Remark.replace(/\v/g, "");
  }

  try { pData = JSON.parse(data.Remark) }
  catch(e) { pData = { error: true, msg: 'JSON parse failure', detail: e } }
  return pData
}

async function getToken(reqHead) {
  // let tokenUrl = store.getters.appTokenUrlWuhu
  let tokenUrl = `https://${store.getters.getBaseUrl}/Public/AppDataVipOneEntry.ashx`
  let tokenBody = {
    "act":"login",
    "memberphone":store.state.member.code,
    "memberPwd":store.state.member.pwd,
    "isFrom": store.state.baseInfo.isFrom,
    "EnterpriseID": store.state.baseInfo.EnterpriseID

  }
  return parseToken(await axios.post(tokenUrl, JSON.stringify(tokenBody), reqHead),true)
  // let tokenData = await axios.post(tokenUrl, tokenBody, reqHead)

  // let tokenDataObj = parse(tokenData.data)

  // let newTokenkey = ''

  // if (tokenDataObj && typeof tokenDataObj !== 'undefined'
  //   && tokenDataObj.length>0) {
  //   newTokenkey = tokenDataObj[0].Tokenkey //??? 當cookie被清空時 Uncaught (in promise) TypeError: Cannot read property '0' of null

  //   // console.log('fetchData.js getToken ===> new Tokenkey =>' + newTokenkey)

  //   store.commit('setMainToken', newTokenkey)

  //   // // 更新密碼到cookie和store
  //   // store.state.member.Tokenkey = newTokenkey
  //   // // 非殼才要存到cookie
  //   // if (typeof(JSInterface) == 'undefined') {
  //   //   // console.log('fetchData.js getToken ===> in browser => save token to cookies')
  //   //   VueCookies.set('kkTokenkey', newTokenkey)
  //   // }

  //   // // 更新token到殼
  //   // let setThing = `{"spName":"Tokenkey", "spValue": "${newTokenkey}"}`
  //   // // console.log('fetchData.js getToken ===> setThing =>' + setThing)
  //   // deviseFunction('SetSP', setThing, '')
  // }

  // return newTokenkey

}
async function getSubsidiaryToken(reqHead) {
  let url = `https://${store.getters.getBaseUrl}/Public/AppDataVipOneEntry.ashx`

  let tokenBody = {
    "act":"login",
    "memberphone":store.state.member.code,
    "memberPwd":store.state.member.pwd,
    "isFrom": store.state.currentAppUser.isFrom,
    "EnterpriseID": store.state.currentAppUser.EnterPriseID

  }
  return parseToken(await axios.post(url, JSON.stringify(tokenBody), reqHead))
}
async function getSubsidiaryThirdToken(reqBody) {
  //console.log("mmmm>",store.state.member)
  const url = store.state.api.consumerPubUrl;
  const {EnterpriseID,isFrom} = reqBody;
  const {FromTDID} = store.state.member;
  const tokenBody = {
    isFrom, EnterpriseID,FromTDID,
    "act": "membersthirdlogin",
    "isFromTD": "LINElogin",
  }
  return parseToken(await axios.post(url, JSON.stringify(tokenBody)))

}
function parseToken(tokenData,isMain){
  if (!tokenData) return '';
  let tokenDataObj = parse(tokenData.data)

  let newTokenkey = ''
  if (tokenDataObj && typeof tokenDataObj !== 'undefined'
    && tokenDataObj.length>0) {
    newTokenkey = tokenDataObj[0].Tokenkey

    // 把Tokenkey存入
    if (!isMain) store.commit('setCurrentAppUserToken', newTokenkey)
    if (isMain) store.commit('setMainToken', newTokenkey)
  }
  return newTokenkey
}
// function mainParseToken(tokenData){
//   if (!tokenData) return '';
//   let tokenDataObj = parse(tokenData.data)
//   console.log("mainParseToken",tokenData)
//   let newTokenkey = ''
//   if (tokenDataObj && typeof tokenDataObj !== 'undefined'
//     && tokenDataObj.length>0) {
//     newTokenkey = tokenDataObj[0].Tokenkey
//     console.log("mainParseToken",newTokenkey)
//     store.commit('setMainToken', newTokenkey)
//   }
//   return newTokenkey
// }
async function getKakarThirdToken(reqBody) {
  const {FromTDID} = store.state.member;
  const {EnterpriseID,isFrom} = reqBody;
  const url = store.state.api.consumerUrl
  const tokenBody = {
    isFrom, EnterpriseID,
    "act": "membersthirdlogin",
    "isFromTD": isFrom,
    "FromTDID": FromTDID
  }

  return parseToken(await axios.post(url, JSON.stringify(tokenBody)),true)
}
async function fetchData(payload={}) {
  let reqBody 	= payload.body || {}
  const {url, head, isSubsidiary} = payload
	,reqUrl 	= url || baseUrl
	,reqHead 	= head || baseHead
	,mainEID	= store.state.baseInfo.EnterpriseID		// app主企業
	,subEID		= reqBody.EnterpriseID								// 卡包
	,apitoken	= subEID == mainEID ? 'Tokenkey' :'ApiToken'
	,_token		= S_Obj.read(apitoken)
	,isNoLine	= store.getters.isNotLinePara

// console.warn('fetchDatatoken失效: ', _token);	// @@
  if (isNoLine) reqBody.Tokenkey = _token

/** @QQ: Test臨時開關
	const _txt = 'subEID: '+subEID+'\n'
						+ 'reqUrl: '+reqUrl+'\n'
						+ 'isFrom: '+reqBody.isFrom+'\n'
						+ 'body: '+JSON.stringify(reqBody)+'\n'
	console.warn('fetchData: ', _txt);	// @@
 */

	let p_reqUrl = reqUrl;
  //0920539935,8012test
 // console.log("tttaa>>",location.href,reqUrl)
  // if (/localhost/.test(location.href) && /9001/.test(reqUrl)) p_reqUrl = p_reqUrl.replace('9001', '8012test');

  // 打 API 取資料
  let {data} = await axios.post(p_reqUrl, reqBody, reqHead)
  if (isNoLine){
    // 999: token失效 => 導到登入頁
    if (data.ErrorCode === '999') {

/** @Debug: 疑失token問題 */	// @@test
	const _txt = 'subEID: '+subEID+'\n'
						+ 'reqUrl: '+reqUrl+'\n'
						+ 'body: '+JSON.stringify(reqBody)+'\n'
const account = store.state.member.code
	,IsHank = /270485/.test(account);	// @@test
IsHank && alert('疑失token問題: \n'+_txt);	// @@

      //console.log('token失效 ===> 登出到登入頁')
      // showAlert('密碼已修改,請重新登入')
      if (isSubsidiary) VueCookies.remove('kkTokenkey_'+reqBody.EnterpriseID) //line應用產生
      //VueCookies.set('kkTokenkey_'+state.currentAppUser.EnterPriseID, '');
      store.dispatch('memberLogout')
      data.ErrorCode = 0
      data.ErrorMsg = null

    } else if (data.ErrorCode === '998') { // 998: token過期
      if (isSubsidiary) VueCookies.remove('kkTokenkey_'+reqBody.EnterpriseID) //line應用產生
      const isLogin = store.getters.isLogin
      //console.log('token過期 ===> isLogin =>' + isLogin)

      if (!isLogin) {
        //console.log('token過期 ===> logout狀態不重撈token')
      } else {
        //console.log('token過期 ===> login狀態需重撈token')
        // 重撈token
        if (subEID == mainEID) {
          const tokenKey = await getToken(reqHead)
          reqBody.Tokenkey = tokenKey
          S_Obj.save(apitoken, tokenKey);
        }else{
          reqBody.Tokenkey = await getSubsidiaryToken(reqHead)
        }

        // 用重撈的token再call一次api
        data = await axios.post(reqUrl, reqBody, reqHead)
        if (data.data.ErrorCode == 0 && data.data.Remark) {
          data = data.data
        }
      }
    }
  }

  //LINE應用,第三方登入,重取token
  if (!isNoLine && store.state.member.FromTDID && (data.ErrorCode === '999' || data.ErrorCode === '998')){
    //let p_body = Object.assign({}, reqBody )
    if (subEID == mainEID){
      //kakar主企業
      VueCookies.remove('kkTokenkey')
      store.state.member.Tokenkey=''
      await getKakarThirdToken(reqBody)
      reqBody.Tokenkey = store.state.member.Tokenkey;
    }else{
      //卡包
      VueCookies.remove('kkTokenkey_'+reqBody.EnterpriseID)
      store.state.currentAppUser.token='';
      await getSubsidiaryThirdToken(reqBody)
      reqBody.Tokenkey = store.state.currentAppUser.token;
    }

    //if (reqBody.EnterpriseID) return window.location.reload()//可能會造成無限reload...再想想

   // 用重撈的token再call一次api
    //console.log("reqBody>>",p_body)
    data = await axios.post(reqUrl, reqBody, reqHead)
    if (data.data.ErrorCode == 0 && data.data.Remark) {
      data = data.data
    }
  }

/*
  if (data.ErrorCode === '999' || data.ErrorCode === '998') { // 999: token失效(過期)

    // console.log('token失效(過期) ===> store.getters.isLogin =>' + store.getters.isLogin)	// @@
    if (store.getters.isLogin === false) {
      // console.log('token失效(過期) ===> logout狀態不重撈token')
    } else {
      // console.log('token失效(過期) ===> login狀態需重撈token')
      // 重撈token
      if (!isSubsidiary) {reqBody.Tokenkey = await getToken(reqHead)}else {reqBody.Tokenkey = await getSubsidiaryToken(reqHead)}

      // 用重撈的token再call一次api
      data = await axios.post(reqUrl, reqBody, reqHead)
      if (data.data.ErrorCode === '0' && data.data.Remark !== '') {
        data = data.data
      }
    }
  }
 */

  // 資料驗證
  if (data.error) return data
  // 非秋林 API 的結構直接 return
  if (!data.ErrorCode && !data.ErrorMsg && !data.Remark) {data.error=true ;return data}
  // 秋林 API 如果 Remark 是空字串 return
  if (data.ErrorCode === '0' && data.Remark === '') return data
  // 秋林 API 如果 Remark 是null return
  if (data.ErrorCode === '0' && !data.Remark) {data.Remark=''; return data}
  // 秋林 API 如果 ErrorMsg 有值 return
  if (data.ErrorCode === '0' && data.ErrorMsg !== '') {
    const p_data = parse(data);
    p_data.ErrorMsg = data.ErrorMsg;
    return p_data;
  }
  // 秋林 API 如果報錯 return error
  if (data.ErrorCode !== '0') return { error: true, msg: data.ErrorMsg, detail: data }

  // 資料回傳
  return parse(data)

}

export default fetchData
