import axios from 'axios'

// 通用打 API function
const performApi = (url, payload={}) => {
  let {method, body, head} = payload
  method = method || 'get'
  body = body || {}
  head = head || {}

  // 錯誤訊息回傳
  const setError = (msg) => { return { error: true, msg: msg } }

  return new Promise(async (resolve) => {
    if (!url || url === '') resolve( setError('沒有 API url') )

    try {
      let request
      if (method === 'get')    request = await axios.get(url, body, head)
      if (method === 'post')   request = await axios.post(url, body, head)
      if (method === 'patch')  request = await axios.patch(url, body, head)
      if (method === 'delete') request = await axios.delete(url, body, head)

      resolve(request.data)
    } catch(err) {
      resolve( setError(err) )
    }
  })
}

export default performApi
