/*
  == click/touch 監聽器 ==
  設計來做指定 DOM 以外的地方被 click/touch 時，觸發定好的 vue method e.g.

  <div ref="logoutDropdownButton" v-if="isLogin" class="dropdown btn-group b-dropdown ml-auto">
    <button class="btn btn-nav-link" @click="showLogoutDropdown = !showLogoutDropdown">季河客服</button>
    <ul v-closable="{exclude: ['logoutDropdownButton'],handler: 'closeLogoutDropdown'}" class="dropdown-menu dropdown-menu-right" :class="{'show': showLogoutDropdown}">
      <li>
        <a class="dropdown-item">登出</a>
      </li>
    </ul>
  </div>

  代表當 ref="logoutDropdownButton" 以外的 DOM 被 click/touch 時，會觸發 this.closeLogoutDropdown()
  用這個元件即可很優雅的實做出 dropdown / showMenu / 表單顯示 ... 等前端操作
  參考資料： Vue 自訂指令 (ref: https://cn.vuejs.org/v2/guide/custom-directive.html)
*/
let handleOutsideClick
const vueClosable = {
  bind (el, binding, vnode) {
    handleOutsideClick = (e) => {
      e.stopPropagation()
      const { handler, exclude } = binding.value
      let clickedOnExcludedEl = false
      exclude.forEach(refName => {
        if (!clickedOnExcludedEl) {
          const excludedEl = vnode.context.$refs[refName]
          if (excludedEl) clickedOnExcludedEl = excludedEl.contains(e.target)
        }
      })
      if (!el.contains(e.target) && !clickedOnExcludedEl) {
        vnode.context[handler]()
      }
    }
    // 元件生成時設置觸發事件
    document.addEventListener('click', handleOutsideClick)
    document.addEventListener('touchstart', handleOutsideClick)
  },
  unbind () {
    // 元件移除時關閉觸發事件
    document.removeEventListener('click', handleOutsideClick)
    document.removeEventListener('touchstart', handleOutsideClick)
  }
}

export default vueClosable
