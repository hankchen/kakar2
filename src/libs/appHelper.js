/* app 通用 helper  */

import Vue from 'vue'
import VueCompositionApi from '@vue/composition-api'
Vue.use(VueCompositionApi)

import store from '../store'
import { computed } from '@vue/composition-api'

// toast
import { Toaster } from "@blueprintjs/core"
const AppToaster = Toaster.create({ position: 'top' })

// 觸發 Toast (訊息通知元件)
function showToast(message, config = {}) {
  const baseConfig = {intent: 'primary', icon: 'notifications' }
  const data = Object.assign({}, baseConfig, config, {message: message})
  return AppToaster.show(data)
  /**
    # 用法：

    * 一般
    => app.showToast('hello world')

    * 加入客制色系 & icon 的用法 (預設: {intent: 'primary', icon: 'notifications' } )
    => app.showToast('hello world', {intent: 'success', icon: 'issue' })

    * 可用參數：
    intent : primary, success, danger, warning
    icon   : 參考 https://blueprintjs.com/docs/#icons
  **/
}

function setLoading(status, mType) {store.commit('setLoading', {status: status, mType: mType})}

// 顯示自訂的 alert 畫面
function showAlert(text) { store.commit('setCustomModal', { type: 'alert', text }) }
function hideModal() { store.commit('setCustomModal', { type: '', text: '' }) }

// 顯示自訂的 confirm 畫面
// text: 大標題
// confirmText: 確認按鈕label
// cancelText: 取消按鈕label
// confirmNoteTitle: 附註標題
// confirmNoteChoice: 附註選項
// confirmNoteChoosed: 附註選項已選
// confirmNoteRemark: 附註選了其他要手key的說明
// confirmNoteRemarkPlaceHolder: 其他原因的place holder
// confirmNoteWarnMsg: 警告訊息
function showConfirm(text, confirmText, cancelText,
  confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
  confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText) {
  return new Promise((resolve, reject) => {
    store.commit("setCustomModal", {
      type: 'confirm',
      text, confirmText, cancelText, 
      confirmNoteTitle, confirmNoteChoice, confirmNoteChoosed, confirmNoteRemark,
      confirmNoteRemarkPlaceHolder, confirmNoteWarnMsg, confirmNoteText,
      resolve, reject
    });
  });
}

// 依據 cssTouch 參數設置 mobile 滑動的 css 參數
const touchScrolling = computed(() => {
  const cssTouch = store.state.cssTouch
  return {'-webkit-overflow-scrolling': (cssTouch) ? 'touch' : 'auto' }
})
function checkReceiver(vip) {
  var errMsg = "";
  const twMobilePhoneRegxp = /^09[0-9]{2}[0-9]{6}$/; // 台灣手機號碼格式需為09XXXXXXXX
  const cnMobilePhoneRegxp = /^1[0-9]{2}[0-9]{8}$/;  // 中國手機號碼格式需為1XXXXXXXXXX
  if (twMobilePhoneRegxp.test(vip.mobileTmp) === false && cnMobilePhoneRegxp.test(vip.mobileTmp) === false) errMsg = '手機號碼格式錯誤'
  if (vip.addrsTmp == "" || vip.receiverTmp == "") errMsg = (errMsg != ""? errMsg + '<br>收件人 或 地址未填寫':'收件人 或 收件人地址未填')
  return errMsg; 
}
function IsMbAgent(){
  var IsMobileAgent=false;
  var userAgent = navigator.userAgent;
  // eslint-disable-next-line no-useless-escape
  var CheckMobile = new RegExp("android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino");
  var CheckMobile2 = new RegExp("mobile|mobi|nokia|samsung|sonyericsson|mot|blackberry|lg|htc|j2me|ucweb|opera mini|mobi|android|iphone");

  if (CheckMobile.test(userAgent) || CheckMobile2.test(userAgent.toLowerCase())) {
    IsMobileAgent = true;
  }
  return IsMobileAgent;
}
function makeid_num(num) {
  num = num || 1;
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < num; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
}
function removeEmojis(string_a) {
  if (!string_a) return "";
  var regex = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|[\ud83c[\ude50\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
  
  var reg = /[~#^$@%&!?%*]/gi;
  return string_a.replace(regex, '').replace(reg, '');
}
function getParameterByName(name) {
  var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImage(imageUrl) {
  // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
  const isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl.toLowerCase())
  // 若不是圖檔，回傳 noImageUrl
  if (isImage === false) return 'https://web.jh8.tw/kakar2/img/img_init.gif'

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.getters.srcUrl + imageUrl
}
function b64_to_utf8(str_){
  if (typeof str_ === "string") return decodeURIComponent(atob(str_));
	return "";
}
function jParse(s){
  let pData ;
  try { 
    if (typeof s === "string"){
      pData = JSON.parse(s)
    }
    pData = pData || {}; 
  }
  catch(e) { pData = {} }
  return pData
}
// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImageWuhu(imageUrl) {
  // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
  const isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl)
  // 若不是圖檔，回傳 notImage 字串
  if (isImage === false) return 'https://web.jh8.tw/kakar2/img/img_init.gif'

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  return store.state.api.picUrl + imageUrl
}
// 顯示圖檔(依據塞入的值，來決定回傳的值)
function showImgSubsidiary(imageUrl) {
  
  // 是否為圖檔 (尾端必須為 .jpg, .jpeg, .png, .gif, .bmp)
  const isImage = /.jpg|.jpeg|.png|.gif|.bmp/.test(imageUrl)
  // 若不是圖檔，回傳 notImage 字串
  if (isImage === false) return 'https://web.jh8.tw/kakar2/img/img_init.gif'

  // 是否為完整網址
  const isFullUrl = /http:|https:/.test(imageUrl)
  // 如果是，直接回傳 imageUrl
  if (isFullUrl) return imageUrl
  // 若不是，則回傳的值要補上 srcUrl
  //console.log("qqq>>>>",store.getters.subSrcUrl + imageUrl);
  return store.getters.subSrcUrl + imageUrl
}
// 判斷是否已登入
const isLogin = computed(() => store.getters.isLogin )
// 是否為一般登入驗證前/後
function isNotLinePara(){
  if (store.state.customModal.type == 'confirm') return true;
  const p_code = store.state.searchURL.code || "";
  const p_state = store.state.searchURL.state || ""; 
  const p_lineP = store.state.searchURL.linePara || ""
  const p_lineNtf = store.state.searchURL.lineNofInfo || ""  
  const t_code = getParameterByName("code") || "";
  const t_state = getParameterByName("state") || "";
  const t_lineP = getParameterByName("linePara") || "";  
  const t_lineNtf = getParameterByName("lineNofInfo") || "";
  return (!p_code && !p_state && !p_lineP && !p_lineNtf && !t_code && !t_state && !t_lineP && !t_lineNtf)
}
// 修正ios鍵盤擠壓頁面無法復原
function fixScroll() {
  // 延遲正確取得activeElement
  setTimeout(() => {
    let u = navigator.userAgent
    let isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    // console.log('====>',document.activeElement.tagName)
    if (isiOS && document.activeElement.tagName !== 'INPUT') {
      window.scrollTo(0, 0);
			document.body.scrollTop = 0;
    }
  }, 0)
}
// 常見問題的資料
const rawFaqData = [
  {
    // 大類：
    code: 'M01',
    // 大類標題
    title: '卡+會員登入/註冊',
    // 小類
    subTypes: [
      { // 小類代碼
        code: 'S01',
        // 小類標題
        title: '如何申請一般會員/一般VIP會員帳號?',
        // 小類內容
        content: '下載卡+於開啟後，點選首頁左上角"加入會員"，填入基本資料並綁定手機號碼，即可成為一般會員。再到門市消費並出示卡+內會員條碼，累計消費滿3000元即可升級為一般VIP會員。',
        open: false
      }, {
        code: 'S03',
        title: '一個人可以註冊幾個會員帳號呢?',
        content: '一個帳號對應且綁定一支手機號碼。',
        open: false
      }, {
        code: 'S04',
        title: '手機一直無法收到簡訊，怎麼辦?',
        content: '請先確認您的手機系統，是否有檔垃圾簡訊或是簡訊功能未開啟的狀況。 <br> 您可以再次點選發送簡訊驗證碼，請系統再次發送手機認證碼至您的手機，請注意點擊【重新寄驗證碼】需間隔一分鐘以上。 <br> 若持續收不到手機認證簡訊，請至聯絡我們填寫您的問題與帳號(手機號碼)，將會派專人盡速處理。',
        open: false
      }, {
        code: 'S06',
        title: '為什麼手機號碼及生日在註冊後無法更改?',
        content: '1.一個會員帳號對應且綁定一隻手機號碼無法更改，若需修正，請至門市送回總公司服務。 <br> 2.會員生日無法自行更改，若需修正，請洽客服專線做修改。修改次數限一次。',
        open: false
      }, {
        code: 'S07',
        title: '忘記密碼如何查詢',
        content: '請您在 卡+ 主畫面下方點選忘記密碼，系統會寄出驗證碼到您的手機簡訊中。 <br> 提醒您，每日僅能操作”忘記密碼”三次，操作後請稍待簡訊送達，也請不要連續按下忘記密碼，因為您必須使用最後一封通知簡訊的驗證碼，前面收到的都將會被覆蓋失效。(晚上0時自動歸零，可再重複查詢)',
        open: false
      }
    ]
  },
  // {
  //   code: 'M04',
  //   title: '儲值金門市儲值及使用問題',
  //   subTypes: [
  //     {
  //       code: 'S01',
  //       title: '如何充值儲值金?',
  //       content: '任一門市皆有提供現金儲值服務，儲值時需出示卡+會員條碼給門市人員收費掃碼，即可完成門市儲值。',
  //       open: false
  //     }, {
  //       code: 'S02',
  //       title: '如何使用儲值金付款',
  //       content: '於門市消費結帳時，出示您的卡+會員條碼並確認要以儲值金全額付款或是部分付款，核對結帳後立即從您的帳號扣款相應儲值金額',
  //       open: false
  //     }, {
  //       code: 'S03',
  //       title: '哪裡可以查詢儲值金交易紀錄',
  //       content: '於下方工具列點選【會員專區】>點擊【儲值金】進入【儲值紀錄】，下方列表只會顯示最近10筆「現金儲值或儲值金消費」紀錄。',
  //       open: false
  //     }
  //   ]
  // },
  {
    code: 'M05',
    title: '積點累積及使用問題',
    subTypes: [
      {
        code: 'S01',
        title: '如何獲得會員積點?',
        content: '至門市消費結帳時，出示卡+會員條碼，單筆發票金額消費每滿100元即可獲得發放1枚積點至該出示會員卡+帳號內；並以單筆發票消費金額除以100後取整數計算方式發放積點，小數點後歸零不於下次累記。(例如一般會員:發票金額470元，即470除以100等於約4.7，該次消費金額即可獲贈4枚積點，小數點0.7部分歸零不累計)。',
        open: false
      }, {
        code: 'S02',
        title: '消費完成後，需要多久才可以獲得會員積點?',
        content: '在網路傳輸正常狀況下，消費完成後五分鐘內會員積點即會顯示在您的會員帳號內',
        open: false
      }, {
        code: 'S03',
        title: '如何在門市使用會員積點',
        content: '請至門市點餐時出示卡+會員條碼，並告知門市服務人員今日消費欲使用幾點會員積點兌換當期活動商品或折抵現金消費，於結帳時進行掃碼直接將您帳號中的積點折抵兌換。',
        open: false
      }, {
        code: 'S04',
        title: '除門市使用外，會員積點也可以兌換指定優惠券，如何兌換?',
        content: '點選卡+下方功能列，進入【會員專區】>【積點換券】，可依照表單上品項用帳號內剩餘積點兌換指定商品優惠券，選定確認後即可兌換該優惠券。',
        open: false
      }, {
        code: 'S05',
        title: '如果積點兌換錯的優惠券，可否取消換回積點呢?',
        content: '一旦確認兌換即無法取消，使用的積點不再歸還，還請務必詳閱票券內容，以確保您的權益。',
        open: false
      }, {
        code: 'S07',
        title: '結帳時無法出示我的卡+條碼，那這次消費可以累積會員積點嗎?',
        content: '若未能於當日結帳時進行累點，僅提供當日回店補登(需出示發票證明)，過該日營業時間，即不再提供補登服務。',
        open: false
      }, {
        code: 'S08',
        title: '可以將我的會員積點轉讓給其他人嗎?',
        content: '很抱歉，因會員積點均以每一帳號進行累積，且為保護顧客資料與交易安全，無法將您的會員積點移轉至其它帳號或其它人。',
        open: false
      }, {
        code: 'S09',
        title: '哪裡可以查詢積點交易紀錄',
        content: '於下方工具列點選【會員專區】>點擊【積點】進入【積點紀錄】，下方列表會顯示最近10筆「積點獲得或積點兌換」紀錄。',
        open: false
      }
    ]
  },
  {
    code: 'M06',
    title: '卡+優惠券兌換及使用問題',
    subTypes: [
      {
        code: 'S01',
        title: '優惠券使用期限及兌換方式',
        content: '至門市點餐時出示優惠券 (點選卡+下方功能列，進入【會員專區】>【優惠券】>【可使用】)，確認兌換品項；於結帳時出示畫面與門市服務人員進行【掃碼兌換】及可兌換指定優惠品項或折扣。 <br> ＊提醒您：所有優惠票券皆有使用期限，為確保您的消費權益，請確實於票券到期前，前往門市進行兌換。兌換券一旦過期，不再做補發。',
        open: false
      }, {
        code: 'S02',
        title: '生日優惠券發送方式? (確認可否分別於生日當天發出推撥，給予優惠)',
        content: '每個月都會發送專屬生日優惠券給當月壽星，生日優惠券包含商品或折扣等不定期優惠，使用期限為一整個月。如：您生日為9月，生日禮有效期限為9/1-9/30。 <br> 生日禮每年僅發送一次，生日禮發送時間為前一個月21號，票券直接發送您的【優惠券】中。 <br> 若您於生日前一個月21號之前已完成下載並註冊會員，即可於生日前一個月 21號收到生日優惠券。 <br> 若您於生日前一個月21號之後才成為會員，您的生日禮將於隔年生日前一個月份 21號收到生日禮。',
        open: false
      },
    ]
  }
]
export { showToast, showAlert, showConfirm, setLoading, hideModal, touchScrolling, isLogin, isNotLinePara, showImage, showImageWuhu,showImgSubsidiary, fixScroll, checkReceiver,IsMbAgent, makeid_num, getParameterByName,b64_to_utf8,jParse,removeEmojis,rawFaqData }
