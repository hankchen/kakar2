/* eslint-disable */
/* 與殼溝通相關的 function */

import axios from 'axios'
import store from 'src/store'
import { S_Obj, L_Obj } from 'src/fun1.js'	// 共用fun在此!
import router from 'src/appKakar2/router'
import { showAlert,isNotLinePara } from 'src/libs/appHelper.js'

const IsWebProd		= S_Obj.isWebProd
	,IsStaging 			= S_Obj.isStaging
	,IsDev 					= S_Obj.isDebug 	//開發debug


// 打 logger
const deviseLogger = function(act, params, cbFn) {
  // 時間戳記
  const getTimeStamp = function(t) {
    // 當下時間
    const time = t || new Date()
    // 補齊位數 => e.g. 1 -> 01 ,  51 -> 051
    const pad = function(num, maxLength) {
      // 要補齊的字數
      const diffLength = maxLength - num.toString().length
      return (new Array(diffLength + 1)).join('0') + num
    }
    // => sample: " @ 13:02:02.993"  (時:分:秒.毫秒)
    return  "@ " + (pad(time.getHours(), 2)) + ":" + (pad(time.getMinutes(), 2)) + ":" + (pad(time.getSeconds(), 2)) + "." + (pad(time.getMilliseconds(), 3));
  }

  // console.groupCollapsed(`devise: ${act} ${getTimeStamp()}`);
  // console.log('%c act',    'color: #afaeae; font-weight: bold', act);
  // console.log('%c params', 'color: #9E9E9E; font-weight: bold', params);
  // console.log('%c cbFn',   'color: #03A9F4; font-weight: bold', cbFn);
  // console.groupEnd();
}

// 執行與殼溝通 function 的統一接口
const deviseFunction = function(act, params='', cbFn=''){
  // if (!act) return console.log('deviseFunction: 請輸入 act')
  // 打 logger
  // deviseLogger(act, params, cbFn) //??是否跟log有關?
  // 必須在殼裡面才能執行
  if (typeof(JSInterface) === 'undefined') return
  // 執行跟殼溝通的 function
  JSInterface.CallJsInterFace(act, params, cbFn)
}

// (藉由殼 API) 發送手機驗證簡訊
function sendDeviseSMSreCode(payload) {
  // payload sample: { phone: '0955111222;;886', SMSSendQuota: '20190514-3' }
  // 將已發送扣打紀錄存入殼裡
  deviseFunction('SetSP', `{"spName":"SMSSendQuota", "spValue":"${payload.SMSSendQuota}"}`, '')
  // 發送手機驗證簡訊
  deviseFunction('AppSMSReCode', payload.phone, '"cbFnSyncDeviseSMSreCode"')
}

// 與殼溝通，存取必要的資料
function ensureDeviseSynced() {
  // console.log('ensureDeviseSynced ======> start')
  // 判斷是否在殼裡面
  const isDeviseApp = store.state.isDeviseApp

  // console.log('ensureDeviseSynced ======> isDeviseApp =>' + isDeviseApp)

  return new Promise(resolve => {
    // console.log('ensureDeviseSynced ======> 2')

    // 不在殼裡 => 吃前端的預設參數, 直接繼續 fetchInitData
    if (!isDeviseApp) return resolve('ok')

    // console.log('ensureDeviseSynced ======> 3')

    // 清空舊的 setSP 資料
    deviseFunction('SetSP', `{"spName":"vipApps", "spValue":""}`, '')

    // console.log('ensureDeviseSynced ======> 4')

    // 如果在殼裡
    //   1. 跟殼取 publicJWT, connect, isFrom, EnterpriseID, 會員登入資訊, 更新 store.state
    //   2. 跟殼取微管雲的系統參數: 每天最多簡訊次數, 每封簡訊發送間隔
    //   3. (以上都做完以後) => resolve('ok') // 繼續 fetchInitData
    deviseFunction('Getlang', '', '"cbFnSyncDeviseLang"')//語系

    // console.log('ensureDeviseSynced ======> 5')

    deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
    deviseFunction('GetAppSysConfig', 'AppSMSIntervalMin,APPSMSDayTimes', '"cbFbSaveAppSysConfig"')

    resolve('ok')
  })
}

// 依據登入參數導向正確的網址
function setRedirectPage() {
  // 必須已登入
  if (!store.getters.isLogin) return

  // 判斷-正式/測試站
	const {RtUrl} 	= store.state.userData
	,hasRtUrl 			= !!RtUrl
  // 從 RtUrl 判斷 正式/ 測試站 帳號

  // 從 location.href 來判斷否為龍海網站
  const isWufuWebProduction = /web\.jh8\.tw\/kakar2/.test(location.href) || /web\.jinher-cn\.com\/kakar2/.test(location.href)
  const isWufuWebStaging = /staging\.jh8\.tw\/kakar2/.test(location.href) || /staging\.jinher-cn\.com\/kakar2/.test(location.href)

// console.log('導向正確的網址-userData: ', store.state.userData);	// @@

  if (hasRtUrl) {
    // SetSP-Homehtml=https://website-staging.jinher.com.tw/wuhulife/index.html
    let setThing = `{"spName":"Homehtml", "spValue": "${ RtUrl }"}`
    // console.log('1deviseHelper.js ===> isStaging =>' + setThing)
    deviseFunction('SetSP', setThing, '')
  } else {
    // SetSP-Homehtml=https://web.jh8.tw/wuhulife/index.html
    let toUrl = 'https://web.jh8.tw/kakar2/index.html'
    if (store.state.language == 'cn'){
      toUrl = 'https://web.jinher-cn.com/kakar2/index.html'
    }
    let setThing = `{"spName":"Homehtml", "spValue": "${ toUrl }"}`
    // console.log('2deviseHelper.js ===> isProduction =>' + setThing)
    deviseFunction('SetSP', setThing, '')
  }

  // 如果在龍海測試站 && 登入的為正式帳號，導回到正式站
  if (isWufuWebStaging && !hasRtUrl && isNotLinePara()) {
    // console.log('3deviseHelper.js  ===> staging -> production')
    alert('您位於測試站，登入的帳號為正式帳號，將導向正式站台')
    if (store.state.language == 'cn'){
      return location.href = 'https://web.jinher-cn.com/kakar2/index.html'
    }else{
      return location.href = 'https://web.jh8.tw/kakar2/index.html'
    }

  }

  // 如果在龍海正式站 && 登入的為測試帳號，導回到測試站
  if (isWufuWebProduction && hasRtUrl && isNotLinePara()) {
    // console.log('4deviseHelper.js  ===> production -> staging')
    alert('您位於正式站，登入的帳號為測試用帳號，將導向測試站台')
    return location.href = RtUrl
  }
}

// callback function: 儲存(從殼過來的) 基本設定資料
const cbFnSyncDeviseInfoToStore = function(e) {
  // showAlert('cbFnSyncDeviseInfoToStore===> e.pwd =>' + e.pwd)
  // console.log('cbFnSyncDeviseInfoToStore ===> e=>', e)

  // 過濾，只取需要的資訊
  let baseInfoData = {}
  baseInfoData.pushNotify   = e.pushNotify // 是否推播訊息
  baseInfoData.WebVer       = e.WebVer     // 前端打包檔版本
  baseInfoData.brightness   = e.brightness // 螢幕亮度值
  baseInfoData.AppOS        = e.AppOS      // 殼的裝置 (iOS / Android)
  baseInfoData.currentEnterPriseID = e.currentEnterPriseID // 導向企業號

  // 將資訊存入 state.baseInfo
  store.commit('setBaseInfo', baseInfoData)

  if (e
    && e.Account && typeof(e.Account)!=undefined
    && e.Account.trim()!='' && e.Account.trim()!='undefined'
    && e.pwd && typeof(e.pwd)!=undefined
    && e.pwd.trim()!='' && e.pwd.trim()!='undefined'
    && (!e.Tokenkey || typeof(e.Tokenkey)==undefined
    || e.Tokenkey.trim()=='' || e.Tokenkey.trim()=='undefined')) {
    // console.log('===> 是第一次轉到卡2,重新登入')
    // store.dispatch('memberLogout')
    let loginForm = {}
    loginForm.account = e.Account
    loginForm.password = e.pwd
    loginForm.noNeedMd5=true
    store.dispatch('memberLogin', loginForm)

  } else {
    // console.log('===> 非第一次轉到卡2,繼續')

    // 存入點擊紀錄
    if ( typeof(e.appClicks) != 'undefined' && e.appClicks != '' ) {
     const clicks = typeof(e.appClicks) === 'object' ? e.appClicks : JSON.parse(e.appClicks)
     store.commit('saveAppClicks', clicks)
    }

    // 存入卡面圖片
    // console.log('===> e.cardPhoto =>', e.cardPhoto )
    if ( typeof(e.cardPhoto) != 'undefined' && e.cardPhoto != '' ) {
     const photos = typeof(e.cardPhoto) === 'object' ? e.cardPhoto : JSON.parse(e.cardPhoto)
     store.commit('saveCardPhoto', photos)
    }

    // 有已登入的資訊 => 將會員資訊存入 store
    if ( typeof(e.Account) != 'undefined' && e.Account != '' ) {
      let userData = {}
      userData.mac          = e.mac
      userData.jwt          = e.jwt
      userData.Account      = e.Account
      userData.Name         = e.Name
      userData.Email        = e.Email
      userData.Address      = e.Address
      userData.CardNO       = e.CardNO
      userData.Birthday     = e.Birthday
      userData.Sex          = e.Sex
      userData.CardID       = e.CardID
      userData.CardTypeCode = e.CardTypeCode
      userData.CardTypeName = e.CardTypeName
      userData.ExpiredDate  = e.ExpiredDate
      userData.EnterPriseID = e.EnterPriseID
      userData.RtUrl        = e.RtUrl
      userData.RtAPIUrl     = e.RtAPIUrl
      userData.pwd          = e.pwd
      userData.Tokenkey     = e.Tokenkey

      const p_key = Object.keys(e);
      const p_line = store.state.thirdKey.line;
      const p_fb = store.state.thirdKey.fb;
      if (p_key.indexOf(p_line) > -1) userData.FromTDID = e[p_line]
      else if (p_key.indexOf(p_fb) > -1) userData.FromTDID = e[p_fb]


      // 存入會員資訊
      store.commit('setLoginInfo', {data: userData, rememberMe: true, pwd: e.pwd})
      // 取會員資料
      //store.dispatch('fetchVipApps') //從殼運行時, 這裡有必要嗎?取卡包
      // console.log('cbFnSyncDeviseInfoToStore ===> Tokenkey =>' + store.state.member.Tokenkey)
    }
  }
}

// (藉由殼 API) 存入登入資訊
function deviseSetLoginInfo(payload) {
  // console.log('deviseHelper ===> deviseSetLoginInfo')
  // payload sample: {type: 'set', data: res.data, rememberMe: true/false} , {type: 'reset'}

  // 預設值
  let data = {
    "Name": "",
    "Email": "",
    "Address": "",
    "CardNO": "",
    "Birthday": "",
    "Sex": 1,
    "mac": "",
    "CardID": "",
    "CardTypeCode": "",
    "CardTypeName": "",
    "ExpiredDate": "",
    "MembersName": "",
    "Account": "",
    "pwd": "",
    "note": "",
    "jwt": "",
    "RtUrl": "",
    "RtAPIUrl": "",
    "Tokenkey": ""
  }

  // 存入登入資料
  if (payload.type === 'set' && payload.rememberMe) {
    data = payload.data
    data['MembersName'] = payload.data.Name
    data['pwd'] = payload.pwd
    // 存入殼的推播設定
    deviseFunction('Do_Register', `{"MembersName":"${data.Name}", "Account":"${data.CardNO}"}`, '')
  }
  let device_key = ['MembersName','Name','Email','Address','CardNO','Birthday','Sex','mac','CardID','CardTypeCode','CardTypeName','ExpiredDate','Account','note','pwd','RtUrl','RtAPIUrl','Tokenkey'];

  //web login不用存裝置(mobile login)的
  const p_key = Object.keys(data);
  if (p_key.indexOf(store.state.thirdKey.line) > -1) device_key.push(store.state.thirdKey.line);
  if (p_key.indexOf(store.state.thirdKey.fb) > -1) device_key.push(store.state.thirdKey.fb);

  // 寫入要存的 key 設定
  data['_KEY'] = device_key.join()//"MembersName,Name,Email,Address,CardNO,Birthday,Sex,mac,CardID,CardTypeCode,CardTypeName,ExpiredDate,Account,note,pwd,RtUrl,RtAPIUrl,Tokenkey"

  // 執行存入殼裡
  deviseFunction('SetSPS', JSON.stringify(data), '')

  // only for test logout
  deviseFunction('GetSPAll', '', '"cbFnSyncDeviseInfoToStore"')
}

// callback function: 儲存(從殼過來的) 殼版本差異參數
const cbFnSetDeviseVersionIsDiff = function(e) {
  store.commit('SetDeviseVersionIsDiff', e.Obj)
}

// callback function: 儲存(從殼過來的) 簡訊驗證碼
const cbFnSyncDeviseSMSreCode = function(e) {
  // 將簡訊驗證碼存入 state.SMS_Config
  store.commit('setSMSreCode', e)
}
// callback function: LINE Login
const cbFnLINELogin = function(e) {
  // 執行LINE第三方登入
  const p_data = typeof(e) === 'object' ? e : JSON.parse(e)
  store.dispatch('LineThirdMobileApp',p_data)
}
// callback function: 儲存(從殼過來的) GPS 資料
const cbFnSyncDeviseGPS = function(e) {
  // console.log('cbFnSyncDeviseGPS ===> e =>', e)	// @@
  // 轉成 google maps api 需要的預設格式
  const gpsData = {gps: {lat: e.Lat, lng: e.Lng} }
  // 將 gps 存入 state.baseInfo
  store.commit('setBaseInfo', gpsData)
}
// callback function: 儲存(從殼過來的) 語系
const cbFnSyncDeviseLang = function(e) {
  // console.log('cbFnSyncDeviseLang ======> e =>', e)
  //console.log("language",e)
  store.commit('setlang', e.toLowerCase())
  if (store.state.language == 'cn') deviseFunction('SetSP', `{"spName":"HoHttp", "spValue":"https://kakar2.jinher-cn.com"}`, '')
}
// callback function: 儲存微管雲系統參數
const cbFbSaveAppSysConfig = function(e) {
  // 目前只有取 APPSMSDayTimes(每天最多簡訊次數), AppSMSIntervalMin(每封簡訊發送間隔) 這二個值，只需要存這二個值就好
  const data = {
    APPSMSDayTimes: e.APPSMSDayTimes,
    AppSMSIntervalMin: e.AppSMSIntervalMin
  }
  store.commit('setSMS_Config', data)
}

// callback function: 存入每天簡訊發送的扣打紀錄
const cbFnSetSMSSendQuota = function(e) {
  const data = { SMSSendQuota: e }
  store.commit('setSMS_Config', data)
}

// callback function: 存入字體大小
const cbFnSetFontSize = function(e) {
  store.commit('setFontSizeTemp', e)
}

// callback function: 掃描卡卡綁定企業 app 的 QRcode
const cbFnScanKakarAppsQrCode = function(QrCode) {
  // QrCode =  QrCode.replace('http://www.jinher.com.tw/Qrcode/QrcodeEntry.html?QrCode=','')
  // QrCode = atob(QrCode);
  // QrCode = QrCode.split("&EnterpriseID=")[0];

	/** @Fix:針對手機掃碼被登出(有登入才會來掃,打api也不需token */
  // if (store.getters.isLogin) {
    const isOkUrl = /surl.jh8.tw/.test(QrCode) && QrCode.indexOf("http") == 0
    // const isAddCard = (/www.jinher.com.tw\/Q2\//.test(QrCode)) && QrCode.indexOf("http") == 0
    // 改成通吃舊的和新的網址
    // 舊的: http://www.jinher.com.tw/Q2/888888.html
    // 舊的: http://www.jinher.com.tw/Q/888888.html
    // 新的: http://web.jh8.tw/home/Q2/544330.html
    // 新的: http://web.jh8.tw/home/Q/544330.html
/*
    let isAddCard = false
    if ((QrCode.indexOf('www.jinher.com.tw/Q')>=0
      || QrCode.indexOf('web.jh8.tw/home/Q')>=0)
      && QrCode.indexOf("http") == 0) {
      isAddCard = true
    }
 */
    const isAddCard = RegExp('www.jinher.com.tw/Q|web.jh8.tw/home/Q').test(QrCode) && /http/.test(QrCode)

    if(isOkUrl){
      deviseFunction('openWeburl', QrCode, '')
    }else if(isAddCard){
      // 改成通吃舊的和新的網址
      // 舊的: http://www.jinher.com.tw/Q2/888888.html
      // 新的: http://web.jh8.tw/home/Q2/544330.html
/*
      // 沒有s
      QrCode =  QrCode.replace('http://www.jinher.com.tw/Q2/','')
      QrCode =  QrCode.replace('http://web.jh8.tw/home/Q2/','')
      QrCode =  QrCode.replace('http://www.jinher.com.tw/Q/','')
      QrCode =  QrCode.replace('http://web.jh8.tw/home/Q/','')
      // 有s
      QrCode =  QrCode.replace('https://www.jinher.com.tw/Q2/','')
      QrCode =  QrCode.replace('https://web.jh8.tw/home/Q2/','')
      QrCode =  QrCode.replace('https://www.jinher.com.tw/Q/','')
      QrCode =  QrCode.replace('https://web.jh8.tw/home/Q/','')
      QrCode =  QrCode.replace('.html','')
 */
			// 直接抓檔名,去掉副檔名
			QrCode = QrCode.substring(QrCode.lastIndexOf('/')).replace('/','').replace('.html','')

      if (typeof(QrCode) !== 'string') return showAlert('條碼資料錯誤！')

			QrCode.length && store.dispatch('ensureNewCardQRCode', {Qr:QrCode,isChkSub:true})

    }else{
      showAlert('無效的掃瞄碼')
    }

  // }else{
    // router.replace({path: '/'})
  // }
}

const cbFnOpenWeburl = function(p_value) {
  //showAlert("payok,>>>",p_value)
}
// callback function: 掃瞄QRcode wufu辦活動用
const cbFnScanWuhuQrCode = function(QrCode) {
  //if (typeof(QrCode) !== 'string') return showAlert('條碼資料錯誤！')
  store.commit('initQrCodeItem');//初始化由掃瞄生成的品項

  if (store.getters.isLogin) {
    const isOkUrl = /surl.jh8.tw/.test(QrCode) && QrCode.indexOf("http") == 0
    if (QrCode === "shop?type=scanResult") router.replace(QrCode);
    else if (isOkUrl) deviseFunction('openWeburl', QrCode, '')
    else showAlert('無效的掃瞄碼')
  }else{
    router.replace({path: '/'})
  }
}
const cbFnLog = function(p_text){

}
const cbFnScanQrCode = function(eObj) {
  // console.log('cbFnScanQrCode ===> e.Obj =>', eObj)
	if (eObj) {
		let QrCode = eObj.QrCode.length  ? eObj.QrCode.trim() : ''
		if (QrCode) {
			// console.log('cbFnScanQrCode-QrCode: ', QrCode);	// @@
      var p_body = store.getters.kakarBody();
      if (p_body.Account && p_body.Account != ""){
        store.dispatch('ensureNewCardQRCode', {Qr:QrCode,isChkSub:isNotLinePara(),fn:eObj.fn})
      }else{
        var tt= 6;
        var counter = setInterval(() => {
          // 等到app ready時,清除倒數計時
          p_body = store.getters.kakarBody();
          if (tt === 0 || p_body.Account && p_body.Account != "") { clearInterval(counter);store.dispatch('ensureNewCardQRCode', {Qr:QrCode,isChkSub:isNotLinePara(),fn:eObj.fn}); }
          tt = tt-1;
        }, 100);
      }
		} else {
			showAlert('條碼資料錯誤！')
		}
	}

}

const cbFnScanQrSerCode = function(qCode) {
	if (qCode) {
		location.hash += '&serQCode='+qCode;
	} else {
		showAlert('無效的掃瞄碼！')
	}
}


// -- 使用 jsInterFace 跟殼取資料的用法(目前有用到的) --

// GetSPAll => 取全部存入殼的資訊
// JSInterface.CallJsInterFace('GetSPAll', '', '')

// // SetSPS            => 設定多值
// data = { "_KEY": "InvType,InvDes,InvVend", "InvType": "InvType", "InvDes": "InvDes", "InvVend": "InvVend" }
// JSON.stringify(data) // '{"_KEY":"InvType,InvDes,InvVend","InvType":"InvType","InvDes":"InvDes","InvVend":"InvVend"}'
// JSInterface.CallJsInterFace('SetSPS', JSON.stringify(data), '');
// // SetSP             => 將資料存入殼裡
// JSInterface.CallJsInterFace('SetSP', '{"spName":"SMSCode", "spValue":"2345222"}', '')
// // GetSP             => 跟殼取資料
// JSInterface.CallJsInterFace('GetSP', 'SMSCode', '')
// // gps               => 取得 GPS 地理資訊
// JSInterface.CallJsInterFace('gps', '', '')
// // GetAppSysConfig   => 取得殼的系統參數
// JSInterface.CallJsInterFace('GetAppSysConfig', 'AppRegNeedSMSCheck,AppSMSIntervalMin,APPSMSDayTimes', '')
// // getAppPublicToken => 取公用接口 token
// JSInterface.CallJsInterFace('getAppPublicToken', '', '')
// // AppSMSForgetPW    => 會改會員的帳號密碼，並將新的密碼簡訊發送給會員
// JSInterface.CallJsInterFace('AppSMSForgetPW', '0956241782', '')
// // AppSMSReCode      => 發送驗證簡訊 => callback 會回傳驗證碼 => 也可以用 '0956241782;hello' 自訂驗證碼
// JSInterface.CallJsInterFace('AppSMSReCode', '0956241782', '' );
// // getBrightness     => 取得目前螢幕亮度
// JSInterface.CallJsInterFace('getBrightness', '', '');
// // setBrightness     => 調整螢幕亮度(最亮值 255)
// JSInterface.CallJsInterFace('setBrightness', '255', '');
// // barcode           => 開啟掃描器
// JSInterface.CallJsInterFace('barcode', '', '');
// // openWeburl        => 開啟外部連結
// JSInterface.CallJsInterFace("openWeburl", link, '')
// // tel               => 啟用撥打電話功能
// JSInterface.CallJsInterFace("tel", '0988123123', '')
// // shareTo           => 啟用分享功能
// JSInterface.CallJsInterFace('shareTo', '{"subject":"給目標的抬頭", "body":"給目標的內容", "chooserTitle":"開啟分享時的抬頭"}', '');
// // setIO             => 離開 -> 再進入畫面時，不用再重新 loading
// JSInterface.CallJsInterFace('setIO', '1', '')
// // onFrontEndInited  => 通知殼：前端已經初始化完畢 (殼才會執行 returnJsInterFace )
// JSInterface.CallJsInterFace('onFrontEndInited', '', '')
// // mailto            => 啟用殼的寄信功能
// JSInterface.CallJsInterFace('mailto', '{"mailsubject":"給目標的抬頭", "mailbody":"給目標的內容", "chooserTitle":"開啟分享時的抬頭", "mailreceiver":"sdlong.jeng@gmail.com"}', '')
// // Do_Register       => 推播設定, 存入 App 推播所需要的參數
// JSInterface.CallJsInterFace('Do_Register', '{"MembersName":"Allen", "Account":"0956241782"}', '')
// // clearchache       => 清除網頁 cache
// JSInterface.CallJsInterFace('clearchache', '', '')

/* callback function 集中於此 */

// -- APP 殼的公用 callback 接口 (所有打向殼的 API，都會由殼裡來 call 此 function，回傳資料) --
// 解說： 跟殼溝通的接口

// sample: JSInterface.CallJsInterFace('GetString', 'Account,mac', console.log('hello'))
// => data: {State: "OK", Type: "GetString", Obj: "177f8e5494d251e62ffd68c9dfe903a2"}
// => 殼會執行 window.returnJsInterFace(data, console.log('hello'))
// 所以此 function 是用來接收殼回傳的資訊，並做對應的邏輯處理

// !!重要!! => 由於 returnJsInterFace 被 public 出去，所以任何人都能在 console 執行 returnJsInterFace 來亂 try
// !!重要!! => 所以在裡面的 callback function 務必設定成白名單形式，只接受合法的，不然會造成資安漏洞
const returnJsInterFace = (data, cbFn) => {
  // console.log('returnJsInterFace receive:', data, ", cbFn:", cbFn)

  try {
    // data sample:
    // {State: "OK", Type: "gps", Obj: "{Lat: 37.785835, Lng: -122.406418}"}
    // {State: "OK", Type: "GetAppSysConfig", Obj: {AppRegNeedSMSCheck: "false", AppSMSIntervalMin: "10", APPSMSDayTimes: "3"}}

    // 依據 Type 不同執行各自的邏輯，這樣設計的用意是做白名單，防止被亂 try
    // Policy:
    //   1. !!重要!! 取得資料後要執行的邏輯一律做在 callback function (e.g. 拿取得的資料存入 store, 打 api , 做 xxx 事情)
    //   2. !!重要!! 這裏只做從殼回傳資料的『驗證與轉換』
    //   3. 在 iOS 的 cbFn 是 return fuction, 在 Android 是 return string
    switch(data.Type) {
      // 將多筆資料存入殼裡 (不做 callback)
      case 'SetSPS': break;
      // 設定離開 app 再進入時是否要 reload app (不做 callback)
      case 'setIO': break;
      // 殼的螢幕亮度 (不做 callback)
      case 'setBrightness': break;
      // 將資料存入殼裡 (不做 callback)
      case 'SetSP': break;
      // 將資料(多筆)從殼取出 (因本專案沒用到 GetSPS, 所以不做 callback)
      case 'GetSPS': break;
      // 啟用分享功能 (不做 callback)
      case 'shareTo': break;
      // 取公用接口 jwt (通常只有第一次安裝才會用到) (不做 callback)
      case 'getAppPublicToken': break;
      // 推播設定, 存入 App 推播所需要的參數 (不做 callback)
      case 'Do_Register': break;

      // 將資料從殼取出
      case 'GetSP':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSetSMSSendQuota') cbFnSetSMSSendQuota(data.Obj)
        if (cbFn === 'cbFnSetFontSize') cbFnSetFontSize(data.Obj)
      break;

      // 將全部存入殼的資料取出
      case 'GetSPAll':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseInfoToStore') cbFnSyncDeviseInfoToStore(data.Obj)
      break;

      // 取系統參數 => 目前只有取 APPSMSDayTimes(每天最多簡訊次數), AppSMSIntervalMin(每封簡訊發送間隔) 這二個值
      case 'GetAppSysConfig':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFbSaveAppSysConfig') cbFbSaveAppSysConfig(data.Obj)
      break

      // 取 gps 資訊
      case 'gps':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseGPS') cbFnSyncDeviseGPS(data.Obj)
      break;

      // 發送簡訊驗證碼
      case 'AppSMSReCode':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseSMSreCode') cbFnSyncDeviseSMSreCode(data.Obj)
      break;
      // Line login//LINELogin
      case 'LINELogin':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnLINELogin') cbFnLINELogin(data.Obj)
        //cbFnLINELogin(data.Obj)
      break;
      // 取得語系
      case 'Getlang':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (cbFn === 'cbFnSyncDeviseLang') cbFnSyncDeviseLang(data.Obj)
      break;
      // email 寄信功能
      case 'mailto':
        // (只有在 iOS 才有此狀況) 電子郵件未設定，跳出請去設定的訊息
        if (data.State === 'error' && data.Obj === '手機電子郵件未設定') showAlert('手機電子郵件未設定, 請參考 <a href="https://support.apple.com/zh-tw/HT201320">電子郵件設定</a>')
      break;

      // 掃條碼
      case 'barcode':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
				const isOK = data.State !== 'error'
				if (isOK) {
					if (cbFn === 'cbFnScanKakarAppsQrCode') {
						cbFnScanKakarAppsQrCode(data.Obj);
					// } else if (cbFn === 'cbFnScanWuhuQrCode') {
						// cbFnScanWuhuQrCode(data.Obj);
					} else if (cbFn === 'cbFnScanQrSerCode') {
						cbFnScanQrSerCode(data.Obj);
					}
				}
      break;

      // 存入殼是否需要更新的參數
      case 'versionIsDiff':
        // (有 cbFn) 必須符合特定參數，才執行該 callback function (防止被亂 try )
        if (data.State !== 'error' && cbFn === 'cbFnSetDeviseVersionIsDiff') cbFnSetDeviseVersionIsDiff(data)
      break;
//
      case 'applicationDidEnterBackground':
        if (store.state.appSite !== 'kakar') return

        store.commit('setLoading', true)
        setTimeout(() => { store.commit('setLoading', false) }, 400)
        store.commit('clearAppPublicData')
        store.commit('clearAppMemberData')
        router.push('/')
        // 重新讀取會員卡列表
        store.dispatch('fetchVipApps')
      break;
      // 離開 app 又回去時，回到卡卡首頁
      case 'applicationDidBecomeActive':
        const nowPath = '/cardShopCart/'+router.app.$route.params.enterpriseId;
        const nowPath2 = '/cardShopCart/'+router.app.$route.params.enterpriseId;
        if ((router.app.$route.path == nowPath || router.app.$route.path == nowPath2) && router.app.$route.query.type == 'finishedTemp') store.commit('setBecomeApp')

        // if (store.state.appSite !== 'kakar') return

        // store.commit('setLoading', true)
        // setTimeout(() => { store.commit('setLoading', false) }, 400)
        // store.commit('clearAppPublicData')
        // store.commit('clearAppMemberData')
        // router.push('/')
        // // 重新讀取會員卡列表
        // store.dispatch('fetchVipApps')
      break;
      // 離開 app 又回去時
      case 'kakar':
        // console.log('kakar ===>' + JSON.stringify(data.Obj))
        // ios和安卓都延遲300毫秒,讓isLogin先跑完
        setTimeout(()=>{
          // console.log('kakar ===> 300 milliseconds up')
          cbFnScanQrCode(data.Obj)
        }, 300)
      break;
      // 其他未設定到的，打 console.warn 來做提醒
      default:
        cbFnLog('type not defined :', data.Type)
        // console.warn('type not defined :', data.Type)

    }
  } catch(error) {
    // 若有任何例外錯誤，要跳 console.error 以利查 bug
    //console.error('returnJsInterFace Exception Error: ', error);
  }
}

/* 導向點餐頁 */
function showMOrder(ShopID, ShopType, backPath, p_data) {
  // const isDevelopment = (/dev-service.jinher.com.tw\/projectFeature\//.test(window.location.href))
  // const isStaging = (/staging.jh8.tw\/kakar2\//.test(window.location.href)) || (/website-staging.jinher.com.tw\/kakar2\//.test(window.location.href)) || (/192.168.1.26/.test(window.location.href))
  const {EnterpriseID,Mobile,Username,mac,connect,lang,Address}  = p_data;
  ShopID = ShopID || '';
  const isStaging = (/staging.jh8.tw\/kakar2\//.test(window.location.href)) || (/website-staging.jinher.com.tw\/kakar2\//.test(window.location.href)) || (/staging.jinher-cn.com\/kakar2\//.test(window.location.href)) || /localhost/.test(location.href)
  let hostName='web.jh8.tw';
  if (isStaging) hostName='staging.jh8.tw';
  if (isStaging && lang == 'cn') hostName='staging.jinher-cn.com';
  if (!isStaging && lang == 'cn') hostName='web.jinher-cn.com';

  //(web value,用短網址存)
  const surl_data = {
    EnterpriseID:EnterpriseID || "",
    Mobile:Mobile || "",
    Username:Username || "",
    OrderNo:'',
    DeskNo:'',
    mac:mac || '',
    OpenID:'',
    isFrom:"kakar2",
    ShopID:ShopID,
    connect:connect || "",
    SaleType:ShopType || '',
    lang:lang || 'tw',
  }
  let morderPath = '';
  store.dispatch('calcuSurl',{data:JSON.stringify(surl_data),qrType:"2",exType:"0",url:'' ,fn:(p_code)=>{
    let url
    //
    if (ShopID === '') morderPath = "#/record";
    //url = `https://dev-service.jinher.com.tw/projectFeature/APP_MOrder/MOrder/index.html?sPara=${p_code}${morderPath}`

    if (isStaging)  url = `https://staging.jh8.tw/morder/index.html?sPara=${p_code}${morderPath}`
    // isCover=1 來自meta[viewport-fit=cover]的版
    if (!isStaging) url = `https://web.jh8.tw/morder/index.html?sPara=${p_code}${morderPath}`
    if (lang == 'cn') url = `http://customer.jinher-cn.com/app/MOrderK/index.html?sPara=${p_code}${morderPath}`

    backPath = backPath || "cardStore";
    const backUrl = `https://${hostName}/kakar2/index.html#/${backPath}/${EnterpriseID}`
    //url = `https://dev-service.jinher.com.tw/projectFeature/APP_MOrder/MOrder/index.html?`
    //TODO: 日後可以把url補參數,在網頁也能導向到微點餐
    // 預設值
    const data = {
      // 會員門店號
      "ShopID": ShopID,
      // 主企業號的 ID
      "mainEnterpriseID": EnterpriseID || "",
      // 主企業號的 connect
      "mainConnect": connect || "",//this.state.publicData.MainConnect,
      // Mode選的type, (ex:takeout)
      //"ShopType": ShopType || "",
      // Mode選的type, (ex:takeout)
      "Mobile": Mobile || "",//this.state.userData.Account,
      // 登入者姓名
      "Username": Username || "",//this.state.userData.Name,
      // 登入者地址
      "Address": Address || "",//this.state.userData.Address,
      "kakarUrlPath": backUrl,
      "isFrom":"kakar2", //卡包都在卡+,線上點餐推播用
    }
    if (ShopType !== '') data.ShopType = ShopType;
    // 寫入要存的 key 設定
    data['_KEY'] = "ShopID,mainEnterpriseID,ShopType,Mobile,Username,Address,mainConnect,kakarUrlPath,isFrom"

    // 執行存入殼裡
    deviseFunction('SetSPS', JSON.stringify(data), '')
    store.commit('setLoading', true);
    return window.location.href = url
  }});

}

export { deviseFunction, returnJsInterFace, ensureDeviseSynced, deviseSetLoginInfo, sendDeviseSMSreCode, setRedirectPage, cbFnScanQrCode, showMOrder }
