# member-app

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


---


## 使用解說:

用 source tree 打開此專案, 點擊右上角的『Terminal』，打開終端機視窗

輸入 `yarn; yarn serve` 安裝 & 執行開發環境


## 系統參數：站台的切換

複製根目錄的 .env , 並將檔名存成 .env.local

```
# 設定網站參數 (目前可用的為: kakar , wuhulife)
VUE_APP_SITE=kakar    # 將站台設成 卡+ (預設)
VUE_APP_SITE=wuhulife # 將站台設成 龍海
```


## 打包並更新至正式站/測試站

複製以下的網址並執行即可

- 卡+ (正式站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=kakar
- 卡+ (測試站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=kakar-s

- 龍海 (正式站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=wuhulife
- 龍海 (測試站): https://jenkins.jinher.com.tw/generic-webhook-trigger/invoke?token=wuhulife-s

## 破壞歷程記錄第N次
----------------------------------------------
- 因為限量功能壞了.第六感這裡也被改壞了(數百行),winnie已修正
    ```
    const isErr = data.detail && data.detail.ErrorCode && data.detail.ErrorCode != 0
    ...
    ...
    if (!isErr && !data.error) {...}
    ```
    git記錄:`已更新為 f3c2b4bc: 整理程式 by hank-20210730-1,hank 已於 2021/7/30 下午 05:27 推送 f3c2b4bc在此時改壞了.商城`
----------------------------------------------